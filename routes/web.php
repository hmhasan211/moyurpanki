<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('reboot', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    file_put_contents(storage_path('logs/laravel.log'),'');
    Artisan::call('key:generate');
    Artisan::call('config:cache');
    return '<center><h1>System Rebooted!</h1></center>';
});
Route::get('/','DashboardController@index');
Route::get('add-product-group-id','DashboardController@addGroupId');

Auth::routes(['register' => false
]);
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'DashboardController@index')->name('home');

    Route::get('test/{id}',function($PurchaseOrderId){
        $purchase = \App\Purchase::find($PurchaseOrderId);
        return view('inventory.purchase.pdf-purchase-order', compact('purchase'));
    });
    /* MR Inventory Start */


    Route::get('User/admin_permission','DashboardController@admin_permission')->name('user.admin_permission');


    Route::get('User/roles-add','DashboardController@roles')->name('user.roles.index');
    Route::post('User/roles-store','DashboardController@storeRole')->name('user.roles.store');
    Route::get('User/roles-edit/{id}','DashboardController@editRoles')->name('users.roles.edit');
    Route::post('User/roles-update/{id}','DashboardController@updateRoles')->name('users.role.update');
    Route::post('User/roles-destroy','DashboardController@destroyRoles')->name('users.roles.destroy');

    Route::get('User/add','UsersController@index')->name('user.add');
    Route::post('User/save','UsersController@store')->name('user.store');
    Route::get('User/edit/{id}','UsersController@show')->name('user.edit');
    Route::post('User/update','UsersController@update')->name('user.update');
    Route::post('User/erase','UsersController@destroy')->name('user.destroy');

    /* Inventory Department start */
    Route::get("Inventory/add-inventory-department","Inventory\InventoryDepartmentController@index")->name('inventory.department.add');
    Route::post("Inventory/store-inventory-department","Inventory\InventoryDepartmentController@store")->name('inventory.department.store');
    Route::get("Inventory/edit-inventory-department/{id}","Inventory\InventoryDepartmentController@edit")->name('inventory.department.edit');
    Route::post("Inventory/update-inventory-department/{id}","Inventory\InventoryDepartmentController@update")->name('inventory.department.update');
    Route::post("Inventory/destroy-inventory-department","Inventory\InventoryDepartmentController@destroy")->name('inventory.department.destroy');
    /* Inventory Department End */



    /* Inventory Group Start */

//    get subcategory
    Route::get("get/subcategory/{id}","Inventory\InventoryGroupController@get_subcategory")->name('inventory.get.sub.category');
    Route::get("get/department/category/{id}","Inventory\InventoryGroupController@get_category")->name('inventory.get.category');

    Route::get("Inventory/add-inventory-group","Inventory\InventoryGroupController@index")->name('inventory.group.add');
    Route::get("Inventory/add-inventory-sub-group","Inventory\InventoryGroupController@subGroup")->name('inventory.sub.group.add');

    Route::post("Inventory/store-inventory-group","Inventory\InventoryGroupController@store")->name('inventory.group.store');
    Route::post("Inventory/store-inventory-sub-group","Inventory\InventoryGroupController@subStore")->name('inventory.sub.group.store');

    Route::get("Inventory/edit-inventory-group/{id}","Inventory\InventoryGroupController@edit")->name('inventory.group.edit');
    Route::post("Inventory/update-inventory-group/{id}","Inventory\InventoryGroupController@update")->name('inventory.group.update');
    Route::post("Inventory/destroy-inventory-group","Inventory\InventoryGroupController@destroy")->name('inventory.group.destroy');
    /* Inventory Group End */

    // store/ department eg: Resturent, madicine, marine store
    Route::get("store/department","Store\StoreController@index")->name('store.department');
    Route::post("add/store/department","Store\StoreController@store")->name('store.add');
    Route::get("store/department/edit/{id}","Store\StoreController@edit")->name('store.department.edit');

    // deck route
    Route::get("Inventory/deck","Deck\DeckController@index")->name('inventory.deck');
    Route::post("Inventory/deck/add","Deck\DeckController@store")->name('deck.add');

    // Locker route
    Route::get("Inventory/locker","Locker\LockerController@index")->name('inventory.locker');
    Route::post("Inventory/locker/add","Locker\LockerController@store")->name('inventory.locker.add');

    /* Inventory Unit Start */
    Route::get("Inventory/add-inventory-unit","Inventory\InventoryUnitController@index")->name('inventory.unit.add');
    Route::post("Inventory/store-inventory-unit","Inventory\InventoryUnitController@store")->name('inventory.unit.store');
    Route::get("Inventory/edit-inventory-unit/{id}","Inventory\InventoryUnitController@edit")->name('inventory.unit.edit');
    Route::post("Inventory/update-inventory-unit/{id}","Inventory\InventoryUnitController@update")->name('inventory.unit.update');
    Route::post("Inventory/destroy-inventory-unit","Inventory\InventoryUnitController@destroy")->name('inventory.unit.destroy');
    /* Inventory Unit End */



    /* Inventory Supplier / Party Start */
    Route::get("Inventory/add-inventory-supplier","Inventory\InventorySupplierController@index")->name('inventory.supplier.add');
    Route::post("Inventory/store-inventory-supplier","Inventory\InventorySupplierController@store")->name('inventory.supplier.store');
    Route::get("Inventory/edit-inventory-supplier/{id}","Inventory\InventorySupplierController@edit")->name('inventory.supplier.edit');
    Route::post("Inventory/update-inventory-supplier/{id}","Inventory\InventorySupplierController@update")->name('inventory.supplier.update');
    Route::post("Inventory/destroy-inventory-supplier","Inventory\InventorySupplierController@destroy")->name('inventory.supplier.destroy');
    /* Inventory Supplier / Party End */

    /* Item Create Start */

    Route::get("Inventory/add-inventory-item","Inventory\ProductController@index")->name('inventory.item.add');
    Route::post("Inventory/store-inventory-item","Inventory\ProductController@store")->name('inventory.item.store');
    Route::get("Inventory/edit-inventory-item/{id}","Inventory\ProductController@edit")->name('inventory.item.edit');
    Route::post("Inventory/update-inventory-item/{id}","Inventory\ProductController@update")->name('inventory.item.update');
    Route::post("Inventory/destroy-inventory-item","Inventory\ProductController@destroy")->name('inventory.item.destroy');

    Route::get("Inventory/search-product-item","Inventory\ProductController@search_product_item")->name('search-product-item');

    /* Group Wise Product List Start */

    Route::post("Inventory/group-wise-product-list","Inventory\ProductController@groupWiseProducts")->name('groupwise.productList');

    /* Group Wise Product List End */

    /* Item Create End */

    /* Requisition Start */
    Route::get("Inventory/add-inventory-requisition","Inventory\InventoryRequisitionController@index")->name('inventory.requisition.add');
    Route::get("Inventory/add-individual-requisition/{requisition_id}","Inventory\InventoryRequisitionController@addIndividualRequisition")->name('individual.requisition.add');
    Route::get("Inventory/add-individual-new-requisition-product/{new_requisition_id}","Inventory\InventoryRequisitionController@addIndividualNewRequisitionProduct")->name('individual.new.requisition.add');
    Route::post("Inventory/store-individual-requisition","Inventory\InventoryRequisitionController@storeIndividualRequisition")->name('individual.requisition.store');
    Route::post("Inventory/store-individual-new-requisition-product","Inventory\InventoryRequisitionController@storeIndividualNewRequisitionProduct")->name('individual.new.requisition.store');
    Route::get("Inventory/edit-individual-requisition","Inventory\InventoryRequisitionController@editIndividualRequisition")->name('individual.requisition.edit');
    Route::get("Inventory/delete-individual-requisition/{id}","Inventory\InventoryRequisitionController@deleteIndividualRequisition")->name('individual.requisition.delete');
    Route::get("Inventory/list-inventory-requisition","Inventory\InventoryRequisitionController@lists")->name('inventory.requisition.lists');

    /* product inforation according to product code or name - id */
    Route::post("Inventory/product-info-inventory-requisition","Inventory\InventoryRequisitionController@findProduct")->name('inventory.item.info');
    Route::post("Inventory/store-requisition-product-info-inventory-requisition","Inventory\InventoryRequisitionController@storeTempRequisitionInfo")->name('inventory.purchase.temp.store');

    /* Requisition product list retrive from TempData start */
    Route::get('pendingProductLists','Inventory\InventoryRequisitionController@productLists')->name('inventory.requisition.pending.products');
    Route::post('Inventory/product-remove-from-requisition-lists','Inventory\InventoryRequisitionController@removeItemFromList')->name('inventory.requisition.item.remove');
    /* R*/

    /* Requisition list store start */
    Route::post('store-purchase-requisition','Inventory\InventoryRequisitionController@storePurchaseRequisition')->name('inventory.store.requisition');
    /* Requisition list store End */
    /* Requisition List View in Front Page Start */
    Route::get("Inventory/view-purchase-requisition","Inventory\InventoryRequisitionController@requisitions")->name('inventory.purchase-requisition.view');
    Route::delete("Inventory/delete-purchase-requisition/{id}","Inventory\InventoryRequisitionController@deleteRequisition")->name('delete.requisition');
    Route::get("Inventory/view-single-purchase-requisition/{id}","Inventory\InventoryRequisitionController@singleRequisition")->name('inventory.purchase-requisition.single');
    Route::get("Inventory/print-single-purchase-requisition/{id}","Inventory\InventoryRequisitionController@singleRequisitionPrint")->name('inventory.purchase-requisition.single-print');
    Route::post("Inventory/single-item-approval-purchase-requisition/{id}",'Inventory\InventoryRequisitionController@checkingApproval')->name('inventory.requisition-item.checking-approval');
    Route::get("Inventory/view-approved-purchase-requisition/{id}","Inventory\InventoryRequisitionController@viewApprovedItems")->name('inventory.requisition.approved-items');

    /* Requisition List View in Front Page End */
    Route::get("inventory/pending/requisition","RequisitionController@pending_requisition")->name('purchase.requisition');
    Route::get("inventory/pending/requisition/product/{id}","RequisitionController@pending_requisition_product")->name('pending.requisition.product');
    Route::post("approve/pending/requisition/{id}","RequisitionController@approve_pending_requisition")->name('approve.pending.requisition');

    /* After Check Purchase Requisition List start*/
    Route::get('Inventory/after-check-purchase-requisition','Inventory\InventoryRequisitionController@AftercheckingItemLists')->name('');
    /* After Check Purchase Requisition List end*/



    /* Requisition End */


    /* ================================               ================================*/
    /* ================================               ================================*/
    /* ========================== Purchase Requisition Start  ========================*/
    /* ================================               ================================*/
    /* ================================               ================================*/

    Route::get("Inventory/add-inventory-requisition-purchase","Inventory\InventoryPurchaseController@index")->name('inventory.requisition.purchase');
    Route::get("Inventory/purchase-order/","Inventory\InventoryPurchaseController@order")->name('inventory.order.purchase');
    Route::get("Inventory/purchase-order-update/{purchase_id}/{requisition_id}/{quotation_id}","Inventory\InventoryPurchaseController@order")->name('inventory.order.purchase.update');
    Route::post("Inventory/invoice-requisition/quotation-list","Inventory\InventoryPurchaseController@requisitionQuotationList")->name('inventory.ajax-quotation-list');
    Route::post("Inventory/purchase-store","Inventory\InventoryPurchaseController@store")->name('inventory.purchase.store');
    Route::post("Inventory/purchase-confirm","Inventory\InventoryPurchaseController@confirm")->name('inventory.purchase.confirm');
    Route::post("Inventory/purchase-update","Inventory\InventoryPurchaseController@update")->name('inventory.purchase.update');
    Route::get('Inventory/purchase-summary','Inventory\InventoryPurchaseController@summaryPurchase')->name('inventory.purchase.summary');
    Route::get('Inventory/print-po-no','Inventory\InventoryPurchaseController@printPONo')->name('inventory.po.no');
    Route::delete('Inventory/delete-purchase-summary/{id}','Inventory\InventoryPurchaseController@deletePurchase')->name('inventory.purchase.delete');
    Route::get('Inventory/purchase-order/print/{id}','Inventory\InventoryPurchaseController@printPurchaseOrder')->name('inventory.purchase.print');
    Route::get('Inventory/purchase-order/details/{id}','Inventory\InventoryPurchaseController@detailPurchaseOrder')->name('inventory.purchase.details');


    Route::get('Inventory/purchase-order/mail/{id}','Inventory\InventoryPurchaseController@mailPurchaseOrder')->name('inventory.purchase.mail');

    /* ================================               ================================*/
    /* ================================               ================================*/
    /* ========================== Purchase Requisition End  ==========================*/
    /* ================================               ================================*/
    /* ================================               ================================*/



    /* Inventory Brand Start */
    Route::get("Inventory/add-inventory-brand","Inventory\InventoryBrandController@index")->name('inventory.brand.add');
    Route::post("Inventory/store-inventory-brand","Inventory\InventoryBrandController@store")->name('inventory.brand.store');
    Route::get("Inventory/edit-inventory-brand/{id}","Inventory\InventoryBrandController@edit")->name('inventory.brand.edit');
    Route::post("Inventory/update-inventory-brand/{id}","Inventory\InventoryBrandController@update")->name('inventory.brand.update');
    Route::post("Inventory/destroy-inventory-brand","Inventory\InventoryBrandController@destroy")->name('inventory.brand.destroy');
    /* Inventory Brand end */

//    stok summery
    Route::get("Inventory/InStock","Inventory\InventoryInStokController@index")->name('inventory.inStock');


    /* Goods in Start */
     Route::get("add-goodsin","GoodsinController@index")->name('goodsin.add');
     Route::get("add-goodsin/{id}/edit","GoodsinController@edit")->name('goodsin.edit');
     Route::put("add-goodsin/{id}/update","GoodsinController@update")->name('goodsin.update');
     Route::delete("add-goodsin/{id}/delete","GoodsinController@delete")->name('goodsin.delete');
     Route::get("add-goodsin/{item_id}/set-unit","GoodsinController@setUnit")->name('goodsin-set-unit.add');
     Route::get("add-goodsin/{item_id}/set-deck","GoodsinController@setDeck")->name('goodsin-set-deck.add');

     Route::get("goodsOut","GoodsOutController@index")->name('goods.out');

     Route::get("add-goodsin/{purchase_id}/get-supplier","GoodsinController@getSupplier")->name('goodsin-get-supplier');
     Route::get("new-goodsin","GoodsinController@newGoodsIn")->name('goodsin.new');
     Route::get("mr-report","GoodsinController@mrReport")->name('mr-report');
     Route::delete("mr-report/{id}","GoodsinController@mrReportDelete")->name('delete-mr-report');
     Route::get("mr-report/{goodsin_id}/print","GoodsinController@mrReportPrint")->name('print.mr-report');
     Route::post("store-goodsin","GoodsinController@store")->name('goodsin.store');
     Route::post("new-store-goodsin","GoodsinController@storeNew")->name('goodsin.store-new');
    //  Route::get('lc-cost','GoodsinController@lcCost')->name('lc.cost');
    //  Route::post('store-lc-cost','GoodsinController@storeLcCost')->name('lccost.store');
    //  Route::get('lc-report','GoodsinController@lcReport')->name('lc.report');
    //  Route::post('find-lc-report','GoodsinController@lcReportFind')->name('lc.reportFind');
     Route::get('goodsin-history','GoodsinController@goodsin_history')->name('goodsin-history');

     Route::get('stock-register','GoodsinController@stockRegister')->name('stock-register');
     Route::get('stock-transaction','GoodsinController@stockTransaction')->name('stock-transaction');

     Route::get('all-categories-stock-summary','GoodsinController@all_categories_stock_summary')->name('all_categories_stock_summary');
     Route::get('all-categories-stock-summary/{category_id}','GoodsinController@category_wise_stock_summary')->name('category_wise_stock_summary');
     Route::get('all-categories-stock-summary/{category_id}/print','GoodsinController@category_wise_stock_summary_print')->name('category_wise_stock_summary_print');

    Route::get('stock/summary','GoodsinController@stock_summary')->name('stock.summary');//Created BY SHIPON

     Route::get('find-purchase-products','GoodsinController@findPurchaseProducts')->name('purchase.productList');

     /* Goods in End */

    /* Quotation taking start */
    Route::get('Inventory/quotation-make/{id}','Inventory\InventoryPurchaseController@makeQuotation')->name('inventory.quotation.add');
    Route::post('Inventory/quotation-store/{id}','Inventory\InventoryPurchaseController@storeQuotation')->name('inventory.quotation.store');
    /* Quotation taking end */

    /* PO taking start */
    Route::get('Inventory/make-po/{id}','Inventory\InventoryPurchaseController@makePurchaseOrder')->name('inventory.po.add');
    Route::post('Inventory/store-po/{id}','Inventory\InventoryPurchaseController@storePurchaseOrder')->name('inventory.po.store');
    /* PO taking end */

    /* Quotation List start */
    Route::get('Inventory/requisition-quotation','Inventory\InventoryPurchaseController@quotationLists')->name('inventory.requisition.quotation.list');
    Route::get('Inventory/save-quotation-product-price/{id}','Inventory\InventoryPurchaseController@saveQuotationProduct')->name('save.quotation-product');
    Route::get('Inventory/save-requisition-product-qty/{id}','Inventory\InventoryPurchaseController@saveRequisitionProductQty')->name('save.requisition-product-qty');
    Route::get('Inventory/view-requisition-quotation/{id}','Inventory\InventoryPurchaseController@allQuotations')->name('quotations.list');
    Route::get('Inventory/change-quotation-supplier/{data}','Inventory\InventoryPurchaseController@changeQuotationSupplier')->name('change.supplier.quotation');
    Route::get('Inventory/set-supplier-payment/{data}','Inventory\InventoryPurchaseController@setPaymentType')->name('quotations.set-payment');
    Route::delete('Inventory/delete-quotation/{id}','Inventory\InventoryPurchaseController@deleteQuotation')->name('delete.quotation');
    Route::get('Inventory/delete-supplier-quotation/{id}','Inventory\InventoryPurchaseController@deleteSupplierQuotation')->name('delete.supplier.quotation');
    Route::get('Inventory/reset-po/{quotation_id}','Inventory\InventoryPurchaseController@resetPO')->name('reset.po');
    Route::get('Inventory/reset-individual-po/{data}','Inventory\InventoryPurchaseController@resetIndividualPO')->name('reset.individual.po');
    Route::get('Inventory/comparative-statement-of-requisition-quotations/{id}','Inventory\InventoryPurchaseController@comparativeStatementQuotations')->name('quotations.comparative-statement');


    /* Quotation List End */

    /* Comparative Statement start */
    Route::get('Inventory/comparative-statements','Inventory\InventoryPurchaseController@comparativeStatements')->name('inventory.comparative.statements');


    /* Comparative Statement End */


    /* Excel File Read Start */

    Route::get("file-read","Inventory\FileUploadController@index")->name('excel.read');
    Route::post("file-upload","Inventory\FileUploadController@store")->name('file.store');

    /* Excel File Read End */

    /** Export Controller start */

    // Route::get("purchase-order-pdf-view/{id}","PurchaseExportController@purchase_order")->name('purchase_order');
    Route::get("purchase-order-excel/{id}","PurchaseExportController@purchase_order_details_excel")->name('purchase_order_excel');
    Route::get("purchase-order-pdf/{id}","PurchaseExportController@purchase_order_details_pdf")->name('purchase_order_pdf');
    Route::get("purchase-requisitions-excel","PurchaseExportController@purchase_requisitions_excel")->name('purchase_requisitions_excel');
    Route::get("purchase-requisitions-excel/{id}","PurchaseExportController@purchase_requisitions_single_excel")->name('purchase_requisitions_single_excel');
    Route::get("purchase-requisitions-after-check-single-excel/{requisition}","PurchaseExportController@purchase_requisitions_after_check_single_excel")->name('purchase_requisitions_after_check_single_excel');
    Route::get("purchase-supplier-quatations-excel/{requisition}","PurchaseExportController@purchase_supplier_quatations_excel")->name('purchase_supplier_quatations_excel');
    Route::get("purchase-requisitions-after-check-excel","PurchaseExportController@purchase_requisitions_after_check_excel")->name('purchase_requisitions_after_check_excel');

    Route::post('settings/unit-setup','DashboardController@unitSetup')->name('unit.setup');
    /** Export Controller end */

    /* MR Inventory End*/
    Route::get("Inventory/add-stock-issue","StockIssueController@index")->name('inventory.stock.issue');
    Route::post("Inventory/add-stock-issue","StockIssueController@store")->name('inventory.stock.issue.store');
    Route::get("Inventory/add-stock-issue/get-suppliers/{product_id}","StockIssueController@getSuppliers")->name('inventory.stock.issue.getSuppliers');

    Route::get('/admin_permission','DashboardController@admin_permission')->name('admin_permission');
    Route::get('/set-theme/{theme_name}/{color}','DashboardController@setTheme')->name('set-theme');
});
Route::get('migrate',function(){
    Artisan::call('migrate');
    return redirect('/');
});


