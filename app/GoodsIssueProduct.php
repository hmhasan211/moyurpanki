<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsIssueProduct extends Model
{
    protected $fillable = ['unit_id', 'user_id', 'product_id', 'supplier_id', 'qty', 'price', 'reference_no', 'issue_date', 'remarks'];

    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
