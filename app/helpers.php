<?php
use Illuminate\Support\Facades\DB;
use App\Role;
use App\Theme;
function isActive($path, $active = 'active menu-open'){
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

function setMainHeaderClass(){
    $theme = Theme::where('user_id',Auth::id())->first();
    if(isset($theme->id)){
      return $theme->navbar_bg.' '.$theme->navbar_color;
    }else{
      return 'bg-white navbar-light';
    }
    
}


function setSideBarClass(){
    $theme = Theme::where('user_id',Auth::id())->first();
    if(isset($theme->id)){
      return $theme->sidebar_class;
    }else{
      return 'sidebar-light-info';
    }
    
}

function setLogoClass(){
    $theme = Theme::where('user_id',Auth::id())->first();
    if(isset($theme->id)){
      return $theme->logo_color;
    }else{
      return '';
    }
    
}


function app_info(){
    $info = DB::table('company_addresses')->first();
    return $info;
}

function payment_type(){
    return  array('1' => 'Cash','2' => 'Credit');
}

function getRequisitionId($unit_id){
   $requisition_id = \App\Requisition::where('unit_id',$unit_id)->max('requisition_id') + 1;
   return $requisition_id;
}
function getCSNumber($unit_id){
    $cs_no = \App\NewRequisition::where('unit_id',$unit_id)->max('cs_no') + 1;
    return $cs_no;
}
function getPurchaseId($unit_id){
   $purchase_id = \App\Purchase::where('unit_id',$unit_id)->max('purchase_no') + 1;
   return $purchase_id;
}



function settings($key){
   $setting = DB::table('settings')->where('field_key','=',$key)->first();
   return $setting->value;
}
function auth_unit(){
    $units=Auth::user()->units;
   if($units){
       if(count($units)>0){
        return Auth::user()->active_unit;
       }
       return $units->first()->id;

   }else{
        return null;
   }
}

function dateFormat($date){
  return date('d-M-Y', strtotime($date));
}
function unit_name(){
    $units=Auth::user()->units;
    // dd(Auth::user()->units);
   if($units){
           return unit_lists()->where('id',Auth::user()->active_unit)->first()->name;
   }else{
        return null;
   }
}

function get_where(){
    $where=[];
    if(auth_unit()){
        $where=[['unit_id',auth_unit()]];
    }
        return $where;
}
function auth_unit_address(){
    $unit = Auth::user()->active_unit;
    if($unit !=null){
        $unit_address = \App\Unit::query()->findOrFail($unit)->address;
    }else{
        $unit_address = null;
    }

    return $unit_address;
}


function unit_lists(){
    if(settings('module')==1){
        $units=  AccountCompany::pluck('name','id');
        return $units;
    }else{
        return Auth::user()->units ? Auth::user()->units : [];
    }
}
function hasPermission($role,$permission){
    $activeRole=Role::with('permissions')->find($role);
    foreach($activeRole->permissions as $activePermission){
        if($activePermission->name==$permission){
            return true;
        }
    }
    return false;
    //dd($permissions);
}
function canSee($roles,$permission){
    foreach($roles as $role){
    $activeRole=Role::with('permissions')->find($role->id);
    foreach($activeRole->permissions as $activePermission){
        if($activePermission->name==$permission){
            return true;
        }
    }
    }
    return false;
    //dd($permissions);
}
