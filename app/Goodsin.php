<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goodsin extends Model
{
    protected $fillable = ['unit_id','mrr_no','bill_no', 'bill_date', 'chalan', 'chalan_date', 'purchase_id', 'quotation_id', 'requisition_id', 'user_id', 'note', 'condition'];

    public function costs(){
        return $this->hasMany(CostGoodsin::class,'goodsin_id');
    }

    public function products(){
        return $this->hasMany(GoodsinProduct::class,'goodsin_id');
    }

    public function purchase(){
        return $this->belongsTo(Purchase::class);
    }

    public function requisition(){
        return $this->belongsTo(Requisition::class);
    }
    public function quotation()
    {
        return $this->belongsTo('App\Quotation');
    }

}
