<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationProduct extends Model
{
    protected $fillable = ['quotation_id','requisition_id' ,'user_id', 'requisition_product_id', 'supplier_id', 'price', 'qty', 'status','product_remarks','product_id'];

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }

    public function requisition(){
        return $this->belongsTo(NewRequisition::class, 'requisition_id', 'id');
    }

    public function quotation(){
        return $this->belongsTo(Quotation::class);
    }

    public function purchase_product(){
        return $this->hasOne(PurchaseProduct::class, 'quotation_id', 'id');
    }

    public function requisitionProduct(){
        return $this->belongsTo(NewRequisitionProduct::class, 'requisition_product_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
