<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['purchase_no','unit_id','requisition_id','purchase_type','user_id','note','confirm','payment_type','supplier_id','mrr_status','note'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function quotation(){
        return $this->belongsTo(Quotation::class);
    }
    public function requisition(){
        return $this->belongsTo(Requisition::class,'requisition_id', 'id');
    }
    public function new_requisition(){
        return $this->belongsTo(NewRequisition::class,'requisition_id', 'id');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }

    public function details(){
        return $this->hasMany(PurchaseProduct::class,'purchase_id');
    }

    public function goods_in(){
        return $this->hasMany(Goodsin::class,'purchase_id');
    }

}
