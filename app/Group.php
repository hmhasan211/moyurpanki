<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name','description'];

    public function products()
    {
        return $this->hasMany('App\Product');
    }
    public function department(){
        return $this->belongsTo(Unit::class,'unit_id','id');
    }
}
