<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewRequisitionProduct extends Model
{
    protected $fillable = ['price', 'qty','quotation_qty','po_qty', 'note', 'new_requisition_id', 'user_id', 'product_id','sent_for'];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function purchase_products(){
        return $this->hasMany(PurchaseProduct::class,'requisition_product_id','id');
    }

}
