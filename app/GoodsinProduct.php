<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodsinProduct extends Model
{
//    protected $fillable = ['unit_id','purchase_order_no','goodsin_id','locker_id', 'requisition_id', 'quotation_id', 'purchase_id', 'product_id','product_unit_id', 'user_id', 'supplier_id', 'qty', 'price', 'remarks','department_id','group_id'];
    protected $fillable = ['goodsin_id','image','product_id','purchase_order_no','group_id','sub_group_id','unit_id','product_unit_id','qty','price','remarks','locker_id','supplier_id','user_id'];

    public function goodsin()
    {
        return $this->belongsTo('App\Goodsin');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }
    public function locker()
    {
        return $this->belongsTo('App\Locker');
    }
    
    public function product_unit()
    {
        return $this->belongsTo('App\ProductUnit');
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function group()
    {
        return $this->belongsTo(Group::class,'group_id');
    }

    public function purchase()
    {
        return $this->belongsTo('App\Purchase');
    }

}
