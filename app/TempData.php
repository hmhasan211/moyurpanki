<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempData extends Model
{
    protected $fillable = ['product_id','unit_id','user_id', 'qty', 'price', 'note', 'type','last_consume','last_purchase_date','last_purchase_price'];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
