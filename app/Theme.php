<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    protected $fillable = [
        'user_id',
        'navbar_bg',
        'navbar_color',
        'sidebar_class',
        'logo_color'
    ];
}
