<?php

namespace App\Http\Controllers;
use App\Group;
use App\Product;
use App\Purchase;
use App\Department;
use App\ProductUnit;
use App\Requisition;
use App\TempData;
use App\RequisitionProduct;
use App\GoodsinProduct;
use App\GoodsIssueProduct;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class StockIssueController extends Controller
{
    public function index(){
        $date_today = date('Y-m-d');
        $items = Product::pluck('name','id');
        return view('inventory.stock_issue.add_stock_issue',compact('date_today','items'));
    }

    public function getSuppliers($product_id){

        //$total_goods_in = GoodsinProduct::where(get_where())->where('product_id',$product_id)->sum('qty') - GoodsIssueProduct::where(get_where())->where('product_id',$product_id)->sum('qty');

        $suppliers = GoodsinProduct::query()->where(get_where())->where('product_id',$product_id)->where('supplier_id', '!=', null)->get(['id','supplier_id']);
        $suppliers_array = array();
        if(isset($suppliers[0]->id)){
            foreach ($suppliers as $key => $value) {
                $suppliers_array[$value->supplier_id]['name'] = Supplier::find($value->supplier_id)->name;
                $suppliers_array[$value->supplier_id]['qty'] = GoodsinProduct::where(get_where())->where('product_id',$product_id)->where('supplier_id',$value->supplier_id)->sum('qty') - GoodsIssueProduct::where(get_where())->where('product_id',$product_id)->where('supplier_id',$value->supplier_id)->sum('qty');
            }
        }

        return view('inventory.stock_issue.get-suppliers',compact('suppliers_array'));

        // if($total_goods_in > 0){
        //     return response()->json(['success'=>true,'total_goods_in'=>$total_goods_in]);
        // }
        // return response()->json(['success'=>false]);
    }


    public function store(Request $request){
        //return $request->all();

        // if($request->issue_qty > 0){
        //     $stock_qty = GoodsinProduct::where(get_where())->where('product_id',$request->product_id)->sum('qty');
        //     if($request->issue_qty <= $stock_qty){
        //         GoodsIssueProduct::create([
        //             'unit_id' => auth_unit()??null,
        //             'user_id' => \Auth::id(),
        //             'product_id' => $request->product_id,
        //             'qty' => $request->issue_qty,
        //             'price' => null,
        //             'reference_no' => $request->reference_no,
        //             'issue_date' => $request->issue_date,
        //             'remarks' => $request->note,
        //         ]);
        //         session()->flash('success','Successfully stock issued !');
        //     }else{
        //         session()->flash('error','Issue Qty must be less than available Qty !');
        //     }
        // }else{
        //     session()->flash('error','Issue Qty must be greater than 0 !');
        // }
        // return back();

        if(!empty($request->input_qty)){
            $i = 0;
            $issued_qty = 0;
            foreach ($request->input_qty as $supplier_id => $qty) {
                if($qty > 0){
                    $i++;
                    $stock_qty = GoodsinProduct::where(get_where())->where('product_id',$request->product_id)->sum('qty');
                    if($qty <= $stock_qty){
                        GoodsIssueProduct::create([
                            'unit_id' => auth_unit()??null,
                            'user_id' => \Auth::id(),
                            'product_id' => $request->product_id,
                            'supplier_id' => $supplier_id,
                            'qty' => $qty,
                            'price' => null,
                            'reference_no' => $request->reference_no,
                            'issue_date' => $request->issue_date,
                            'remarks' => $request->note,
                        ]);
                        $issued_qty++;
                    }
                }
            }
            if($i == 0){
                session()->flash('error','Total Qty must be greater than 0');
            }else{
                if($issued_qty == 0){
                    session()->flash('error','Input Qty must be less than available Qty !');
                }else{
                    session()->flash('success','Goods Issued successfully for '.$issued_qty.' supplier !');
                }

            }
        }else{
            session()->flash('error','No supplier selected');
        }
        return back();
    }


}
