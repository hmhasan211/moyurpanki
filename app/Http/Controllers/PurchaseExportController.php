<?php

namespace App\Http\Controllers;

use PDF;
use App\Purchase;
use Illuminate\Http\Request;
use App\Exports\PurchaseOrderExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SupplierQuatationsExport;
use App\Exports\PurchaseRequisitionsExport;
use App\Exports\PurchaseSingleRequisitionExport;
use App\Exports\PurchaseRequisitionsAfterCheckExport;
use App\Exports\PurchaseRequisitionsAfterCheckSingleExport;

class PurchaseExportController extends Controller
{
    public function purchase_order_details_excel($PurchaseOrderId)
    {
        return Excel::download(new PurchaseOrderExport($PurchaseOrderId), 'Purchase Order.xlsx');
    }
    public function purchase_order_details_pdf($PurchaseOrderId)
    {
        $purchase = Purchase::find($PurchaseOrderId);
        $pdf = PDF::loadView('inventory.purchase.pdf-purchase-order', compact('purchase'));
        return $pdf->download('purchase-order.pdf');
    }

    public function purchase_requisitions_excel()
    {
        return Excel::download(new PurchaseRequisitionsExport(), 'Purchase Requisitions.xlsx');
    }

    public function purchase_requisitions_single_excel($requisitionId)
    {
        return Excel::download(new PurchaseSingleRequisitionExport($requisitionId), "Purchase Requisition no ".$requisitionId.".xlsx");
    }

    public function purchase_requisitions_after_check_single_excel($requisitionId)
    {
        return Excel::download(new PurchaseRequisitionsAfterCheckSingleExport($requisitionId), "After check Purchase Requisition no ".$requisitionId.".xlsx");
    }

    public function purchase_supplier_quatations_excel($requisitionId)
    {
        return Excel::download(new SupplierQuatationsExport($requisitionId), "Supplier Quatatiions of Req. no ".$requisitionId.".xlsx");
    }


    public function purchase_requisitions_after_check_excel()
    {
        return Excel::download(new PurchaseRequisitionsAfterCheckExport(), "After check Supplier Quatatiions.xlsx");
    }

}
