<?php

namespace App\Http\Controllers;

use App\Product;
use App\Purchase;
use App\Requisition;
use App\GoodsinProduct;
use App\User;
use App\Permission;
use App\Role;
use App\Theme;
use Auth;
use Illuminate\Http\Request;
use Route;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setTheme($content, $color){

        $theme = Theme::where('user_id', Auth::id())->first();
        if(!isset($theme->id)){
            $theme = Theme::create(['user_id'=>Auth::id()]);
        }
        if($content == 'navbar'){
            $nav_color = explode('&', $color)[0];
            $bg = explode('&', $color)[1];
            $theme->update(['navbar_bg'=>$bg,'navbar_color'=>$nav_color]);
        }

        if($content == 'dark_sidebar'){
            $main_color = explode('-',$color)[1];
            $side_bar_color = 'sidebar-dark-'.$main_color;
            $theme->update(['sidebar_class'=>$side_bar_color]);
        }

        if($content == 'light_sidebar'){
            $main_color = explode('-',$color)[1];
            $side_bar_color = 'sidebar-light-'.$main_color;
            $theme->update(['sidebar_class'=>$side_bar_color]);
        }

        if($content == 'logo'){
            $theme->update(['logo_color'=>$color]);
        }

        return response()->json(['success'=>true]);

    }

    public function addGroupId(){

        $data= GoodsinProduct::all();
        foreach ($data as $list){
            $product_id = $list->product_id;
            $group_id = $list->product ? $list->product->group_id : null;
            if($group_id !=null){
                $goodsInProduct = $list->update(['group_id'=>$group_id]);
            }
        }

    }
    public function index()
    {

        //  $routeName=[];
        // $allRoutes = Route::getRoutes();
        // //dd($allRoutes->nameList);
        // foreach($allRoutes as $key=>$route){
        //     if($route->getName()){
        //          $routeName[$key]['name']=$route->getName();
        //          $name=str_replace('-',' ',str_replace('_',' ',str_replace('.',' ',$route->getName())));
        //          $group=explode(" ",$name);
        //          $routeName[$key]['label']=$name;
        //          $routeName[$key]['label_group']=$group[0];
        //     }
        // }
        // //dd($routeName);
        // DB::table('permissions')->insert($routeName);

        $activeUser=Auth::user();
        if(!$activeUser->active_unit){
            $activeUser->active_unit=Auth::user()->units->first()->id;
            $activeUser->update();
        }
        $total_items= Product::get()->count();
        $total_requisitions = Requisition::get()->count();
        $total_stocks= 0; //GoodsinProduct::get()->sum('qty');
        $total_purchse_orders=Purchase::get()->count();
        return view('dashboard.index',compact('total_items','total_requisitions','total_stocks','total_purchse_orders'));
    }


    public function unitSetup(Request $request){
        $unitsPermittedForLoggedInUser=unit_lists();
        $settedUnit=$unitsPermittedForLoggedInUser->where('id',$request->unit_id)->first();
        $activeUser=User::find(Auth::user()->id);
        $activeUser->active_unit=$settedUnit->id;
        $activeUser->update();
        session()->flash('success','Unit has been Changed Successfully');
        return $activeUser;
    }
    public function admin_permission()
    {
        $permissions=Permission::pluck('id');
        if(Auth::user()->roles->first()->name=='ADMIN'){
            Auth::user()->roles->first()->permissions()->sync($permissions);
        }
    }
    public function assignPermissionToRole()
    {
        $permissions=Permission::all();
        $roles=Roles::all();
        return view('users.add-role',compact('roles','permissions'));
    }
    public function roles()
    {
        $permissions=Permission::all();
        $roles=Role::all();
        return view('users.add-role',compact('roles','permissions'));
    }

    public function storeRole(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles',
        ]);
        $role=new Role;
        $role->name=$request->name;
        $role->save();
        return redirect()->back()->with('success','Role sucessfully added and Now please Permission in that Role');
    }

    public function editRoles($id)
    {
        $permission_groups=Permission::all()->groupBy('label_group');
        $role=Role::with('permissions')->find($id);
        $roles=Role::all();
        //return $role->permissions->toArray();
        return view('users.edit-roles',compact('roles','role','permission_groups'));
    }
    public function updateRoles(Request $request,$id)
    {
        $role=Role::with('permissions')->find($id);
        $role->name=$request->name;
        $role->permissions()->sync($request->asignpermission);
        $role->update();
        return redirect()->back()->with('success','Role sucessfully Updated With Permissions');
    }
    public function index2()
    {
        return view('dashboard.index2');
    }

    public function index3()
    {
        return view('dashboard.index3');
    }
}
