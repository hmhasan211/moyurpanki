<?php

namespace App\Http\Controllers;

use App\GoodsIssueProduct;
use App\Group;
use App\Goodsin;
use App\Locker;
use App\Purchase;
use App\PurchaseProduct;
use App\Product;
use App\GoodsinProduct;
use App\QuotationProduct;
use App\Supplier;
use App\ProductUnit;
use App\RequisitionProduct;
use App\NewRequisitionProduct;
use App\NewRequisition;
use App\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Builder;
use File;

class GoodsinController extends Controller
{
    public function index()
    {
        //GoodsinProduct::where('supplier_id',null)->update(['supplier_id'=>111]);

        $units = ProductUnit::get();
        $lockers = Locker::all();
        $supplier = Supplier::all();
        $goods_in = GoodsinProduct::query()->where(get_where())->with('product', 'product.group')->get();
        $products = Product::all();
        $store = Unit::all();
        return view('inventory.goodsin.add-goodsin', compact('store','products', 'units', 'goods_in','lockers','supplier'));
    }

    public function edit($id)
    {
        $goods_in = GoodsinProduct::find($id);
        $units = ProductUnit::get();
        $products = Product::all();
        return view('inventory.goodsin.edit-goodsin', compact('units', 'products', 'goods_in'));
    }

    public function update(Request $request, $id)
    {
        //return $request->all();
        $goods_in = GoodsinProduct::find($id);
        $goods_in->update($request->all());

        session()->flash('success', 'Item Information Updated');
        return back();
    }

    public function delete($id)
    {
        $goods_in = GoodsinProduct::find($id)->delete();

        if ($goods_in) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function setUnit($item_id)
    {
        //$units = ProductUnit::get();
        $item_unit = Product::find($item_id)->product_unit_id;
        return response()->json([
            //'units'=>$units,
            'item_unit' => $item_unit,
        ]);
    }
    public function setDeck($locker_Id)
    {
        //$units = ProductUnit::get();
        $deck = Locker::find($locker_Id)->deck;
        return response()->json([
            //'units'=>$units,
            'deck' => $deck,
        ]);
    }

    public function newGoodsIn()
    {

        $unit = Auth::user()->active_unit;
        $products = Product::all();
        $purchases = Purchase::query()->with('requisition')->where('unit_id', $unit)->orderBy('id', 'desc')->get();

        return view('inventory.goodsin.new-goodsin', compact('purchases', 'products'));
    }

    public function getSupplier($purchase_id)
    {

        $purchase_product = PurchaseProduct::where('purchase_id', $purchase_id)->first();
        if ($purchase_product) {
            $supplier_id = $purchase_product->supplier_id;
            $supplier_name = Supplier::find($supplier_id)->name;
        } else {
            $supplier_name = 'No Name';
        }
        return response()->json(['supplier_name' => $supplier_name]);
    }

    public function mrReport()
    {

        $goods_in = Goodsin::query()->where(get_where())->orderBy('id', 'desc')->get();
        return view('inventory.goodsin.mr-report', compact('goods_in'));
    }

     public function mrReportDelete($id)
    {
        $goods_in = Goodsin::find($id);
        $delete_goods_in = $goods_in->delete();
        $delete_goods_in_product = GoodsinProduct::where('goodsin_id',$goods_in->id)->delete();

        if($delete_goods_in && $delete_goods_in_product){
            // check if any mrr for this purchase order is available or not
            $check_mrr = Goodsin::where('purchase_id',$goods_in->purchase_id)->first();
            if(!isset($check_mrr->id)){
                Purchase::find($goods_in->purchase_id)->update(['mrr_status'=>0]);
            }
            return response()->json(['success'=>true]);
        }
        return response()->json(['success'=>false]);
    }

    public function mrReportPrint($id)
    {

        $goods_in = Goodsin::with('products')->find($id);

        return view('inventory.goodsin.mr-report-print', compact('goods_in'));
    }

    public function store(Request $request)
    {

//        $request->validate([
//            'title'=>'required',
//            'Description'=>'required',
//            'image'=>'required','image|mimes:jpeg,png,jpg,gif,svg',
//            'category'=>'required',
//        ]);
       $last_id= GoodsinProduct::orderBy('id','DESC')->first();
        if(empty($last_id)){
            $last_id=1;
        }else{
            $last_id=$last_id->id+1;
        }

     if($request->has('image')){
            $date= date("FY");
        $file=$request->file('image');
        $imageName = $date.'/'.$request->item.'_goodsIn_'.($last_id).'.'.$file->getClientOriginalExtension();
        if(!File::exists('images/goods_in/'.$date)) {
            File::makeDirectory('images/goods_in/'.$date,0777,true);
        }
        $destinationPath = 'images/goods_in/'.$date;
        $file->move($destinationPath, $imageName);
     }else{
         $imageName=null;
     }

        $product = Product::query()->findOrFail($request->item);

        $data['goodsin_id'] = 0;
        $data['product_id'] = $request->item;
        $data['purchase_order_no'] = $request->purchase_order;
//        $data['group_id'] = $product->category_id != null ? $product->category_id : null;
        $data['group_id'] = 1;
        $data['sub_group_id'] = 1;
//        $data['unit_id'] = auth_unit()?? null;
        $data['unit_id'] = $request->department_id?$request->department_id:auth_unit();
        $data['product_unit_id'] = $request->product_unit_id;
        $data['qty'] = $request->qty;
        $data['price'] = $request->price;
        $data['image'] = $imageName;
        $data['remarks'] = $request->remarks;
        $data['locker_id'] = $request->locker_id;
        $data['supplier_id'] = $request->supplier_id;

        $data['user_id'] = Auth::id();
        $goods_in_product = GoodsinProduct::create($data);


//        GoodsinProduct::insert([
//            'goodsin_id' => 0,
//            'unit_id' => auth_unit()?? null,
//            'product_id' => $request->item,
//            'product_unit_id' => $request->unit_id,
//            'qty' => $request->qty,
//            'price' => $request->price,
//            'user_id' => Auth::id(),
//            'created_at' => date('Y-m-d H:i:s'),
//        ]);
//


        /*Goods In Product Save End */

        session()->flash('success', 'Goods in Created successfully');
        return redirect()->back();
    }


    public function storeNew(Request $request)
    {

//        dd($request->all());
        $total_receive_qty = 0;
        $qty_greater_than_zero = 0;
        $name_of_products = '';
        foreach ($request->receive_qty as $productKey => $data) {
            $purchase_product = PurchaseProduct::query()->where('purchase_id', $request->purchase_id)->where('product_id',$request->product_id[$productKey])->first();
            if($data > $purchase_product->qty){
                $qty_greater_than_zero++;
                $name_of_product =  Product::find($request->product_id[$productKey])->name;
                $name_of_products .= ''.$name_of_product.', ';
            }
            $total_receive_qty += intval($data);
        }

        if($qty_greater_than_zero > 0){
            session()->flash('error', 'Greater value given for '.$name_of_products);
            return redirect()->back();
        }

        // Rezaul Hoque 11/01/2021
        // this option modify for mothin vai requirement.0.5 value not working for this logic if ($total_receive_qty > 0) { so i changed logic 

        // if ($total_receive_qty > 0) {

        //     $unit = auth_unit()?? null;

        //     $request['user_id'] = Auth::id();
        //     $request['unit_id'] = $unit;
        //     $purchase = Purchase::query()->find($request->purchase_id);

        //     //$result = Goodsin::query()->where('unit_id', $unit)->get()->last();
        //     $result = Goodsin::query()->where('unit_id', $unit)->max('mrr_no');

        //     if($result != 0){
        //         $request['mrr_no'] = $result +1;
        //     }else{
        //         $request['mrr_no'] =1;
        //     }


        //     $goods_in = Goodsin::create($request->all());
        //     $purchase->update(['mrr_status'=>1]);
        //     $no_of_goods_in = 0;

        //     foreach ($request->product_id as $key => $product_id) {
        //         $purchase_product = PurchaseProduct::query()->where('purchase_id', $purchase->id)->where('product_id',$product_id)->first();
        //         if ($request->receive_qty[$key] > 0 && $request->receive_qty[$key] <= $purchase_product->qty) {
        //             $product = Product::query()->findOrFail($product_id);
        //             $data = new GoodsinProduct;
        //             $data->unit_id = $unit;

        //             $data->goodsin_id = $goods_in->id;
        //             $data->product_id = $product_id;
        //             $data->group_id = $product->group_id;
        //             $data->qty = $request->receive_qty[$key];
        //             $data->price = $request->price[$key];
        //             $data->purchase_id = $goods_in->purchase_id;
        //             $data->remarks = $request->remarks[$key];
        //             $data->supplier_id = $purchase->supplier_id;

        //             $data->requisition_id = $purchase->requisition_id;
        //             $data->product_unit_id = $product->product_unit_id;
        //             $data->save();

        //             $no_of_goods_in++;
        //         }

        //     }
        //     /*Goods In Product Save End */

        //     session()->flash('success', 'Goods in Created successfully');
        //     return redirect()->back();
        // } else {
        //     session()->flash('error', 'Nothing to Add');
        //     return redirect()->back();
        // }
        
        
            // Rezaul Hoque 11/01/2021
            // this option modify for mothin vai requirement.0.5 value not working for this logic if ($total_receive_qty > 0) { so i changed logic
        
            $unit = auth_unit()?? null;

            $request['user_id'] = Auth::id();
            $request['unit_id'] = $unit;
            $purchase = Purchase::query()->find($request->purchase_id);

            //$result = Goodsin::query()->where('unit_id', $unit)->get()->last();
            $result = Goodsin::query()->where('unit_id', $unit)->max('mrr_no');

            if($result != 0){
                $request['mrr_no'] = $result +1;
            }else{
                $request['mrr_no'] =1;
            }


            $goods_in = Goodsin::create($request->all());
            $purchase->update(['mrr_status'=>1]);
            $no_of_goods_in = 0;

            foreach ($request->product_id as $key => $product_id) {
                $purchase_product = PurchaseProduct::query()->where('purchase_id', $purchase->id)->where('product_id',$product_id)->first();
                if ( $request->receive_qty[$key] <= $purchase_product->qty) {
                    $product = Product::query()->findOrFail($product_id);
                    $data = new GoodsinProduct;
                    $data->unit_id = $unit;

                    $data->goodsin_id = $goods_in->id;
                    $data->product_id = $product_id;
                    $data->group_id = $product->group_id;
                    $data->qty = $request->receive_qty[$key];
                    $data->price = $request->price[$key];
                    $data->purchase_id = $goods_in->purchase_id;
                    $data->remarks = $request->remarks[$key];
                    $data->supplier_id = $purchase->supplier_id;

                    $data->requisition_id = $purchase->requisition_id;
                    $data->product_unit_id = $product->product_unit_id;
                    $data->save();

                    $no_of_goods_in++;
                }

            }
            /*Goods In Product Save End */

            session()->flash('success', 'Goods in Created successfully');
            return redirect()->back();
   


    }

    // public function lcCost(){
    //     $lcs = Goodsin::pluck('lc','id');
    //     $costs = Cost::pluck('name','id');
    //     $lccosts = CostGoodsin::all();
    //     return view('inventory.goodsin.lc-cost',compact('lcs','costs','lccosts'));
    // }

    // public function storeLcCost(Request $request){

    //     $this->validate($request,[
    //         'cost_id'=>'required',
    //         'goodsin_id'=>'required'
    //     ]);

    //     CostGoodsin::create($request->all());
    //     session()->flash('success','LC Costing save successfully Complete');
    //     return redirect()->route('lc.cost');
    // }


    /* LC Report start */
    public function lcReport(){
        $lcs = Goodsin::pluck('lc','id');
        return view('inventory.report.lc-report',compact('lcs'));
    }
    /* LC Report End */



    /* Individual LC Report Findout Start */
    // public function lcReportFind(Request $request){
    //     $id = $request->goodsin_id;
    //     $goodsin = Goodsin::findOrFail($id);
    //     $sl=0;
    //     foreach ($goodsin->costs as $key=>$info){
    //         $data['sl'][$key] = ++$sl;
    //         $data['lc'][$key] = $goodsin->lc;
    //         $data['cost'][$key] = $info->cost->name;
    //         $data['amount'][$key] = $info->amount;
    //         $data['time'][$key] = $info->created_at->diffForHumans();
    //     }
    //     return json_encode(['success'=>$data]);
    // }

    /* Individual LC Report Findout End*/


//    public function findPurchaseProducts2(Request $request){
//        $purchase = Purchase::find($request->purchase_id);
//        if($purchase->quotation_id == 0){
//            $purchase_products = PurchaseProduct::where('purchase_id',$purchase->id)->get(['id','quotation_id']);
//            $quotation_array = array();
//            if(isset($purchase_products[0]->id)){
//                foreach ($purchase_products as $key => $value) {
//                    array_push($quotation_array, $value->quotation_id);
//                }
//            }
//
//            if(!empty($quotation_array)){
//                $products = QuotationProduct::whereIn('id',$quotation_array)->get();
//            }
//        }else{
//            $products = NewRequisitionProduct::where('new_requisition_id',$purchase->requisition_id)->get();
//        }
//
//        $sl =0;
//        $html ='';
//
//        if(count($products)>0){
//            return view('inventory.goodsin.product-list', compact('products','purchase'));
//        }else{
//            return $html.='<tr><td colspan="11" class="text-center bg-danger">No Product Information Found</td></tr>';
//        }
//    }

    public function findPurchaseProducts(Request $request){
        $purchase = Purchase::query()->find($request->purchase_id);
        $html ='';

        if($purchase !=null){
            return view('inventory.goodsin.product-list', compact('purchase'));
        }else{
            return $html.='<tr><td colspan="11" class="text-center bg-danger">No Product Information Found</td></tr>';
        }
    }

    public function goodsin_history(Request $request)
    {
        $goodsin_products = GoodsinProduct::where(get_where())->get();
        return view('inventory.goodsin.goodsin-history',compact('goodsin_products'));
    }

    public function stock_summary()
    {
        $groups = Group::query()->whereNotIn('id',[152.153,168,171])->pluck('name','id');
        if (Input::get('group_id') != null && Input::get('group_id')) {
            $group_id = Input::get('group_id');
            $group_name = Group::query()->findOrFail($group_id)->name;
            $stocks = GoodsinProduct::query()->where(get_where())->where('group_id',$group_id)->with('product','product.group')->get()->groupBy('product_id');
            $dataArray =[];
            foreach ($stocks as $product=>$stock){

                $stocksIssue = GoodsIssueProduct::query()->where(get_where())->where('product_id',$product)->get();

                $dataArray[] =[
                    'product_id'=>$stock->first()->product->id,
                    'name'=>$stock->first()->product->name,
                    'unit'=>optional($stock->first()->product->unit)->name,
                    'qty'=> $stock->sum('qty') - $stocksIssue->sum('qty'),
                    'price'=>$stock->sum('price')/count($stock),
//                    'value'=>$stock->sum('qty')* $stock->sum('price'),
                ];

            }
            $stocks = collect($dataArray);

        }else{
            $group_id = '';
            $stocks = [];
            $group_name = '';
        }

        return view('inventory.goodsin.stock-summary',compact('groups','stocks','group_name','group_id'));
    }

    //Sakib
    public function stockRegister(Request $request)
    {
        $payment_method = '';
        $today_date = $from = $to = date('Y-m-d');
        $items = Product::pluck('name','id');
        $suppliers = Supplier::pluck('name','id');
        $goodsin_products = [];
        $item_name = 'All Items';
        $item = '';
        $supplier = '';
        $unit_name = '';
        
        if($request->from != null && $request->to != null){
            $payment_method = $request->payment_method;
            $from = $request->from;
            $to = $request->to;
            $goodsin_products = GoodsinProduct::where(\DB::raw('substr(`created_at`,1,10)'), '>=', $from)->where(\DB::raw('substr(`created_at`,1,10)'),'<=',$to)
                ->when($payment_method == '1' || $payment_method == '2', function($query) use ($payment_method){
                    return $query->whereHas('purchase', function ($query) use ($payment_method){
                        return $query->where('payment_type', $payment_method);
                    });
                })
                ->whereHas('goodsin', function ($query){
                    return $query->where(get_where());
                })->get();
            if($request->product_id != null){
                $item = $request->product_id;
                $item_name = Product::find($request->product_id)->name;
                $goodsin_products = $goodsin_products->where('product_id',$request->product_id);
                if($request->supplier_id != null){
                    $supplier = $request->supplier_id;
                    $goodsin_products = $goodsin_products->where('supplier_id',$request->supplier_id);
                }
            }else{
                if($request->supplier_id != null){
                    $supplier = $request->supplier_id;
                    $goodsin_products = $goodsin_products->where('supplier_id',$request->supplier_id);
                }
            }
        }
        return view('inventory.goodsin.stock-register',compact('goodsin_products','today_date','items','suppliers','from','to','item_name','unit_name','item','supplier','payment_method'));
    }

    public function stockTransaction(Request $request)
    {
        $today_date = $from = $to = date('Y-m-d');
        $items = Product::pluck('name','id');
        $suppliers = Supplier::pluck('name','id');
        $goodsin_products = [];
        $item_name = 'All Items';
        $item = '';
        $supplier = '';
        $unit_name = '';
        if($request->from != null && $request->to != null){
            $from = $request->from;
            $to = $request->to;
            $goodsin_products = GoodsinProduct::where('created_at','<=', $request->to)->where('created_at','>=',$request->from)->get();
            if($request->product_id != null){
                $item = $request->product_id;
                $item_name = Product::find($request->product_id)->name;
                $goodsin_products = $goodsin_products->where('product_id',$request->product_id);
                if($request->supplier_id != null){
                    $supplier = $request->supplier_id;
                    $goodsin_products = $goodsin_products->where('supplier_id',$request->supplier_id);
                }
            }else{
                if($request->supplier_id != null){
                    $supplier = $request->supplier_id;
                    $goodsin_products = $goodsin_products->where('supplier_id',$request->supplier_id);
                }
            }
        }
        return view('inventory.goodsin.stock-register',compact('goodsin_products','today_date','items','suppliers','from','to','item_name','unit_name','item','supplier'));
    }
    

    public function all_categories_stock_summary()
    {
        $category_id = 0;
        $categories = Group::with('products','products.stock')->whereHas('products.stock',function($q){
            $q->where('product_id','!=',null);
        })->get();
        //dd($categories);
        $groups=GoodsinProduct::where(get_where())->get()->groupBy('product_id');
        //dd($groups);
        // foreach($groups as $key=>$products){
        //     $qty=0;
        //     $value=0;
        //        foreach($products as $stock){
        //             $qty+=$stock->qty;
        //             $value+=($stock->price*$stock->qty);
        //             // $key=$stock->product->group_id;
        //        }
        //    $stock_qty[$key] = $qty;
        //    $stock_value[$key] = $value;
        // }
        //dd($data);
        return view('inventory.goodsin.all_categories_stock_summary', compact('category_id','categories','groups'));
    }
    public function category_wise_stock_summary($category_id)
    {
        $categories = Group::with('products','products.stock')->whereHas('products.stock',function($q){
            $q->where('product_id','!=',null);
        })->get();
        //dd($categories);

        $products = Product::when($category_id != '0', function ($query) use ($category_id){
            return $query->where('group_id',$category_id);
        })->get();
        $products_array = array();

        if(isset($products[0]->id)){
            foreach ($products as $key => $value) {
                array_push($products_array, $value->id);
            }
        }

        $groups=GoodsinProduct::where(get_where())->whereIn('product_id',$products_array)->get()->groupBy('product_id');
        return view('inventory.goodsin.all_categories_stock_summary', compact('category_id','categories','groups'));
    }

    public function category_wise_stock_summary_print($category_id)
    {
        $categories = Group::with('products','products.stock')->whereHas('products.stock',function($q){
            $q->where('product_id','!=',null);
        })->get();
        //dd($categories);

        $products = Product::when($category_id != '0', function ($query) use ($category_id){
            return $query->where('group_id',$category_id);
        })->get();
        $products_array = array();

        if(isset($products[0]->id)){
            foreach ($products as $key => $value) {
                array_push($products_array, $value->id);
            }
        }
        $category_name = 'All Categories';
        if($category_id != '0'){
            $category_name = Group::find($category_id)->name;
        }

        $groups=GoodsinProduct::where(get_where())->whereIn('product_id',$products_array)->get()->groupBy('product_id');
        return view('inventory.goodsin.all_categories_stock_summary_print', compact('category_id','category_name','categories','groups'));
    }
    
}
