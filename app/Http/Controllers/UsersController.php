<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        $roles=Role::where('id','!=',1)->get();
       //return $roles;
        return view('employee.add-employee',compact('users','roles'));
    }

    public function store(Request $request)
    {
        $data = $request->except('image');
        if($request->email){
            $request->validate([
                'email' => 'required|unique:users',
                'password' => 'required',
                'name' => 'required',
            ]);
            $filename='public/demo/employee.jpg';
        
            if($request->file('image')){
                $image = $request->file('image');
                $filename = Date('Y')."_".substr(md5(time()),0,6).".".$image->getClientOriginalExtension();           
                $image->move(base_path('public/admin/employee/upload/'),$filename);
            }

            $user=new User;
            $user->name=$request->name;
            $user->email=$request->email;
            $user->image=$filename;
            $user->password=bcrypt($request->password);
            $user->role_id=$request->role; //'employee'
            $user->save(); 
            $data['user_id']=$user->id;
        }
        
       // User::create($data);
        session()->flash('success','Employee has store Successfully complete');
        return redirect()->route('employee.add');
    }

    public function show($id)
    {
        $roles=Role::all();
        $user = User::find($id);
        return view('employee.edit-employee',compact('user','roles'));
    }

    public function edit($id)
    {

    }

    public function update(Request $request)
    {
        //dd($request->all());
        $filename=null;
       // $employee = User::find($request->id);
        // if($request->has('image')){
        //     $path = "public/admin/employee/upload/".$employee->image;
        //     unlink($path);
        //     $image = $request->file('image');
        //     $filename = Date('Y')."_".substr(md5(time()),0,6).".".$image->getClientOriginalExtension();
        //     $data = $request->except('image');
        //     $data['image'] = $filename;
        //     $image->move(base_path('public/admin/employee/upload/'),$filename);
        //    $employee->update($data);
        // }

            if($request->id){
                $user= User::find($request->id);
                $user->name=$request->name;
                $user->email=$request->email;
                $user->image=$filename??$user->image;
                $user->password=$request->npassword ? bcrypt($request->npassword):$user->password;
                $user->update(); 
                $user->roles()->sync($request->role);
            }

        session()->flash('success','User has update Successfully complete');
        return redirect()->route('user.add');
    }

    public function destroy(Request $request)
    {
        $employee = User::find($request->id);
        $path = "public/admin/employee/upload/".$employee->image;
        unlink($path);
        $employee->delete();
    }
}
