<?php

namespace App\Http\Controllers\Inventory;

use App\GoodsinProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InventoryInStokController extends Controller
{
    public function index() {
        $goods_in = GoodsinProduct::query()->with('locker','product')->where(get_where())->orderBy('id', 'desc')->get();
        return view('inventory.stock.index',compact('goods_in'));
    }
}
