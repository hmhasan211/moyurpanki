<?php

namespace App\Http\Controllers\Inventory;

use App\Group;
use App\Product;
use App\Department;
use App\ProductUnit;
use App\Brand;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        $items = Product::all();
        $departments = Department::all()->pluck('name','id');
        $groups = Group::all()->pluck('name','id');
        $units = ProductUnit::all()->pluck('name','id');
        $brands = Brand::all()->pluck('name','id');
        return view('inventory.item.add-item',compact('items','departments','groups','units','brands'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:products',
            //'item_code'=>'required|unique:products',
            'product_unit_id'=>'required',
            // 'department_id'=>'required',
            'group_id'=>'required',
            'brand_id'=>'required',
            //'price'=>'required',
        ]);

//        dd($request->all());
        $item =  Product::query()->orderBy('created_at','DESC')->first();

//        if($item !=null){
//         $item_code = intval(explode("-",$item->item_code)[1])+1;
//        }else{
//          $item_code = 1001;
//        }
//        dd($request->all());
//        $request['item_code']=$request->group_id."-".$item_code;
        $request['name']=strtoupper($request['name']);

//        if ($request->item_code!=null){
//            $ritemcode=$request->item_code;
//        }else{
//            $ritemcode=$request->group_id."-".$item_code;
//        }

        $data=[
            'item_code'=>$request->item_code,
            'name'=>$request->name,
            'group_id'=>$request->group_id,
            'product_unit_id'=>$request->product_unit_id,
            'description'=>$request->description,
            'sub_group_id'=>$request->sub_group_id,
            'size'=>$request->size,
            'color'=>$request->color,
            'brand_id'=>$request->brand_id,
            'created_at'=>Carbon::now(),
        ];
        Product::query()->insert($data);
        session()->flash('success','Item information successfully stored');
        return redirect()->back()->with('success','Product Added Successfully');
    }

    public function edit($id){
        $items = Product::all();

        $departments = Department::all()->pluck('name','id');
        $groups = Group::all()->pluck('name','id');
        $units = ProductUnit::all()->pluck('name','id');
        $brands = Brand::all()->pluck('name','id');
        $item = Product::query()->findOrFail($id);

     //   dd($item->unit);

        return view('inventory.item.edit-item-inventory',compact('items','departments','groups','units','item','brands'));

    }

    public function update(Request $request,$id){
        Product::query()->findOrFail($id)->update($request->all());
        session()->flash('success','Item information successfully updated');
        return redirect()->route('inventory.item.add');
    }

    public function destroy(Request $request){
        Product::query()->findOrFail($request->id)->delete();
    }

    /* department wise Product List start */

    public function groupWiseProducts(Request $request){
        $items = ($request->group_id !=null ? Product::where('group_id',$request->group_id)->get() : Product::all() );
        return view('inventory.item.items-inventory', compact('items'));
        // $data = [];
        // $sl=0;
        //     foreach ($products as $key=>$product){
        //         $data['sl'][$key] = ++$sl;
        //         $data['name'][$key] = $product->name;
        //         $data['code'][$key] = $product->item_code;
        //         $data['group'][$key] = $product->group_id !=null ? $product->group->name : "N/A";
        //         $data['unit'][$key] = $product->product_unit_id !=null ? $product->unit->name : "N/A";
        //         $data['department'][$key] = $product->department_id !=null ? $product->department->name : "N/A";
        //         $data['price'][$key] = $product->price;
        //         $data['description'][$key] = $product->description;

        //         $data["edit"][$key] =  '<a href="'.route('inventory.item.edit',$product->id).'" class="btn btn-edit btn-success far fa-edit"></a>  <button type="button" data-id="'.$product->id.'" data-url="'.route('inventory.item.destroy').'" class="btn btn-danger fas fa-trash-alt erase"></button>';
        //     }
        //     return $data;

    }

    /* department wise Product List End */

    public function search_product_item(Request $request)
    {
        $item = request('searchTerm');
        $items =  Product::query()
            ->where('name','like',"%".$item."%")
            ->orWhere('item_code','like',"%".$item."%")
            ->take(100)
            ->get();
           return  $items;
    }
}
