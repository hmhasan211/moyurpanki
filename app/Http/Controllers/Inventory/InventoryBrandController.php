<?php

namespace App\Http\Controllers\Inventory;

use App\ProductUnit;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class InventoryBrandController extends Controller
{
    public function index(){
        $brands = Brand::all();
        return view('inventory.brand.add-brand-inventory',compact('brands'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:brands'
        ]);

        Brand::create($request->all());
        session()->flash('success','Inventory brand Successfully stored in ERP System');
        return redirect()->route('inventory.brand.add');
    }

    public function edit($id){
        $brand = Brand::findOrFail($id);
        $brands = Brand::all();
        return view('inventory.brand.edit-brand-inventory',compact('brand','brands'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>['required',Rule::unique('brands')->ignore($id)]
        ]);
        Brand::findOrFail($id)->update($request->all());
        session()->flash('success','Inventory Unit Successfully updated in ERP System');
        return redirect()->route('inventory.brand.add');

    }

    public function destroy(Request $request){
        Brand::findOrFail($request->id)->delete();
    }
}
