<?php

namespace App\Http\Controllers\Inventory;

use App\Group;
use App\Unit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubGroup;
use Illuminate\Validation\Rule;

class InventoryGroupController extends Controller
{
    public function index(){
        $groups = Group::with('department')->get();
        $units = Unit::all();
        return view('inventory.group.add-group-inventory',compact('groups','units'));
    }

    public function subGroup(){
        $subgroups = SubGroup::all();
        $groups = Group::all();
        return view('inventory.group.add-sub-group-inventory',compact('groups','subgroups'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:groups',
            'unit_id'=>'required',
        ]);
                $data=[
                    'name'=>$request->name,
                    'unit_id'=>$request->unit_id,
                    'description'=>$request->description,
                    'created_at'=>Carbon::now(),
                ];
                Group::insert($data);

        // Group::create($request->all());

        session()->flash('success','Inventory Group Successfully stored in ERP System');
        return redirect()->route('inventory.group.add');
    }

    public function subStore(Request $request){
        $data=[
            'name'=>$request->name,
            'group_id'=>$request->sub_group_id,
            'description'=>$request->description,
        ];
        SubGroup::insert($data);
        session()->flash('success','Inventory Group Successfully stored in ERP System');
        return redirect()->back();
    }

    public function get_subcategory($id){

       $sub_group= SubGroup::where('group_id',$id)->get();
       return response()->json($sub_group);
    }
    public function get_category($id){
       $group= Group::where('unit_id',$id)->get();
       return response()->json($group);
    }

    public function edit($id){
        $group = Group::findOrFail($id);
        $groups = Group::all();
         $units = Unit::all();
        return view('inventory.group.edit-group-inventory',compact('group','groups','units'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>['required',Rule::unique('groups')->ignore($id)]
        ]);
        Group::findOrFail($id)->update($request->all());
        session()->flash('success','Inventory Group Successfully updated in ERP System');
        return redirect()->route('inventory.group.add');

    }

    public function destroy(Request $request){
        Group::findOrFail($request->id)->delete();
    }
}
