<?php

namespace App\Http\Controllers\Locker;

use App\Deck;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Locker;
use Carbon\Carbon;

class LockerController extends Controller
{
    public function index(){
        $lockers = Locker::orderBy('id','DESC')->get();
        $decks = Deck::all();
        return view('inventory.locker.index',compact('lockers','decks'));
    }
    public function store(Request $r){
        $data=[
            'name'=>$r->name,
            'deck_id'=>$r->deck_id,
            'description'=>$r->description,
            'created_at'=>Carbon::now()
        ];
        Locker::insert($data);
        return redirect()->back();
    }
}
