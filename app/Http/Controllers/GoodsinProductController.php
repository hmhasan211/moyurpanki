<?php

namespace App\Http\Controllers;

use App\GoodsinProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoodsinProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GoodsinProduct  $goodsinProduct
     * @return \Illuminate\Http\Response
     */
    public function show(GoodsinProduct $goodsinProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GoodsinProduct  $goodsinProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(GoodsinProduct $goodsinProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GoodsinProduct  $goodsinProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GoodsinProduct $goodsinProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GoodsinProduct  $goodsinProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(GoodsinProduct $goodsinProduct)
    {
        //
    }
}
