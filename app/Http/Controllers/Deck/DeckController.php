<?php

namespace App\Http\Controllers\Deck;

use App\Deck;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DeckController extends Controller
{
    public function index(){
        $decks = Deck::orderBy('id','DESC')->get();
        return view('inventory.deck.index',compact('decks'));
    }
    public function store(Request $r){
        $data=[
            'name'=>$r->name,
            'description'=>$r->description,
            'created_at'=>Carbon::now()
        ];
        Deck::insert($data);
        return redirect()->back();
    }
}
