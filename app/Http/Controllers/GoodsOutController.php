<?php

namespace App\Http\Controllers;

use App\GoodsinProduct;
use App\Locker;
use App\Product;
use App\ProductUnit;
use App\Supplier;
use App\Unit;
use Illuminate\Http\Request;

class GoodsOutController extends Controller
{
    public function index (){
        $units = ProductUnit::get();
        $lockers = Locker::all();
        $supplier = Supplier::all();
        $goods_in = GoodsinProduct::query()->where(get_where())->with('product', 'product.group')->get();
        $products = Product::all();
        $store = Unit::all();
        return view('inventory.goodsin.goods_out',compact('store','products', 'units', 'goods_in','lockers','supplier'));
    }
}
