<?php

namespace App\Http\Controllers;

use App\NewRequisition;
use App\NewRequisitionProduct;
use App\Requisition;
use App\RequisitionProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RequisitionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function pending_requisition(){

        $requisitions = Requisition::query()->where(get_where())->get();
        $pending_requisitions =[];
        foreach ($requisitions as $requisition){
            if(!$requisition->new_requisition->isEmpty()){
                $requisition_total_qty = $requisition->requisition_products->sum('qty');
                $approved_requisitions = $requisition->new_requisition;
                $approved_requisition_total_qty = 0;
                foreach ($approved_requisitions as $approved_requisition){
                    $approved_requisition_total_qty += $approved_requisition->new_requisition_products->sum('qty');
                }

                $is_remain =  $requisition_total_qty -$approved_requisition_total_qty;
                if($is_remain >0){
                    $pending_requisitions[] =[
                        'id'=>$requisition->id,
                        'requisition_total_qty'=>$requisition_total_qty,
                        'approved_requisition_total_qty'=>$approved_requisition_total_qty,
                    ];

                }
            }

        }
        $data = collect($pending_requisitions);

        return view('requisition.pending',compact('data'));
    }

    public function pending_requisition_product($id){

        $requisition = Requisition::query()->findOrFail($id);

        return view('requisition.pending-requisition-product',compact('requisition'));
    }

    public function approve_pending_requisition(Request $request,$id){
        if (isset($request->requisition_product_id)){

            $new_requisition = NewRequisition::query()->create([
                'requisition_id' => $id,
                'cs_no' => getCSNumber(auth_unit()),
                'invoice_no' => substr(md5(time()),0,6)."1",
                'note' => $request->checking_note,
                'user_id' => Auth::id(),
                'unit_id' => auth_unit()?? null,
                'sent_for' => $request->sent_for,
                'purchased_status' => 1,
                'check_user_id' => Auth::user()->id,

            ]);

            foreach ($request->requisition_product_id as $key => $requisition_product_table_id) {
                //update requisition product table
                DB::table('requisition_products')
                    ->where('id', $requisition_product_table_id)
                    ->update(['status' => 1]);

                $requisition_product = RequisitionProduct::query()->find($requisition_product_table_id);

                NewRequisitionProduct::query()->create([
                    'new_requisition_id' => $new_requisition->id,
                    'product_id' => $requisition_product->product_id,
                    'qty' => $request['qty'][$key] != null ? $request['qty'][$key] : $requisition_product->qty,
                    'user_id' => Auth::id(),
                    'sent_for' => $request->sent_for,
                ]);


            }


        }else{
            session()->flash('error','Please check at least one product ');
            return redirect()->back();
        }

        return redirect()->route('inventory.purchase-requisition.view');


    }

}
