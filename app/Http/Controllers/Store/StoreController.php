<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Unit;
use Carbon\Carbon;

class StoreController extends Controller
{
    public function index(){
        $units = Unit::orderBy('id','DESC')->get();
        return view('department.index',compact('units'));
    }

    public function store(Request $r){
        $data=[
            'name'=>$r->name,
            'description'=>$r->description,
            'created_at'=>Carbon::now()
        ];
        Unit::insert($data);
        return redirect()->back();
    }
    // public function edit(id){

    // }
}
