<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locker extends Model
{
    public function deck(){
        return $this->belongsTo(Deck::class);
    }
}
