<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewRequisition extends Model
{
    protected $fillable = ['requisition_id','cs_no','invoice_no', 'qty', 'price', 'note', 'custom_date','check_user_id', 'user_id', 'unit_id','sent_for','purchased_status'];

    public function new_requisition_products(){
        return $this->hasMany(NewRequisitionProduct::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function requisition(){
        return $this->belongsTo(Requisition::class,'requisition_id','id');
    }

    public function checkedBy(){
        return $this->belongsTo(User::class,'check_user_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function quotations()
    {
        return $this->hasMany('App\Quotation', 'requisition_id', 'id');
    }

}
