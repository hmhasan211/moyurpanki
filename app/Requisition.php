<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisition extends Model
{
    protected $fillable = ['requisition_id','invoice_no', 'qty', 'price', 'note', 'custom_date','check_user_id', 'user_id', 'unit_id','sent_for','purchased_status'];

    public function requisition_products(){
        return $this->hasMany(RequisitionProduct::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function new_requisition(){
        return $this->hasMany(NewRequisition::class,'requisition_id');
    }

    public function checkedBy(){
        return $this->belongsTo(User::class,'check_user_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function quotations()
    {
        return $this->hasMany('App\Quotation');
    }

}
