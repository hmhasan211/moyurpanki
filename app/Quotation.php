<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = ['requisition_id', 'supplier_id','user_id', 'price', 'qty', 'status','note'];

    public function quotationProduct(){
        return $this->hasMany(QuotationProduct::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }

    public function purchase(){
        return $this->hasOne(Purchase::class,'requisition_id','requisition_id')->where('supplier_id',$this->supplier_id);
    }

    public function purchase_products()
    {
        return $this->hasManyThrough(PurchaseProduct::class, QuotationProduct::class,'quotation_id','quotation_id','id','id');
    }
}
