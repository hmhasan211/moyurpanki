<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequisitionProduct extends Model
{
    protected $fillable = ['price', 'qty','checked_qty','quotation_qty', 'note', 'requisition_id', 'user_id', 'product_id','sent_for','disabled','status'];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function requisition(){
        return $this->belongsTo(Requisition::class,'requisition_id');
    }

    public function new_requisition(){
        return $this->belongsTo(NewRequisition::class,'requisition_id','requisition_id');//Should be hasMany
    }

    public function calculate_approve_qty($requisition,$product){

        if(!$product->requisition->new_requisition->isEmpty()){
            $approved_qty =0;
            foreach ($product->requisition->new_requisition as $new_requisition){
                $data =  $new_requisition ? $new_requisition->new_requisition_products ? $new_requisition->new_requisition_products->where('product_id',$product->product_id)->first():null: null;
                if($data!=null){
                    $approved_qty +=$data->qty;
                }else{
                    $approved_qty +=0;
                }
            }
        }else{
            $approved_qty=0;
        }


        return $approved_qty;
    }



}
