<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['item_code','name','description','department_id','product_unit_id','group_id','price','brand_id','last_purchase_price','last_purchase_date','last_purchase_qty','last_purchase_order_no'];

    public function department(){
        return $this->belongsTo(Department::class,'department_id');
    }

    public function group(){
        return $this->belongsTo(Group::class,'group_id');
    }

    public function unit(){
        return $this->belongsTo(ProductUnit::class,'product_unit_id');
    }
    public function brand(){
        return $this->belongsTo(Brand::class,'brand_id');
    }
    public function stock(){
        return $this->hasMany(GoodsinProduct::class,'product_id');
    }
}
