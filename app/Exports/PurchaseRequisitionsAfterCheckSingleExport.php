<?php

namespace App\Exports;

use App\Purchase;
use App\Requisition;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Exports\PurchaseRequisitionsExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PurchaseRequisitionsAfterCheckSingleExport implements FromView , ShouldAutoSize
{
    protected $requisitionId;
    public function __construct($requisitionId)
    {
        $this->requisitionId = $requisitionId;
    }

    public function view(): View
        {
            return view('inventory.requisition.view-approved-requisition-items-excel', [
                'requisition' =>  Requisition::find($this->requisitionId)
            ]);
        }
}