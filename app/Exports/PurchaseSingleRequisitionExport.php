<?php

namespace App\Exports;

use App\Requisition;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PurchaseSingleRequisitionExport implements FromView , ShouldAutoSize
{
    protected $requisitionId;
    public function __construct($requisitionId)
    {
        $this->requisitionId = $requisitionId;
    }

    public function view(): View
        {
            return view('inventory.requisition.invoice-requisition-excel', [
                'requisition' =>  Requisition::find($this->requisitionId)
            ]);
        }
}
