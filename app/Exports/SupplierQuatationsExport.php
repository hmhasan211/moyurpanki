<?php

namespace App\Exports;

use App\Requisition;
use App\QuotationProduct;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SupplierQuatationsExport implements FromView , ShouldAutoSize
{
    protected $requisitionId;
    public function __construct($requisitionId)
    {
        $this->requisitionId = $requisitionId;
    }

    public function view(): View
        {
            $quotations = QuotationProduct::query()->with('supplier')->where('requisition_id',$this->requisitionId)->get()->groupBy('supplier_id');
            if ($quotations->count()==0){
                session()->flash('failed','No quotation found!');
                return redirect()->route('inventory.requisition.quotation.list');
            }
            return view('inventory.purchase.supplier-quotation-excel', [
                'requisition' =>  Requisition::find($this->requisitionId), 'quotations' => $quotations
            ]);
        }
}
