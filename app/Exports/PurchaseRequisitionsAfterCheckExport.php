<?php

namespace App\Exports;

use App\Purchase;
use App\Requisition;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Exports\PurchaseRequisitionsExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PurchaseRequisitionsAfterCheckExport implements FromView , ShouldAutoSize
{
    public function view(): View
        {
            return view('inventory.requisition.after-check-inventory-requisition-excel', [
                'requisitions' =>  Requisition::whereNotNull('check_user_id')->get()
            ]);
        }
}