<?php

namespace App\Exports;

use App\Purchase;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PurchaseOrderExport implements FromView , ShouldAutoSize
{
    protected $purchaseOrderId;
    public function __construct($purchaseOrderId)
        {
            $this->purchaseOrderId = $purchaseOrderId;
        }

    public function view(): View
        {
            return view('inventory.purchase.excel-purchase-order', [
                'purchase' =>  Purchase::find($this->purchaseOrderId)
            ]);
        }
}
