<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ['name','phone','email','address'];
    public function quotation_products()
    {
        return $this->hasMany('App\QuotationProduct');
    }
}
