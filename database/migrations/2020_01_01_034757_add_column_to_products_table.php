<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('last_consume')->nullable()->comment('last_two_months_consume')->after('price');
            $table->double('last_purchase_price')->nullable()->comment('last_price')->after('price');
            $table->date('last_purchase_date')->nullable()->comment('last_date')->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['last_consume','last_purchase_price','last_purchase_date']);
        });
    }
}
