@extends('layouts.fixed')

@section('title','Pending Requisition')
@section('style')
    <style>
        .check_label{
            padding: 5px;
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Pending Requisitions</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Pending Requisition</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6 text-right">
                    {{--<a href="{{ route('purchase_requisitions_excel') }}" class="btn btn-info">Excel</a>--}}
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">

            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        {{-- requisition  lists start --}}
                        <div class="col-lg-12 table-responsive">

                            <table class="table-bordered table-striped table text-center" id="myTable">
                                <thead>
                                    <tr class="bg-info">
                                        <th>#SL</th>
                                        <th>Requisition No</th>
                                        <th>Total Qty</th>
                                        <th>Approved Qty</th>
                                        <th>R. Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                @php $i=1@endphp
                                    @if(count($data)>0)
                                        @foreach($data as $list)

                                            @php
                                                $requisition = \App\Requisition::query()->findOrFail($list['id']);
                                            @endphp

                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{ $requisition->requisition_id }}</td>
                                                <td>{{ $list['requisition_total_qty'] }}</td>
                                                <td>{{ $list['approved_requisition_total_qty'] }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')}}  -- {{ $requisition->created_at->diffForHumans() }}</td>
                                                <td>
                                                    {{--<a class=" {{ $requisition->status !=null ? "fa fa-eye btn btn-danger" : "fa fa-eye  btn btn-success" }}" href="{{ route('pending.requisition.product',$requisition->id) }}"></a>--}}
                                                    <a class="fa fa-eye  btn btn-success" href="{{ route('pending.requisition.product',$requisition->id) }}"></a>
                                                </td>
                                            </tr>
                                       @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <span class="float-right">

                            </span>
                        </div>
                        {{-- requisition  lists End --}}

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->



@stop

@section('style')
    <!-- DataTables -->
    {{--  <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">  --}}
@stop

@section('plugin')
    <!-- DataTables -->
    {{--  <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>  --}}

    <!-- page script -->
    <script type="text/javascript">

    </script>
@stop
