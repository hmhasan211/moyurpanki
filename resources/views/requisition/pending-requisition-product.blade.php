@extends('layouts.fixed')

@section('title','Pending Requisition Product')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Pending Requisition Product</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Pending Requisition</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6 text-right">
{{--                    <a onclick="window.open('{{ route('inventory.purchase-requisition.single-print',$requisition->id) }}')" class="btn bg-success"> <i class="fa fa-print"></i> Print</a>--}}
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-globe"></i> Requisition No : {{ $requisition->requisition_id }}
                                    <small class="float-right"> Date: <label class="label-success" style="padding: 5px 15px"> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')  }}</label> </small>
                                </h4>
                                <br>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped table-bordered text-center">
                                    <thead>
                                        <tr class="bg-info">
                                            <th> #SL</th>
                                            <th> Approval</th>
                                            <th>Group</th>
                                            <th>Item Name</th>
                                            <th> Unit </th>
                                            <th> Total Qty</th>
                                            <th> Approved Qty</th>
                                            <th> Approve Qty</th>
                                            <th> Cur Stock</th>
                                            <th> Last Purchase</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                    @php $i=0; @endphp
                                    @if($requisition->count()>0)
                                        {{ Form::open(['route'=>['approve.pending.requisition',$requisition->id],'method'=>'post']) }}

                                        @foreach($requisition->requisition_products as $product)
                                            @php
                                              $total_qty = $product->qty;


                                             //$approved_total_qty = $product->new_requisition !=null ? $product->new_requisition->new_requisition_products->where('product_id',$product->product_id)->sum('qty') : 0;
                                             $approved_total_qty = $product->calculate_approve_qty($requisition,$product);

                                            $last_purchase = $product->product->find($product->product_id);
                                            $last_purchase_date = $last_purchase->last_purchase_date !=null ?  "Date: ". $last_purchase->last_purchase_date : "";
                                            $last_purchase_price = $last_purchase->last_purchase_price !=null ? "Price: ". $last_purchase->last_purchase_price : "";
                                            @endphp
                                            @if($total_qty - $approved_total_qty >0)
                                            <tr>
                                                <input type="hidden" name="requisition_qty[]" value="{{ $product->qty }}" class="form-control form-control-sm" />

                                                <td>{{ ++$i }}</td>
                                                <td><input type="checkbox" name="requisition_product_id[]" value="{{ $product->id }}" class="form-control">
                                                </td>
                                                <td>{{ $product->product->group ? $product->product->group->name : " N/A " }}</td>
                                                <td>{{ $product->product ? $product->product->name  : " N/A " }}</td>
                                                <td>{{ $product->product->unit ? $product->product->unit->name : " N/A "  }}</td>
                                                <td>{{ $product->qty }}</td>
                                                <td>{{ $approved_total_qty}}</td>
                                                <td>
                                                    <input type="text" name="qty[]" value="" class="form-control form-control" />
                                                </td>

                                                <td> N/A</td>
                                                <td>{{ $last_purchase_price}} <br/> {{ $last_purchase_date }} </td>

                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-lg-4">
                                <label for="sent_for">Sent For: <span style="color: red"> *</span></label>
                                <select id="sent_for" name="sent_for" class="form-control">
                                    <option  value="1">Quotation</option>
                                    <option  value="2">Without Quotation</option>
                                    {{--<option @if($requisition->sent_for == 2) selected @endif value="2">Without Quotation</option>--}}
                                </select>
                            </div>

                            <div class="col-lg-12">
                                <br>
                                {{ Form::textarea('checking_note',null,['class'=>'form-control','rows'=>5,'cols'=>5,'placeholder'=>'Approval Note....']) }}
                            </div>
                            <div class="col-lg-12">
                                <br>
                                {{ Form::submit('Approved',['class'=>'btn btn-info']) }}
                            </div>


                        </div>

                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

    <!-- page script -->
    <script type="text/javascript">
        function toggleSupplier(){
            var sent_for = $('#sent_for').val();
            if(sent_for == 1){
                $('#suppliers').hide();
            }else{
                $('#suppliers').show();
            }
        }

    </script>
    <!-- Script -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
@stop
