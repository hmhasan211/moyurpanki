
<table>
    <thead>
        <tr class="bg-info">
        <th>#SL</th>
        <th>Requisition No</th>
        <th>R. Invoice No</th>
        <th>Price</th>
        <th>Qty</th>
        <th>Note</th>
        <th>R. Date</th>
        <th>Status</th>
        </tr>
    </thead>

    <tbody>
        @php $i=0; @endphp
        @if($requisitions->count()>0)
            @foreach($requisitions as $requisition)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td width="11%">{{ $requisition->id }}</td>
                    <td>{{ $requisition->invoice_no }}</td>
                    <td>{{ $requisition->price }}</td>
                    <td>{{ $requisition->qty }}</td>
                    <td>{{ $requisition->note }}</td>
                    <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')   }}  -- {{ $requisition->created_at->diffForHumans() }}</td>
                    <td>

                        {!! \App\RequisitionProduct::where('requisition_id',$requisition->id)->whereNotNull('user_id')->get()->count()>0 ? "<label class='label label-success check_label'> Checked </label>" : "<label class='label label-danger check_label'>Not Checked</label>" !!}
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>