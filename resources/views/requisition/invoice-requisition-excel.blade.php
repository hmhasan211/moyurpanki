<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td></td>
            <td>Requisition No : {{ $requisition->id }}</td>
            <td></td>
            <td> Requisition Invoice : {{ $requisition->invoice_no }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td>Date:</td>
            <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')  }}</td>
        </tr>
        <tr>
            <th>#SL</th>
            <th>Approval</th>
            <th>Section / Department</th>
            <th>Group</th>
            <th>Item Name</th>
            <th>Item Code</th>
            <th>Unit </th>
            <th>Present Req.Price</th>
            <th>Present Req.Qty</th>
            <th>Remarks</th>
        </tr>
    </thead>
    <tbody>
        @php $i=0; @endphp
        @if($requisition->count()>0)
        {{ Form::open(['route'=>['inventory.requisition-item.checking-approval',$requisition->id],'method'=>'post']) }}

        @foreach($requisition->RequisitonProducts as $product)
            <tr>
                <td>{{ ++$i }}</td>

                <td> {{ $product->user_id !=null ? "Approved" : "Not Approved" }} </td>
                <td>{{ $product->product->department ? $product->product->department->name : "N/A" }}</td>
                <td>{{ $product->product->group ? $product->product->group->name : " N/A " }}</td>
                <td>{{ $product->product ? $product->product->name  : " N/A " }}</td>
                <td>{{ $product->product ? $product->product->item_code : " N/A "  }}</td>
                <td>{{ $product->product->unit ? $product->product->unit->name : " N/A "  }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->qty }}</td>
                <td>{{ $product->note }}</td>
            </tr>
        @endforeach
        @endif
    </tbody>
</table>
