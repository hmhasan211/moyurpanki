<div class="row">
    <div class="col-12 table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>Requisition No : </th>
                    <th>{{ $requisition->id }}</th>
                    <th>Requisition Invoice :</th>
                    <th> {{ $requisition->invoice_no }}</th>
                    <th> Date: </th>
                    <th> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')  }}</th>
                </tr>
                <tr>
                    <th> #SL</th>
                    <th>Section / Department</th>
                    <th>Group</th>
                    <th>Item Name</th>
                    <th>Item Brand</th>
                    <th>Item Code</th>
                    <th> Unit </th>
                    <th> Cur Stock</th>
                    <th> 2 Monts Consum.</th>
                    <th> present Req.Price</th>
                    <th> present Req.Qty</th>
                    <th> Last Purchase</th>
                    <th>Remarks</th>
                </tr>
            </thead>
            <tbody>
                @php $i=0; @endphp
                @if($requisition->count()>0)
                @foreach($requisition->RequisitonProducts as $product)
                @if($product->user_id !=null)
                @php
                $last_purchase = $product->product->find($product->product_id);
                $last_purchase_date = $last_purchase->last_purchase_date !=null ? "Date: ".
                $last_purchase->last_purchase_date : "";
                $last_purchase_price = $last_purchase->last_purchase_price !=null ? "Price: ".
                $last_purchase->last_purchase_price : "";
                @endphp
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $product->product->department ? $product->product->department->name : "N/A" }}</td>
                    <td>{{ $product->product->group ? $product->product->group->name  : "N/A"}}</td>
                    <td>{{ $product->product ? $product->product->name  : "N/A"}}</td>
                    <td>{{ $product->product->brand ? $product->product->brand->name  : "N/A"}}</td>
                    <td>{{ $product->product ? $product->product->item_code : "N/A" }}</td>
                    <td>{{ $product->product->unit ? $product->product->unit->name : "N/A" }}</td>
                    <td> N/A</td>
                    <td> N/A </td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->qty }}</td>
                    <td> {{ $last_purchase_price}} <br /> {{ $last_purchase_date }} </td>
                    <td>{{ $product->note }}</td>
                </tr>
                @endif
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
