@extends('layouts.fixed')

@section('title','Dashboard')

@section('content')
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage Department</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Setup</a></li>
                        <li class="breadcrumb-item active">Manage Department</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="row">
               
                <div class="col-lg-4">
                    <div class=" bg-light">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Add Department</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div> <!-- /.card-header -->
                            <form action="{{route('store.add')}}" method="POST">
                                @csrf
                                <div class="card-body" style="display: block;">
                                    <div class="mb-3">
                                        <label for="exampleFormControlInput1" class="form-label">Department Name</label>
                                        <input type="text" required name="name" class="form-control" id="exampleFormControlInput1" placeholder="name">
                                        </div>
                                        <div class="mb-3">
                                        <label for="description" class="form-label">Department Description</label>
                                            <textarea name="description" class="form-control" id="description" rows="3"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </div>
                                </div>
                             </form>
                    </div>
                </div>
   
                <div class="col-md-8">
                    <div class="card card-warning card-outline">
                        <div class="card-header">
                            <div class="card-title">
                                <h5 class="text-info">Department List</h5>
                            </div>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="display: block;">
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">S/L</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $sl = 1 @endphp
                                @foreach($units as $unit)
                                <tr>
                                <th scope="row">{{$sl}}</th>
                                <td>{{$unit->name}}</td>
                                <td>{{$unit->description}}</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#">Edit</a>
                                    <!-- <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-s">Delete</a> -->
                                </td>
                                </tr>
                                @php $sl++ @endphp
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
               
            </div>
        </div>
        </div>

    </section><!-- /.content -->
@stop

@section('plugin-css')

@stop

@section('plugin')
  
@stop