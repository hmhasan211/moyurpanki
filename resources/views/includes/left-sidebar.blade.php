<!-- Brand Logo -->
<a href="{{ url('/') }}" class="brand-link {{ setLogoClass() }}">
    <img src="{{ asset('favicon.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">Ringer ERP 1.0</span>
</a>

<!-- Sidebar -->
<div class="sidebar nano">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="{{ route('home') }}" class="d-block">Moyurponkhi</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item {{ isActive(['/','home']) }}">
                <a href="{{ action('DashboardController@index') }}" class="nav-link {{ isActive(['/','home']) }} main-menu">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                        {{--<span class="right badge badge-danger">New</span>--}}
                    </p>
                </a>
            </li>

            {{-- Inventory UL Li Start --}}

            {{-- Menu Setup Start --}}
            @php $active = ['store/department','Inventory/add-inventory-sub-group','Inventory/locker','Inventory/deck','Inventory/add-inventory-department','Inventory/add-inventory-group','Inventory/add-inventory-unit','Inventory/add-inventory-brand']; @endphp
            <li class="nav-item has-treeview {{ isActive(['User*']) }}">
                <a href="#" class="nav-link {{ isActive('User/*') }} main-menu">
                    <i class="nav-icon fa fa-user"></i>
                    <p> User Management <i class="fas fa-angle-left right"></i> </p>
                </a>

                <ul class="nav nav-treeview">
                    @if(canSee(Auth::user()->roles,'user.add'))
                    <li class="nav-item">
                        <a href="{{ route('user.add') }}" class="nav-link {{ isActive('User/add') }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">User List</p>
                        </a>
                    </li>
                    @endif
                @if(canSee(Auth::user()->roles,'user.roles.index'))
                    <li class="nav-item">
                        <a href="{{ route('user.roles.index') }}" class="nav-link {{ isActive('User/roles-add') }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Role Management</p>
                        </a>
                    </li>
                @endif
                </ul>
            </li>
            {{--<li class="nav-item {{ isActive(['store/department']) }}">--}}
                {{--<a href="{{ route('store.department') }}" class="nav-link {{ isActive(['store/department']) }} main-menu">--}}
                    {{--<i class="nav-icon fas fa-tachometer-alt"></i>--}}
                    {{--<p>--}}
                        {{--Store/Department--}}
                    {{--</p>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="nav-item has-treeview {{ isActive($active) }}">
                <a href="#" class="nav-link {{ isActive($active) }} main-menu">
                    <i class="nav-icon fas fa-tools"></i>
                    <p>Setup  <i class="fas fa-angle-left right"></i> </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ url('store/department') }}" class="nav-link {{ isActive(['store/department']) }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Department</p>
                        </a>
                    </li>
                    <!--<li class="nav-item">-->
                    <!--    <a href="{{ url('Inventory/deck') }}" class="nav-link {{ isActive(['Inventory/deck']) }} sub-menu">-->
                    <!--        <i class="fas fa-directions"></i><p style="margin-left:30px">Deck</p>-->
                    <!--    </a>-->
                    <!--</li>-->
                    <!--<li class="nav-item">-->
                    <!--    <a href="{{ url('Inventory/locker') }}" class="nav-link {{ isActive(['Inventory/locker']) }} sub-menu">-->
                    <!--        <i class="fas fa-directions"></i><p style="margin-left:30px">Locker</p>-->
                    <!--    </a>-->
                    <!--</li>-->
                    <li class="nav-item">
                       <a href="{{ url('Inventory/add-inventory-group') }}" class="nav-link {{ isActive(['Inventory/add-inventory-group']) }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Categories</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('Inventory/add-inventory-sub-group') }}" class="nav-link {{ isActive(['Inventory/add-inventory-sub-group']) }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Sub Categories</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('Inventory/add-inventory-unit') }}" class="nav-link {{ isActive(['Inventory/add-inventory-unit']) }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Unit</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('Inventory/add-inventory-brand') }}" class="nav-link {{ isActive(['Inventory/add-inventory-brand']) }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Brand</p>
                        </a>
                    </li>
                </ul>
            </li>

            {{-- Menu Setup End --}}

            {{-- Menu Setup Start --}}

            <li class="nav-item has-treeview {{ isActive(['Inventory/add-inventory-supplier']) }}">
                <a href="#" class="nav-link {{ isActive(['Inventory/add-inventory-supplier']) }} main-menu">
                    <i class="nav-icon fas fa-users"></i>
                    <p> Suppler / Party Setting<i class="fas fa-angle-left right"></i> </p>
                </a>
                <ul class="nav nav-treeview">

                    <li class="nav-item">
                        <a href="{{ url('Inventory/add-inventory-supplier') }}" class="nav-link {{ isActive(['Inventory/add-inventory-supplier']) }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Suppliers</p>
                        </a>
                    </li>
                </ul>
            </li>

            {{-- Menu Setup End --}}

            {{-- Item Create Start --}}
            <li class="nav-item has-treeview {{ isActive(['Inventory/add-inventory-item']) }}">
                <a href="#" class="nav-link {{ isActive(['Inventory/add-inventory-item']) }} main-menu">
                    <i class="nav-icon fas fa-box-open"></i>
                    <p> Item Management<i class="fas fa-angle-left right"></i> </p>
                </a>
                <ul class="nav nav-treeview">

                    <li class="nav-item">
                        <a href="{{ url('Inventory/add-inventory-item') }}" class="nav-link {{ isActive(['Inventory/add-inventory-item']) }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Item Create & View </p>
                        </a>
                    </li>
                </ul>
            </li>

            {{-- Item Create End --}}

            {{-- Requistiion Manage Start --}}

            {{--<li class="nav-item has-treeview {{ isActive(['Inventory/add-inventory-requisition','Inventory/view-purchase-requisition','Inventory/after-check-purchase-requisition','inventory/pending/requisition']) }}">--}}
                {{--<a href="#" class="nav-link {{ isActive(['Inventory/add-inventory-requisition','Inventory/view-purchase-requisition','Inventory/after-check-purchase-requisition','inventory/pending/requisition']) }} main-menu">--}}
                    {{--<i class="nav-icon fas fa-boxes"></i>--}}
                    {{--<p> Requisition Management<i class="fas fa-angle-left right"></i> </p>--}}
                {{--</a>--}}
                {{--<ul class="nav nav-treeview">--}}

                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ url('Inventory/add-inventory-requisition') }}" class="nav-link {{ isActive('Inventory/add-inventory-requisition') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Create Requisition</p>--}}
                        {{--</a>--}}

                        {{--<a href="{{ url('Inventory/view-purchase-requisition') }}" class="nav-link {{ isActive('Inventory/view-purchase-requisition') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">All Requisition</p>--}}
                        {{--</a>--}}

                        {{--<a href="{{ url('inventory/pending/requisition') }}" class="nav-link {{ isActive('inventory/pending/requisition') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Pending Requisition</p>--}}
                        {{--</a>--}}

                        {{--<a href="{{ url('Inventory/after-check-purchase-requisition') }}" class="nav-link {{ isActive('Inventory/after-check-purchase-requisition') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">After Checking Requisition</p>--}}
                        {{--</a>--}}

                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{-- Requistiion Manage End--}}

            {{-- Purchase Management Start --}}
            {{--<li class="nav-item has-treeview {{ isActive(['Inventory/add-inventory-requisition-purchase','Inventory/requisition-quotation','Inventory/purchase-summary','Inventory/purchase-order','Inventory/comparative-statements']) }}">--}}
                {{--<a href="#" class="nav-link {{ isActive(['Inventory/add-inventory-requisition-purchase','Inventory/requisition-quotation','Inventory/purchase-summary','Inventory/purchase-order','Inventory/comparative-statements']) }} main-menu">--}}
                    {{--<i class="nav-icon fas fa-clipboard-list"></i>--}}
                    {{--<p> Purchase Management<i class="fas fa-angle-left right"></i> </p>--}}
                {{--</a>--}}
                {{--<ul class="nav nav-treeview">--}}

                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ url('Inventory/add-inventory-requisition-purchase') }}" class="nav-link {{ isActive('Inventory/add-inventory-requisition-purchase') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Pending Purchase</p>--}}
                        {{--</a>--}}

                        {{--<a href="{{ url('Inventory/comparative-statements') }}" class="nav-link {{ isActive('Inventory/comparative-statements') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Comparative Statements</p>--}}
                        {{--</a>--}}


                        {{--<a href="{{ url('Inventory/requisition-quotation') }}" class="nav-link {{ isActive('Inventory/requisition-quotation') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">All Requisition Quotation</p>--}}
                        {{--</a>--}}


                        {{--<a href="{{ url('Inventory/purchase-order') }}" class="nav-link {{ isActive('Inventory/purchase-order') }}">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Make Purchase Order </p>--}}
                        {{--</a>--}}
                        {{--<a href="{{ url('Inventory/purchase-summary') }}" class="nav-link {{ isActive('Inventory/purchase-summary') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Purchase Order Summary </p>--}}
                        {{--</a>--}}

                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{-- Purchase Management End --}}
            {{-- Goods In  Start --}}
            <li class="nav-item has-treeview {{ isActive(['add-goodsin*','new-goodsin*','mr-report*','Inventory/InStock']) }}">
                <a href="#" class="nav-link {{ isActive(['add-goodsin*','new-goodsin*','mr-report*','Inventory/InStock']) }} main-menu">
                    <i class="nav-icon fas fa-dolly"></i>
                    <p>Inventory<i class="fas fa-angle-left right"></i> </p>
                </a>
                <ul class="nav nav-treeview">

                    <li class="nav-item">
                        <a href="{{ url('add-goodsin') }}" class="nav-link {{ isActive('add-goodsin') }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Add Goods In </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('inventory.inStock')}}" class="nav-link {{ isActive('Inventory/InStock') }} sub-menu">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">InStock</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('goods.out')}}" class="nav-link ">
                            <i class="fas fa-directions"></i><p style="margin-left:30px">Goods Out</p>
                        </a>
                    </li>
                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ url('new-goodsin') }}" class="nav-link {{ isActive('new-goodsin') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">New Goods In </p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ url('mr-report') }}" class="nav-link {{ isActive('mr-report') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">MR Report</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </li>

            {{-- Goods in End --}}

            {{-- Goods Issue Menu Start --}}
            @php $active = ['Inventory/add-inventory-department','Inventory/add-stock-issue','all-categories-stock-summary']; @endphp
            {{--<li class="nav-item has-treeview {{ isActive($active) }}">--}}
                {{--<a href="#" class="nav-link {{ isActive($active) }} main-menu">--}}
                    {{--<i class="nav-icon fas fa-dolly" style="transform: scaleX(-1);"></i>--}}
                    {{--<p> Goods Issue  <i class="fas fa-angle-left right"></i> </p>--}}
                {{--</a>--}}
                {{--<ul class="nav nav-treeview">--}}

                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ route('inventory.stock.issue') }}" class="nav-link {{ isActive('Inventory/add-stock-issue') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Stock Issue</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{-- <li class="nav-item">--}}
                        {{--<a href="#" class="nav-link {{ isActive('') }}">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Stock History</p>--}}
                        {{--</a>--}}
                    {{--</li> --}}
                    {{-- <li class="nav-item">--}}
                        {{--<a href="#" class="nav-link {{ isActive('') }}">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Stock Transaction</p>--}}
                        {{--</a>--}}
                    {{--</li> --}}

                {{--</ul>--}}
            {{--</li>--}}

            {{-- Goods Issue Menu End --}}
            {{-- Report Menu Start --}}
            @php $active = ['Inventory/add-inventory-department','goodsin-history','stock/summary','stock-register','stock-transaction']; @endphp
            {{--<li class="nav-item has-treeview {{ isActive($active) }}">--}}
                {{--<a href="#" class="nav-link {{ isActive($active) }} main-menu">--}}
                    {{--<i class="nav-icon fas fa-chart-pie"></i>--}}
                    {{--<p> Report  <i class="fas fa-angle-left right"></i> </p>--}}
                {{--</a>--}}
                {{--<ul class="nav nav-treeview">--}}
                    {{-- <li class="nav-item">--}}
                        {{--<a href="{{ route('goodsin-history') }}" class="nav-link {{ isActive('goodsin-history') }}">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Goods In History</p>--}}
                        {{--</a>--}}
                    {{--</li> --}}
                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ route('stock-register') }}" class="nav-link {{ isActive('stock-register') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Stock Register</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ route('stock.summary') }}" class="nav-link {{ isActive('stock/summary') }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Stock Summary</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{-- <li class="nav-item">--}}
                        {{--<a href="{{ route('stock-transaction') }}" class="nav-link {{ isActive('stock-transaction') }}">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Stock Transaction</p>--}}
                        {{--</a>--}}
                    {{--</li> --}}

                {{--</ul>--}}
            {{--</li>--}}

            {{-- Report Menu End --}}
            {{-- Settings Menu Start --}}
            @php $active = ['Inventory/add-inventory-department']; @endphp
            {{--<li class="nav-item has-treeview {{ isActive($active) }}">--}}
                {{--<a href="#" class="nav-link {{ isActive($active) }} main-menu">--}}
                    {{--<i class="nav-icon fas fa-tools"></i>--}}
                    {{--<p> Settings  <i class="fas fa-angle-left right"></i> </p>--}}
                {{--</a>--}}
                {{--<ul class="nav nav-treeview">--}}

                    {{--<li class="nav-item">--}}
                        {{--<a href="{{ url('Inventory/add-inventory-department') }}" class="nav-link {{ isActive(['Inventory/add-inventory-department']) }} sub-menu">--}}
                            {{--<i class="fas fa-directions"></i><p style="margin-left:30px">Departments Settings</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                {{--</ul>--}}
            {{--</li>--}}

                    {{-- Settings Menu End --}}
                {{--</ul>
            </li>--}}

            {{-- Inventory UL Li End --}}

        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>

