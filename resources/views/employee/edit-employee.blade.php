@extends('layouts.fixed')

@section('title','USER')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update USER  </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Settings</a></li>
                        <li class="breadcrumb-item active">Update USER  </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="row">
                {{--create/edit start --}}
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                    <div class=" bg-light">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Update USER  </h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div> <!-- /.card-header -->

                            <div class="card-body" style="display: block;">
                                {{ Form::model($user,['route'=>'user.update','method'=>'post','id'=>'CustomerForm','enctype'=>'multipart/form-data']) }}

                                <div class="col-lg-12 col-sm-12 {{$errors->has('name') ? 'has-error' : ''}}">
                                    {{ Form::hidden('id',$user->id,['class'=>'form-control','id'=>'id']) }}
                                    {{ Form::label('Customername','Name : ',['class'=>'control-label'])}}
                                    {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Ex: Customer Name','id'=>'name'])}}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                    <hr>
                                </div>
                                <div class="col-lg-12 col-sm-12 {{$errors->has('email') ? 'has-error' : ''}}">
                                    {{ Form::label('Customername','Email : ',['class'=>'control-label'])}}
                                    {{ Form::text('email',$user->email,['class'=>'form-control','placeholder'=>'Ex: USER@gmail.com','id'=>'email'])}}
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <hr>
                                </div>
                                <div class="col-lg-12 col-sm-12 {{$errors->has('password') ? 'has-error' : ''}}">
                                    {{ Form::label('Customername','Password : ',['class'=>'control-label'])}}
                                    {{ Form::text('npassword',null,['class'=>'form-control','placeholder'=>'Ex: 123456789101245','id'=>'name'])}}
                                    @if ($errors->has('npassword'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('npassword') }}</strong>
                                        </span>
                                    @endif
                                    <hr>
                                </div>
                                <div class="col-lg-12 col-sm-12 {{$errors->has('password') ? 'has-error' : ''}}">
                                    {{ Form::label('Customername','Role : ',['class'=>'control-label'])}}
                                    <select name="role[]" id="role" class="form-control" multiple>
                                        @forelse ($roles as $role)
                                                <option value="{{ $role->id }}"
                                                 @php 
                                                 $selected=false;
                                                 foreach($user->roles as $urole){
                                                    if($urole->id==$role->id) {
                                                        $selected=true;
                                                    }} 
                                                    @endphp @if($selected) selected="{{$selected}}" @endif>{{ $role->name }}</option>
                                        @empty
                                            
                                        @endforelse
                                    </select>
                                        @if ($errors->has('role'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('role') }}</strong>
                                            </span>
                                        @endif
                                    <hr>
                                </div>

                               
                              

                                <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">

                                    {{ Form::label('Customerimage','Image : ',['class'=>'control-label'])}}
                                    {{ Form::file('image',old('image'),['class'=>'form-control','required'])}}
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                    <hr>
                                </div>


                                <div class="col-lg-12 col-sm-12 {{$errors->has('image') ? 'has-error' : ''}}">

                                    {{ Form::label('Customerimage','Old Image : ',['class'=>'control-label'])}}

                                    <img src="{{ asset('public/admin/product/upload/'.$user->image) }}" alt="no image found" style="height: 80px;width: 80px;">

                                    <hr>
                                </div>

                                <div class="col-md-12 col-xs-12">
                                    <br>
                                </div>

                                <div class="col-md-12 col-xs-12">
                                    {{ Form::button('UPDATE',['type'=>'submit','id'=>'saveCustomer','class'=>'btn btn-primary']) }}
                                    {{ Form::hidden('canceledit',"Cancel Edit",['type'=>'reset','id'=>'cancel','class'=>'btn btn-danger']) }}
                                    <a href="{{ route('user.add') }}" class="fa fa-pencil btn btn-danger">Cancel</a>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                </div>
           

            </div>
        </div>

    </section><!-- /.content -->
@stop

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@stop

@section('script')
  <script>
    </script>
@stop