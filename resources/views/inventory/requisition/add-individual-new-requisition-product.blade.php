@extends('layouts.fixed')

@section('title','Checking Purchase Requisition | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Product for Requistion: #{{ $new_requisition->requisition->requisition_id }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Requisition</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">

            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 table-responsive">
                            
                            <form method="POST" action="{{ url('Inventory/store-individual-new-requisition-product') }}">
                                @csrf
                                <input type="hidden" name="new_requisition_id" value="{{ $new_requisition->id }}">
                                <input type="hidden" name="requisition_id" value="{{ $new_requisition->requisition_id }}">
                        {{-- Requisition items add form Start --}}
                        <div class="col-lg-12 table-responsive">
                            <h4 class="bg-info text-center" style="padding: 10px;">Item Add Form</h4>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4">
                                    <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                        {{Form::label('', 'Item Code', ['class'=>'label-control'])}}

                                        <select class='selectedProduct form-control' name="product_id" style='width: 200px;'  id="product_id">
                                            <option value='0'>- Select Code/Name -</option>
                                        </select>
                                        @if ($errors->has('code'))
                                            <span class="help-block text-center" id="success-alert">
                                            <strong>{{ $errors->first('code') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-2 col-sm-2">
                                    <div class="form-group{{ $errors->has('product_unit_id') ? ' has-error' : '' }}">
                                        {{ Form::label('', 'Unit', ['class'=>'label-control'])}}
                                        {{ Form::text('product_unit_id',null,['class'=>'form-control','autofocus','readonly','id'=>'unit']) }}

                                        @if ($errors->has('product_unit_id'))
                                            <span class="help-block text-center" id="success-alert">
                                            <strong>{{ $errors->first('product_unit_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-3">
                                    <div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
                                        {{Form::label('', 'Qty', ['class'=>'label-control'])}}
                                        {{ Form::text('qty',null,['class'=>'form-control','autofocus','required','placeholder'=>'Ex. 50','autocomplete'=>"off",'id'=>'qty','required']) }}

                                        @if ($errors->has('qty'))
                                            <span class="help-block text-center" id="success-alert">
                                        <strong>{{ $errors->first('qty') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-3">
                                    <div class="form-group{{ $errors->has('approved_qty') ? ' has-error' : '' }}">
                                        {{Form::label('', 'Approved Qty', ['class'=>'label-control'])}}
                                        {{ Form::text('approved_qty',null,['class'=>'form-control','autofocus','placeholder'=>'Ex. 50','autocomplete'=>"off",'id'=>'approved_qty']) }}

                                        @if ($errors->has('approved_qty'))
                                            <span class="help-block text-center" id="success-alert">
                                        <strong>{{ $errors->first('approved_qty') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-lg-12 col-sm-12">
                                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                        {{Form::label('', 'Note / Remarks / Description ', ['class'=>'label-control'])}}
                                        {{ Form::textarea('note',null,['class'=>'form-control','autofocus','rows'=>5,'cols'=>5,'placeholder'=>'Ex. Remarks / Note / Description','id'=>'note']) }}

                                        @if ($errors->has('price'))
                                            <span class="help-block text-center" id="success-alert">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class=" col-lg-3">
                                    <div class="form-group">
                                        {{ Form::button("Add Product",['type'=>'submit','class'=>'btn btn-primary btn-block']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

    <!-- page script -->
    <script type="text/javascript">
        $(document).ready(function () {
            pendingProducts();
        });

        /* pending product list start */
        function pendingProducts() {
            $.ajax({
                method:"get",
                url:"{{ route('inventory.requisition.pending.products') }}",
                dataType:"html",
                success:function (response) {
                    $("#requisition_items").html(response);
                }
            })
        }
        /* pending product list end */


        /* filter according to Product Code Start */

        $(document).on('change','#product_id',function () {
            var id = $(this).val();
            if(id !=null){
                $.ajax({
                    method:"post",
                    url:"{{ route('inventory.item.info') }}",
                    data: {id:id, _token:"{{ csrf_token() }}"},
                    dataType:'json',
                    success:function (response) {
                        $("#unit").val(response.unit);
                        $("#price").val(response.price);
                    }
                });
            }
        });

        /* filter according to Product Code End */

        /* store requisition at TempData Start*/
        $(document).on('click','.storeRequestRequisition',function () {
            var product_id = $("#product_id").val();
            var price = $("#price").val();
            var qty = $('#qty').val();
            var note = $("#note").val();

            if(product_id =="Select Code/Name" || qty ==''){
                $("#qty").css({"background":"#DC4C40","color":"#fff"});
                //$("#price").css({"background":"#DC4C40","color":"#fff"});
            }else{
                $("#qty").css({"background":"#fff","color":"#444"});
                //$("#price").css({"background":"#fff","color":"#444"});
                $.ajax({
                    method:"post",
                    url:"{{ route('inventory.purchase.temp.store') }}",
                    data : {product_id:product_id,price:price,qty:qty,note:note, _token:"{{ csrf_token() }}"},
                    dataType:"json",
                    success:function (res) {
                        pendingProducts();
                        if(res.success == 1){
                            $.notify("Product / Item Added to Requisition List", {globalPosition: 'bottom center',className: 'success'});
                        }
                        $("#unit").val(null);
                        $("#price").val(null);
                        $('#qty').val(null);
                        $('#note').val(null);
                    }
                });
            }
        });
        /* store requisition at TempData End */



        /* Requisition Confirmation Start */
        $(document).on("click",".confirmRequisition",function () {
           var note = $('.confirmationNote').val();
           var unit_id = {{ Auth::user()->active_unit }}
            var token  = "{{ csrf_token() }}";
            if(unit_id != 0){
                $("#unit_id").css({"background":"#fff","color":"#444"});
                $.ajax({
                    method:"post",
                    url : "{{ route('inventory.store.requisition') }}",
                    data:{ note:note ,unit_id:unit_id, _token:token},
                    dataType:"json",
                    success:function (res) {
                        if (res.success == 1){
                            pendingProducts();
                            $.notify("Purchase Requisition successfully created", {globalPosition: 'bottom center',className: 'success'});
                            $('.confirmationNote').val(null);
                        }else{
                            $.notify("Creating Purchase Requisition failed", {globalPosition: 'bottom center',className: 'error'});
                        }
                    }
                });
            }else{
                $("#unit_id").css({"background":"#DC4C40","color":"#fff"});
            }

        });
        /* Requisition Confirmation End */

        $(document).ready(function(){
            $(".selectedProduct").select2({
                ajax: {
                    url: "{{ url('Inventory/search-product-item') }}",
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            searchTerm: params.term,
                            type: 'query'
                          }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        var op = [];
                        $.each(data,function(index,value){
                            var text = value.item_code + " " + value.name ;
                            op.push(
                                {
                                    id:value.id,
                                    text:text
                                }
                            )
                        });
                        return {
                            results : op
                        }
                    },
                    cache: true
                }
            });
        });

    </script>
    <!-- Script -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
@stop

