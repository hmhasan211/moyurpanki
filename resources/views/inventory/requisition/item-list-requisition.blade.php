<div class="table-responsive">
    <h4>DEPARTMENT: STORE</h4>
    <table class="table table-hover table-striped table-bordered">
        <thead>
            <tr class="text-center bg-info">
                <td rowspan="2"> #SL</td>
                {{-- <td rowspan="2"> Department</td> --}}
                <td rowspan="2"> Group Name</td>
                <td rowspan="2"> Code</td>
                <td rowspan="2"> Item Name</td>
                <td rowspan="2"> Brand</td>
                <td rowspan="2"> Unit</td>
                {{-- <td rowspan="2"> Price</td> --}}
                <td rowspan="2"> Cur Stock</td>
                <td rowspan="2"> 2 Months Consum.</td>
                <td rowspan="2"> Present Req. Qty</td>
                <td colspan="4"> Last Purchase </td>
                <td rowspan="2"> Remarks</td>
                <td rowspan="2"> Action </td>
            </tr>
            <tr class="bg-info">
                <td>Order No</td>
                {{-- <td> Department</td> --}}
                <td>Date</td>
                <td> Qty</td>
                <td> Rate</td>
            </tr>
        </thead>

        <tbody class="bodyItem" id="requisition_items">
            <tr>
                <td colspan="14" class=" bg-danger text-center"> No Requisition Product found</td>
            </tr>
        </tbody>
    </table>

    <br>
    <br>
    <br>

</div>
