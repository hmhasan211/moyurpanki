@extends('report.index')

@section('title')
PR Report
@endsection

@section('report_title')
PR Report
@endsection

@section('by')
Who prepared this
@endsection

@section('content')
<style type="text/css">
  .invoice {
    border-right: 1px solid #2e2d2d;
    border-bottom: 1px solid #2e2d2d;
    padding: 3px;
  }
  .th{
      border-bottom: 1px solid #2e2d2d;border-right: 1px solid #2e2d2d;
  }
</style>
  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
    <div id="logo" style="float:right;">
      <img src="{{ asset('logo.png') }}" alt="Well Group" height="40" style="border-style: none;">
    </div>
    <div style="margin-left: 10%;float: center !important">
      <center>
        <span style="font-size: 30px"><strong>{{ unit_name() ?? app_info()->company_name }}</strong></span>
        <br>
        <span style="font-size: 24px"><strong>Purchase Requisition</strong></span>
        {{-- <h1 style="margin-left: 10%">{{ unit_name() ?? app_info()->company_name }}</h1>
        <h2>Purchase Requistion Form</h2> --}}
      </center>
    </div>
    <br>
    <div style="margin-bottom: 5px;font-size: 16px;float: left">
      <strong>
          Requisition No : {{ $requisition->requisition_id }}
      </strong>
    </div>
    <div style="margin-bottom: 5px;font-size: 16px;float: right">
      <strong>
          Requisition Date : {{ dateFormat($requisition->created_at) }}
      </strong>
    </div>
<table class="table" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">

    <thead>
        <tr style="font-size: 12px;height: 30px; background-color: #e3e1dc;" class="text-center">
            <td class="th" rowspan="2"> #SL</td>
            <td class="th" rowspan="2"> Group Name</td>
            <td class="th" width="6%" rowspan="2"> Code</td>
            <td class="th" width="20%" rowspan="2"> Item Name</td>
            <td class="th" rowspan="2" width="3%"> Unit</td>
            <td class="th" width="5%" rowspan="2"> Cur Stock</td>
            <td class="th" width="5%" rowspan="2"> 2 Monts Consum.</td>
            {{--<td class="th" rowspan="2"> 2 Months Consum.</td>--}}
            <td class="th"  width="7%"  rowspan="2"> Present Req. Qty</td>
            <td class="th" colspan="4"> Last Purchase </td>
            <td class="th"  width="17%" rowspan="2"> Remarks</td>
      </tr>
      <tr style="font-size: 12px;height: 30px; background-color: #e3e1dc;" class="text-center">
        <td class="th" width="5%" > Ord No</td>
        <td class="th" width="7%" > Date</td>
        <td class="th" width="5%" > Qty</td>
        <td class="th" width="5%" > Rate</td>
    </tr>
    </thead>
    <tbody style="font-size: 12px">
        @php $i=0; @endphp
        @if($requisition->count()>0)
            {{ Form::open(['route'=>['inventory.requisition-item.checking-approval',$requisition->id],'method'=>'post']) }}

            @foreach($requisition->requisition_products as $product)
            @if($product->disabled == 0)
            @php
                 // $last_purchase = App\PurchaseProduct::whereHas('purchase', function ($query) {
                 //                    return $query->where(get_where());
                 //                })->orderBy('id','desc')->where('product_id',$product->product_id)->with('purchase')->first();

                $last_purchase = App\GoodsinProduct::where('product_id',$product->product_id)->orderBy('id','desc')->first();

                // $last_purchase = $product->product->find($product->product_id);
                // $last_purchase_order_no = $last_purchase->last_purchase_order_no !=null ? $last_purchase->last_purchase_order_no : "";
                // $last_purchase_qty = $last_purchase->last_purchase_qty !=null ? $last_purchase->last_purchase_qty : "";
                // $last_purchase_date = $last_purchase->last_purchase_date !=null ? $last_purchase->last_purchase_date : "";
                // $last_purchase_price = $last_purchase->last_purchase_price !=null ? $last_purchase->last_purchase_price : "";
                $curr_stock = App\GoodsinProduct::where(get_where())->where('product_id',$product->product_id)->sum('qty');
                $date_today = date('Y-m-d H:i:s');
                $date_two_months = date("Y-m-d H:i:s", strtotime("-2 months"));
                $two_months_consume = App\GoodsinProduct::where(get_where())->where('product_id',$product->product_id)->where('created_at', '<=', $date_today)->where('created_at', '>=', $date_two_months)->sum('qty');
            @endphp
                <tr>
                    <td class="invoice" >{{ ++$i }}</td>
                    {{-- <td class="invoice" > {{ Form::checkbox('product_id[]',$product->id,($product->user_id !=null ? true : false),['class'=>'form-control']) }} </td> --}}
                    {{-- <td class="invoice" >{{ $product->product->department ? $product->product->department->name : "N/A" }}</td> --}}
                    <td class="invoice" >{{ $product->product->group ? $product->product->group->name : " N/A " }}</td>
                    <td class="invoice text-center" >{{ $product->product ? $product->product->item_code : " N/A "  }}</td>
                    <td class="invoice text-left" >{{ $product->product ? $product->product->name  : " N/A " }}</td>


                    <td class="invoice text-center" >{{ $product->product->unit ? $product->product->unit->name : " N/A "  }}</td>
                    <td class="invoice text-right" > {{ number_format($curr_stock) }}</td>
                    <td class="invoice text-right" > {{ number_format($two_months_consume) }}</td>
                    {{--<td class="invoice" > N/A </td>--}}
                    <td class="invoice text-right" >{{ $product->qty > 0? number_format(floatval($product->qty),2) : '' }}</td>
                    <td class="invoice text-right" >
                      @if(isset($last_purchase->id))
                      {{ optional(optional($last_purchase->goodsin)->purchase)->purchase_no}} 
                      @endif
                    </td>
                    <td class="invoice text-center" >
                      @if(isset($last_purchase->id))
                      {{ date('d-m-Y',strtotime(optional($last_purchase->goodsin)->bill_date)) }} 
                      @endif
                    </td>
                    <td class="invoice text-right" >
                      @if(isset($last_purchase->id))
                      {{ $last_purchase->qty > 0? number_format(floatval($last_purchase->qty),2) : ''}} 
                      @endif
                    </td>
                    <td class="invoice text-right" >
                      @if(isset($last_purchase->id))
                      {{ $last_purchase->price > 0? number_format(floatval($last_purchase->price),2) : '' }}
                      @endif
                    </td>
                    <td class="invoice" >{{ $product->note }}</td>

                </tr>
            @endif
            @endforeach
        @endif
    </tbody>
</table>
<div style="font-size: 12px">
  <strong>Note: </strong> <i>{!! $requisition->note !!}</i>
</div>
{{-- <br>
<div style="font-size: 12px">
  Terms & Conditions:
</div>
<table class="table"  align="center" cellpadding="0" cellspacing="0" style='width: 100%;border:none; margin-top:10px; float:left;position: relative; bottom: 0;'>
  <tr>
    <td style= "font-size: 12px; width: 10%">a)</td>
    <td style= "font-size: 12px"><strong> Order will be confirmed only after approval of sample.</strong>
    </td>
  </tr>
  <tr><td style= "font-size: 12px; width: 10%">b)</td><td style= "font-size: 12px"><strong> If quality is not as per sample, goods supplied will be returned to the seler at supplier cost.</strong></td></tr>
  <tr><td style= "font-size: 12px; width: 10%">c)</td><td style= "font-size: 12px"><strong> If after use of supplied goods quality of product us found defective, the supplier must compensate to the buyer based on the quantity of the defective goods and value of the same.</strong></td></tr>
  <tr><td style= "font-size: 12px; width: 10%">d)</td><td style= "font-size: 12px"><strong> Goods must be delivered within the delivery date fixed in purchase order.</strong></td></tr>
  <tr><td style= "font-size: 12px; width: 10%">e)</td><td style= "font-size: 12px"><strong> Quantity of goods ordered & goods delivered must be same.</strong></td></tr>
  <tr><td style= "font-size: 12px; width: 10%">f)</td><td style= "font-size: 12px"><strong> Supplier must provide VAT Challan along with Delivery Challan.</strong></td></tr>
  <tr><td style= "font-size: 12px; width: 10%">g)</td><td style= "font-size: 12px"><strong> Part shipment allowed/Not allowed.</strong></td></tr>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div class="signature">
    <table class=" table"  align="center" cellpadding="0" cellspacing="0" style="width: 100%;border:none; float:left;">
      <tr>
        <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 12px; margin-top:30px;" ><br>Prepare By</td>
        <td style="width: 15%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 12px; margin-top:30px;" ><br> Purchase Officer</td>
        <td style="width: 15%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 12px"><br>Head Of the Dept.</td>
        <td style="width: 15%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 12px"><br><span class="strong">ED/ Director</span></td>
      </tr>
    </table>
</div> --}}
<br><br><br>
<center><a id="print" class="btn-print print-none" onclick="window.print()" >Print</a></center>
</section>

@endsection
