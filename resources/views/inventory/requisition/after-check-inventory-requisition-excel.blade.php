    <div class="content">
        <div class="col-lg-12">
            <td></td>
            <div class="col-sm-6">
                <h1>After Checking Requisitions</h1>
            </div>
            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        {{-- requisition  lists start --}}
                        <div class="col-lg-12 table-responsive">

                            <table class="table-bordered table-striped table" id="myTable">
                                <thead>
                                    <tr class="bg-info">
                                        <th>#SL</th>
                                        <th>Requisition No</th>
                                        <th>R. Invoice No</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                        <th>Total Item</th>
                                        <th>Approved Item</th>
                                        <th>R. Date</th>
                                        <th> Checked By</th>
                                        <th>Note</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=0; @endphp
                                    @if($requisitions->count()>0)
                                    @foreach($requisitions as $requisition)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td width="11%">{{ $requisition->id }}</td>
                                        <td>{{ $requisition->invoice_no }}</td>
                                        <td>{{ $requisition->price }} TK</td>
                                        <td>{{ $requisition->qty }}</td>

                                        <td>
                                            @php $items =
                                            \App\RequisitionProduct::where('requisition_id',$requisition->id)->get()
                                            @endphp
                                            {{ $items->count() }}
                                        </td>

                                        <td>
                                            @php $items =
                                            \App\RequisitionProduct::where('requisition_id',$requisition->id)->whereNotNUll('user_id')->get()
                                            @endphp
                                            {{ $items->count() }}
                                        </td>

                                        <td>
                                            {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')   }}
                                            -- {{ $requisition->created_at->diffForHumans() }}
                                        </td>
                                        <td>

                                            {!! "<label class='label label-primary' style='padding:5px 15px'>
                                                ".$requisition->user->name." </label> " !!} </td>
                                        <td>{{ $requisition->note  }}</td>

                                        <td>
                                            <a class="fa fa-eye btn btn-success"
                                                href="{{ route('inventory.requisition.approved-items',$requisition->id) }}">
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
