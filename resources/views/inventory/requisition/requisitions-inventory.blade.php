@extends('layouts.fixed')

@section('title','Manage Requisition - Purcahse Requisition')
@section('style')
    <style>
        .check_label{
            padding: 5px;
        }
    </style>
@stop

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Requisitions</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a href="{{ route('purchase_requisitions_excel') }}" class="btn btn-success"><i class="fa fa-file-excel"></i> Excel</a>
                        {{-- <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Manage Requisition</li> --}}
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6 text-right">
                    {{-- <a href="{{ route('purchase_requisitions_excel') }}" class="btn btn-info">Excel</a> --}}
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">

            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        {{-- requisition  lists start --}}
                        <div class="col-lg-12 table-responsive">
                            <table class="table-bordered table-striped table" id="myRequisitionTable">
                                <thead>
                                    <tr class="bg-info">
                                        <th>#SL</th>
                                        <th>Requisition No</th>
                                        <th>R. Invoice No</th>
                                        <th>Price</th>
                                        <th>Factory Unit</th>
                                        <th>Qty</th>
                                        <th>Note</th>
                                        <th>R. Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($requisitions->count()>0)
                                        @php
                                            $i =1;
                                                @endphp
                                        @foreach($requisitions as $key => $requisition)
                                            <tr id="tr-{{ $requisition->id }}">
                                                <td>{{$i++}}</td>
                                                <td width="11%">{{ $requisition->requisition_id }}</td>
                                                <td>{{ $requisition->invoice_no }}</td>
                                                <td>{{ $requisition->price }}</td>
                                                <td>{{ $requisition->unit != null ?$requisition->unit->name : "N/A" }}</td>
                                                <td>{{ $requisition->qty }}</td>
                                                <td>{{ $requisition->note }}</td>
                                                <td> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')   }}  -- {{ $requisition->created_at->diffForHumans() }}</td>
                                                {{--<td>{{ $requisition->new_requisition !=null ? "<label class='label label-success check_label'> Checked </label>" : "<label class='label label-success check_label'>Not Checked </label>" }}</td>--}}

                                                <td>
                                                    {!! \App\NewRequisition::query()->where('requisition_id',$requisition->id)->first()!=null ? "<label class='label label-success check_label'> Checked </label>" : "<label class='label label-danger check_label'>Not Checked</label>" !!}
                                                </td>
                                                <td>
                                                    <a title="Details" class=" {{ $requisition->status !=null ? "fa fa-eye btn btn-danger" : "fa fa-eye  btn bg-success" }}" href="{{ route('inventory.purchase-requisition.single',$requisition->id) }}"></a>
                                                    @if(App\NewRequisition::query()->where('requisition_id',$requisition->id)->first() == null)
                                                    <button type="button" onclick="Delete('{{ $requisition->id }}')" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                    @endif
                                                     {{--<a class=" {{ $requisition->status !=null ? "fa fa-eye btn btn-danger" : "fa fa-eye  btn btn-success" }}" href="{{ $requisition->status !=null ? "#" : route('inventory.purchase-requisition.single',$requisition->id) }}"></a> --}}
                                                    <a href="{{route('purchase_requisitions_single_excel',$requisition->id)}}" class="btn btn-info">Excel</a></td>
                                            </tr>
                                       @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <span class="float-right">
                                {{--{{ $requisitions->links() }}--}}
                            </span>
                        </div>
                        {{-- requisition  lists End --}}

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->



@stop

@section('style')
    <!-- DataTables -->
     <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">

@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>

    <!-- page script -->
    <script type="text/javascript">
        $(document).ready(function () {
            table = $("#myRequisitionTable").DataTable();
        });


          function Delete(id) {
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to delete ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        $.ajax({
                          headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' },
                          url: "{{url('Inventory/delete-purchase-requisition')}}/"+id,
                          type: 'DELETE',
                          dataType: 'json',
                          data: {},
                          success:function(response) {
                            if(response.success){
                              $('#tr-'+id).fadeOut();
                            }else{
                              $.alert({
                                title:"Whoops!",
                                content:"<hr><strong class='text-danger'>Something Went Wrong!</strong><hr>",
                                type:"red"
                              });
                            }
                          }
                        });
                    },
                    cancel: function () {

                    }
                }
            });   
          }
    </script>
@stop
