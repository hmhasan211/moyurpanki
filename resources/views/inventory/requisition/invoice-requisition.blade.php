@extends('layouts.fixed')

@section('title','Checking Purchase Requisition | Invoice')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Purchase Requisition View</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Requisition</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{route('purchase_requisitions_single_excel',$requisition->id)}}" class="btn btn-info"><i class="fa fa-file-excel"></i> Excel</a>
                    <a onclick="window.open('{{ route('inventory.purchase-requisition.single-print',$requisition->id) }}')" class="btn bg-success"> <i class="fa fa-print"></i> Print</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-globe"></i> Requisition No : {{ $requisition->requisition_id }} , Requisition Invoice : {{ $requisition->invoice_no }} <a href="{{ route('individual.requisition.add', $requisition->id) }}" type="button" class="btn btn-sm btn-success"><i class="fa fa-plus" ></i>&nbsp;Add New Item</a>
                                    <small class="float-right"> Date: <label class="label-success" style="padding: 5px 15px"> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')  }}</label> </small>
                                </h4>
                                <br>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped table-bordered text-center">
                                    <thead>
                                        <tr class="bg-info">
                                            <th> #SL</th>
                                            <th> Approval</th>
                                            {{--<th>Section / Department</th>--}}
                                            <th>Group</th>
                                            <th>Item Name</th>
                                            <th>Item Code</th>
                                            <th> Unit </th>

                                            <th width="10%">Approve Qty</th>
                                            <th> Req. Qty</th>
                                            <th> Approved Qty</th>
                                            {{--<th width="10%"> Remaining req. qty</th>--}}
                                            <th> Cur Stock</th>
                                            <th> 2 Monts Consum.</th>
                                            <th> Last Purchase</th>
                                            <th>Remarks</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                    @php $i=0; @endphp
                                    @if($requisition->count()>0)
                                        {{ Form::open(['route'=>['inventory.requisition-item.checking-approval',$requisition->id],'method'=>'post']) }}

                                        @foreach($requisition->requisition_products as $product)
                                            @if($product->disabled == 0)
                                            @php
                                            $last_purchase = App\GoodsinProduct::where('product_id',$product->product_id)->orderBy('id','desc')->first();
                                            // $last_purchase = $product->product->find($product->product_id);
                                            // $last_purchase_date = $last_purchase->last_purchase_date !=null ?  "Date: ". $last_purchase->last_purchase_date : "";
                                            // $last_purchase_price = $last_purchase->last_purchase_price !=null ? "Price: ". $last_purchase->last_purchase_price : "";
                                            $approved_total_qty = $product->calculate_approve_qty($requisition,$product);
                                            $curr_stock = App\GoodsinProduct::where(get_where())->where('product_id',$product->product_id)->sum('qty');
                                            $date_today = date('Y-m-d H:i:s');
                                            $date_two_months = date("Y-m-d H:i:s", strtotime("-2 months"));
                                            $two_months_consume = App\GoodsinProduct::where(get_where())->where('product_id',$product->product_id)->where('created_at', '<=', $date_today)->where('created_at', '>=', $date_two_months)->sum('qty');

                                            @endphp
                                            <tr id="tr-{{ $product->id }}">
                                                <input type="hidden" name="requisition_qty[]" value="{{ $product->qty }}" class="form-control form-control-sm" />
                                                <td>{{ ++$i }}</td>
                                                <td><input type="checkbox" name="requisition_product_id[]" value="{{ $product->id }}" class="form-control" @if($product->status == 1) checked @endif>
                                                </td>
                                                <td>{{ $product->product->group ? $product->product->group->name : " N/A " }}</td>
                                                <td>{{ $product->product ? $product->product->name  : " N/A " }}</td>
                                                <td>{{ $product->product ? $product->product->item_code : " N/A "  }}</td>
                                                <td>{{ $product->product->unit ? $product->product->unit->name : " N/A "  }}</td>

                                                <td>
                                                    <input type="text" name="qty[]" value="" class="form-control text-right" />
                                                </td>
                                                <td>
                                                    <input type="text" name="req_qty[{{ $product->id }}]" value="{{ $product->qty }}" class="form-control text-right" />
                                                    
                                                </td>
                                                <td>{{ $approved_total_qty }}</td>
                                                {{--<td>--}}
                                                    {{--<input type="text" class="form-control" name="present_qty[{{ $product->id }}]" value="{{ $product->qty - $product->checked_qty }}">--}}
                                                {{--</td>--}}
                                                <td> {{ $curr_stock }} </td>
                                                <td> {{ $two_months_consume }} </td>
                                                <td>@if(isset($last_purchase->id))
                                                    Price: {{ $last_purchase->price }} <br/>Date: {{ date('d-m-Y',strtotime(optional($last_purchase->goodsin)->bill_date)) }} 
                                                    @endif
                                                </td>
                                                <td>{{ $product->note }}</td>
                                                <td>
                                                    {{-- @if($product->status == 0)
                                                    <a href="{{ route('individual.requisition.edit', $product->id) }}" title="Edit" class="btn bg-primary btn-sm"><i class="fa fa-edit"></i> </a>
                                                    @endif --}}
                                                    <button onclick="Delete('{{ $product->id }}')" title="Delete" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> </button>
                                                </td>

                                            </tr>
                                            @endif

                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-lg-4">
                                <label for="sent_for">Sent For: <span style="color: red"> *</span></label>
                                <select id="sent_for" name="sent_for" class="form-control">
                                    <option  value="1">Quotation</option>
                                    {{-- <option  value="2">Without Quotation</option> --}}
                                    {{--<option @if($requisition->sent_for == 2) selected @endif value="2">Without Quotation</option>--}}
                                </select>
                            </div>
                            {{-- <div id="suppliers" class="col-lg-4" style="display: none">
                                <label for="supplier_id">Suppliers: <span style="color: red"> *</span></label>
                                <select id="supplier_id" name="supplier_id" class="form-control">
                                    <option value="0">--- Select Supplier ---</option>
                                    @foreach($suppliers as $supplier)
                                    <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="col-lg-12">
                                <br>
                                {{ Form::textarea('checking_note',null,['class'=>'form-control','rows'=>5,'cols'=>5,'placeholder'=>'Approval Note....']) }}
                            </div>
                            <div class="col-lg-12">
                                <br>

                                @if($requisition->new_requisition->isEmpty())
                                {{ Form::submit('Approved',['class'=>'btn btn-info']) }}
                               @endif
                            </div>


                        </div>

                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>

    <!-- page script -->
    <script type="text/javascript">
        function toggleSupplier(){
            var sent_for = $('#sent_for').val();
            if(sent_for == 1){
                $('#suppliers').hide();
            }else{
                $('#suppliers').show();
            }
        }

        function Delete(id) {
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to delete ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        $.ajax({
                          url: "{{url('Inventory/delete-individual-requisition')}}/"+id,
                          type: 'GET',
                          dataType: 'json',
                          success:function(response) {
                            if(response.success){
                              $('#tr-'+id).fadeOut();
                            }else{
                              $.alert({
                                title:"Whoops!",
                                content:"<hr><strong class='text-danger'>Something Went Wrong!</strong><hr>",
                                type:"red"
                              });
                            }
                          }
                        });
                    },
                    cancel: function () {

                    }
                }
            });   
          }

    </script>
    <!-- Script -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
@stop
