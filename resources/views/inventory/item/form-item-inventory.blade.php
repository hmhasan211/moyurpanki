
{{--  <div class="form-group{{ $errors->has('item_code') ? 'has-error' : '' }}">
    {{Form::label('', 'Item Code ', ['class'=>'label-control'])}}

    {{ Form::text('item_code',null,['class'=>'form-control','placeholder'=>'W#00001']) }}

    @if ($errors->has('item_code'))
        <p class="text-danger" id="success-alert">
            {{ $errors->first('item_code') }}
        </p>
    @endif

</div>  --}}


<div class="row">
    <div class="col-lg-12">
        <div class="form-group{{ $errors->has('item_code') ? 'has-error' : '' }}">
            {{Form::label('', 'Item Code ', ['class'=>'label-control'])}}
            {{ Form::text('item_code',null,['class'=>'form-control','placeholder'=>'Item Code','required']) }}
            @if ($errors->has('item_code'))
                <p class="text-danger" id="success-alert">
                    {{ $errors->first('item_code') }}
                </p>
            @endif
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }}">
            {{Form::label('', 'Item Name ', ['class'=>'label-control'])}}
            {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Ex. 1 No Amper Tube','required']) }}
            @if ($errors->has('name'))
                <p class="text-danger" id="success-alert">
                    {{ $errors->first('name') }}
                </p>
            @endif
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group{{ $errors->has('product_unit_id') ? 'has-error' : '' }}">
            {{ Form::label('', 'Item Unit', ['class'=>'label-control'])}}
            {{ Form::select('product_unit_id',$units,null,['class'=>'form-control select-2','required']) }}

            @if ($errors->has('product_unit_id'))
                <p class="text-danger" id="success-alert">
                    {{ $errors->first('product_unit_id') }}
                </p>
            @endif
        </div>
    </div>

    <div class="col-lg-12">
        <div class="form-group{{ $errors->has('brand_id') ? 'has-error' : '' }}">
            {{ Form::label('', 'Item Brand', ['class'=>'label-control'])}}
            {{ Form::select('brand_id',$brands,null,['class'=>'form-control select-2']) }}
            @if ($errors->has('brand_id'))
                <p class="text-danger" id="success-alert">
                    {{ $errors->first('brand_id') }}
                </p>
            @endif
        </div>
    </div>
    {{--@dd($groups)--}}
    <div class="col-lg-12">
        <div class="form-group{{ $errors->has('group_id') ? 'has-error' : '' }}">

            <div class="form-group{{ $errors->has('group_id') ? 'has-error' : '' }}">
                <label for="category_id" class="label-control">Category</label>
                <select name="group_id" id="category_id" required onchange="setSubCategory()" class=" form-control select-2">
                    @foreach($groups as $key=>$category)
                        <option value="{{ $key}}">{{ $category }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group{{ $errors->has('sub_group_id') ? 'has-error' : '' }}">
                <label for="category_id" class="label-control">Sub Category</label>
                <select name="sub_group_id" id="sub_category_id" class=" form-control select-2">

                </select>
            </div>

            {{--{{ Form::label('', 'Category', ['class'=>'label-control'])}}--}}
            {{--{{ Form::select('group_id',$groups,null,['class'=>'form-control select2']) }} --}}{{--  group_id used as item category   --}}

            {{----}}
            {{--@if ($errors->has('group_id'))--}}
                {{--<p class="text-danger" id="success-alert">--}}
                    {{--{{ $errors->first('group_id') }}--}}
                {{--</p>--}}
            {{--@endif--}}
        </div>
    </div>
    {{--<div class="col-lg-12">--}}
        {{--<div class="form-group{{ $errors->has('group_id') ? 'has-error' : '' }}">--}}
            {{--<label for="" class="label-control">Sub Category</label>--}}
            {{--<select name="item" id="item_id" class="select2 form-control">--}}
                {{--<option value="">Select Item</option>--}}
                {{--@foreach($groups as $key=>$category)--}}
                    {{--<option value="{{ $key }}">{{ $category }}</option>--}}
                {{--@endforeach--}}
            {{--</select>--}}
            {{--{{ Form::label('', 'Sub Category', ['class'=>'label-control'])}}--}}
            {{--{{ Form::select('group_id',$groups,null,['class'=>'form-control select2']) }} --}}{{----}}{{--  group_id used as item category   --}}

            {{--@if ($errors->has('group_id'))--}}
                {{--<p class="text-danger" id="success-alert">--}}
                    {{--{{ $errors->first('group_id') }}--}}
                {{--</p>--}}
            {{--@endif--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="col-lg-12">
        <div class="form-group">
            <label for="" class="label-control">Size</label>
            <input name="size" type="text" placeholder="size" class="form-control">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <label for="" class="label-control">Color</label>
            <input name="color" type="text" placeholder="color" class="form-control">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <label for="" class="label-control">Image</label>
            <input name="image" type="file" class="form-control">
        </div>
    </div>
</div>




{{--<div class="form-group{{ $errors->has('price') ? 'has-error' : '' }}">--}}
    {{--{{ Form::label('', 'Item Price ', ['class'=>'label-control'])}}--}}
    {{--{{ Form::text('price',null,['class'=>'form-control','placeholder'=>'Ex. 50 TK']) }}--}}
    {{--@if ($errors->has('price'))--}}
        {{--<p class="text-danger" id="success-alert">--}}
            {{--{{ $errors->first('price') }}--}}
        {{--</p>--}}
    {{--@endif--}}
{{--</div>--}}



<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    {{Form::label('description', 'Description', ['class'=>'label-control'])}}
    {{ Form::textarea('description',old('description'),['class'=>'form-control','placeholder'=>'','rows'=>5,'cols'=>5]) }}

    @if ($errors->has('description'))
        <span class="text-danger">
            <p> {{ $errors->first('description') }} </p>
        </span>
    @endif
</div>

<div class="form-group row text-right">
    <div class="col-md-12">
        <a href="{{ URL::previous() }}">
            <button  class="btn btn-sm btn-danger">Back</button>
        </a>
        <button type="submit" class="btn btn-sm btn-success">{{$buttonText}}</button>
        <input type="reset" value="Reset" class="btn btn-sm btn-warning">
    </div>
</div>
<script>
    function setSubCategory(){
        var category_id = $('#category_id').val();
//        console.log(category_id)
        if(category_id != null){
            $.ajax({
                url: '{{ url('get/subcategory') }}/'+category_id,
                type: 'GET',
                dataType: 'json',
            })
                .done(function(response) {
                    console.log(response)
//                    $('#sub_category_id').val(response).select2();
                     data = '<option value="">Select Sub Category</option>';
                     selected = '';
                     $.each(response, function(index, val) {
//                         if(val.id == response.item_unit){
//                             selected = 'selected';
//                         }
                         console.log('ok');
                         data += '<option value='+val.id+' '+selected+'>'+val.name+'</option>';
                     });
                     $('#sub_category_id').html(data);
                });

        }else{
            //$('#unit_id').html('');
        }
    }
</script>
