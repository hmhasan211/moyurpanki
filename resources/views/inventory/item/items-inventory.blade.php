{{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}"> --}}
<div class="table-responsive">
    <table class="table table-hover" id="productTable">
        <thead>
        <tr>
            <td> #SL</td>
            <td> Code</td>
            <td> Name</td>
            <td> Category</td>
            <td> Unit</td>
            <td> Action</td>
        </tr>
        </thead>

        <tbody id="tbodyItem">
        @php $i=0; @endphp
        @foreach($items as $key=>$info)
            <tr>
                <td> {{ ++$i }}</td>
                <td> {{ $info->item_code  }}</td>
                <td> {{ $info->name  }}</td>
                <td>
                    @if($info->group)
                        {{ $info->group_id !=null ? $info->group->name : "N/A"  }}
                    @endif
                </td>
                <td>
                    @if($info->unit)
                        {{ $info->product_unit_id !=null ? $info->unit->name : "N/A"  }}
                    @endif
                </td>
                <td>
                    <a href="{{ route('inventory.item.edit',$info->id) }}" class="btn btn-edit btn-success far fa-edit"></a>
                    {{-- {{ Form::button('',['class'=>'btn btn-danger fas fa-trash-alt erase','data-id'=>$info->id,'data-url'=>route('inventory.item.destroy')]) }} --}}

                </td>
            </tr>
        @endforeach
        </tbody>

    </table>


</div>
@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@stop
<script type="text/javascript">
    $(document).ready(function () {
            table = $("#productTable").DataTable();
        });
</script>