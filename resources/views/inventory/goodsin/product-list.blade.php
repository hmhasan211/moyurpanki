@foreach($purchase->details as $key => $product)
	@php
    //dd($product);
		$total_goods_in = App\GoodsinProduct::query()->where('purchase_id', $purchase->id)->where('product_id',$product->product_id)->sum('qty');
		$received_qty = $total_goods_in;
		$balanced_qty = $product->qty - $total_goods_in;
        //$requisition_qty= $product->calculate_requisition_qty($product);

		$receivable_qty= $product->calculate_receivable_qty($product);
	@endphp
	<tr>
		<td>{{ $key + 1 }}</td>
		<td>{{ optional($product->product)->name }}</td>
{{--		<td>{{ optional(optional($product->product)->department)->name }}</td>--}}
		<td>{{ optional(optional($product->product)->group)->name }}</td>
		<td>{{ $product->qty }}</td>
		<td>{{ $receivable_qty}}</td>
        <td>
        	{{--<input type="number" class="form-control" value="" min="0" @if($receivable_qty > 0)  name="receive_qty[]" @endif  {{$receivable_qty > 0  ? 'required' : 'readonly'}}>--}}
        	<!--<input type="number" class="form-control" value="" min="0" step="0.00001"  name="receive_qty[]" {{$receivable_qty > 0  ? '' : 'readonly'}}>-->
        	<input type="number" class="form-control" value="" step="0.00001"   name="receive_qty[]">
        	<input type="hidden" name="product_id[]" value="{{ $product->product_id }}">

        </td>
		<td> <input class="form-control text-center" type="text"  name="price[]" value="{{ $product->price }}"></td>
		{{--<td>{{ $balanced_qty }}</td>--}}
		<td>{{ optional(optional($product->product)->unit)->name }}</td>
		<td><textarea name="remarks[]" id="" cols="10" rows="1" class="form-control" placeholder="Remarks"></textarea> </td>
	</tr>
@endforeach
