    <div class="col-lg-2">
        <div class="form-group">
            {{ Form::label('','Purchase Order  : ') }}
            <select onchange="getSupplier()" class="form-control purchase_id select-2" id="purchase_id" name="purchase_id" required>
                <option value="">Select PO</option>
                @foreach($purchases as $purchase)
                    <option value="{{$purchase->id}}">{{'R'.($purchase->new_requisition ? $purchase->new_requisition->requisition->requisition_id : '').'--'.'P'.$purchase->purchase_no}}</option>
                @endforeach
            </select>
            {{-- {{ Form::select('purchase_id',$purchases,false,['class'=>'form-control purchase_id ','placeholder'=>"Select Purchase Order ",'required'=>true]) }} --}}
        </div>
    </div>

    <div class="col-lg-2">
        <div class="form-group">
            {{ Form::label('','Supllier Name  : ') }}
            <input class="form-control" type="text" id="supplier_name" name="supplier_name" readonly>
            {{-- {{ Form::select('purchase_id',$purchases,false,['class'=>'form-control purchase_id ','placeholder'=>"Select Purchase Order ",'required'=>true]) }} --}}
        </div>
    </div>

    <div class="col-lg-2">
        <div class="form-group {{   $errors->has('bill_no') ? 'has-error' : '' }} ">
            {{ Form::label('','Bill No : ') }}
            {{ Form::text('bill_no',null,['class'=>'form-control','placeholder'=>"Ex. 11"]) }}

            @if($errors->has('bill_no'))
                <span class="">
                    <strong>{{ $errors->get('bill_no') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-lg-2">
        <div class="form-group {{   $errors->has('bill_date') ? 'has-error' : '' }} ">
            {{ Form::label('','Bill Date : ') }}
            {{ Form::text('bill_date',null,['class'=>'form-control date','placeholder'=>"Ex. 2020-01-01", 'required']) }}

            @if($errors->has('bill_date'))
                <span class="">
                    <strong>{{ $errors->get('bill_date') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-lg-2">
        <div class="form-group {{   $errors->has('chalan') ? 'has-error' : '' }} ">

            {{ Form::label('','Chalan No : ') }}
            {{ Form::text('chalan',null,['class'=>'form-control','placeholder'=>"Ex. 101"]) }}

            @if($errors->has('chalan'))
                <span class="">
                    <strong>{{ $errors->get('chalan') }}</strong>
                </span>
            @endif

        </div>
    </div>

    <div class="col-lg-2">
        <div class="form-group {{   $errors->has('chalan_date') ? 'has-error' : '' }} ">
            {{ Form::label('','Chalan Date : ') }}
            {{ Form::text('chalan_date',null,['class'=>'form-control date','placeholder'=>"Ex. 2020-01-01"]) }}

            @if($errors->has('chalan_date'))
                <span class="">
                    <strong>{{ $errors->get('chalan_date') }}</strong>
                </span>
            @endif
        </div>
    </div>


