@extends('layouts.fixed')
@section('title','Stock Summary report')
@section('css')
    <style>
        body {
            overflow-y: hidden;
            /*font-family: "Times New Roman";*/
            color: #000000;
        }
        .table-bordered td, .table-bordered th {
            border: 0.1rem solid rgba(8, 8, 8, 0.33) !important;
        !important;
        }

        h5.heading-title {
            text-transform: initial;
            font-weight: bold;
            letter-spacing: 1px;
        }

        tr th, tr td {
            line-height: 1.1;
            font-size: 20px;
            padding: 7px !important;

        }

        tr th, span strong, div span {
            text-transform: initial;
            font-weight: bold;
        }

        @media print {
            @page {
                size: A4 potrait;
                padding: 0 !important;
                margin: 0 !important;
            }
            .table-bordered td, .table-bordered th {
                border: 0.1rem solid rgba(8, 8, 8, 0.50) !important;
            }

            body{
                overflow: hidden !important;
                color: #000000;
            }

            div.card-body{
                margin: auto 20px;
            }

            div.card-body{
                padding-top: 0;
            }
            .table .thead-dark th {
                color: #000 !important;
                background-color: #dadada !important;
                /*border-color: #32383e !important;*/
            }
            /*body {*/
            /*-webkit-print-color-adjust: exact;*/
            /*}*/

        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Stock Summary report</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Report</a></li>
                        <li class="breadcrumb-item active">Stock Summary</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content no-print">
        <div class="container-fluid">
            {{--start --}}
            <div class="col-lg-12 col-sm-8 col-md-6 col-xs-12">
                <div class="card card-primary card-outline">
                    <div class="card-body " style="padding-bottom:0">
                        {{ Form::open(['route'=>'stock.summary','method'=>'get']) }}

                        <div class="form-row">
                            <div class="form-group col-md-5" >
                                <div>
                                    <select name="group_id" class="form-control">
                                        <option value="">Select Group</option>
                                        @foreach($groups as $key => $group)
                                            @php
                                                $count_items = 0;
                                                $products = count(App\Product::where('group_id',$key)->get(['id']));
                                                // foreach ($products as $productKey => $product) {
                                                //     $count_items += App\GoodsinProduct::where('product_id',$product->id)->sum('qty') - App\GoodsIssueProduct::where('product_id',$product->id)->sum('qty');
                                                // }

                                            @endphp
                                            @if($products > 0)
                                                <option value="{{ $key }}" @if($group_id == $key) selected @endif>{{ $group }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                {{-- {{ Form::select('group_id',$groups,null,['class'=>'form-control','placeholder'=>'Select Group']) }} --}}
                            </div>

                            <div class="form-group col-md-3" style="">
                                <button class="btn btn-info btn-md" type="submit"><i class="fa fa-search"></i> Search</button>
                                {{-- <input type="submit" class="btn btn-info btn-md" value="search"> --}}
                            </div>
                            <div class="form-group col-md-1" style="">
                                <button class="btn btn-success btn-md" onclick="window.print(); return false;"><i class="fa fa-print"></i> Print</button>
                            </div>

                        </div>
                        {{  Form::close() }}
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>{{--end --}}
    </section>

@if($stocks)
@if(count($stocks) >0)
    @php
        $total = count($stocks);
         $total_quantity = 0;
          $total_value = 0;
          $pageNo= 1;
          $i=1;
    @endphp
    @foreach($stocks->chunk(35) as $chunkStocks)

        @php
            $total -=count($chunkStocks);
        @endphp
        <section class="content">
            <div class="col-lg-12">
                <div class="card"><br>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12 text-right" style="margin:2px 30px 0 0">
                                <h6>Page No: {{$pageNo++}}</h6>
                            </div>
                            <div class="col-md-12 text-center">
                                <h3><b>  {{ is_numeric(Auth::user()->active_unit) ? \App\Unit::query()->findOrFail(Auth::user()->active_unit)->name : 'NULL' }}</b></h3>
                                <h5><b>Stock Summary Report</b></h5>
                            </div>
                            <div class="col-md-6 text-left">
                                <h4>{{ $group_name}}</h4>

                            </div>
                            <div class="col-md-6 text-right">
                                <span> Printed Time: {{ \Carbon\Carbon::now()->format('h:i:s A') }}</span>
                            </div>
                        </div>
                        {{-- view start --}}
                        <table class="table table-hover table-bordered table-condensed text-center" width="100% ">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">#SL</th>
                                <th width="35%">Name</th>
                                <th width="10%">Unit</th>
                                <th width="10%">Quantity</th>
                                <th width="10%">Rate</th>
                                <th width="10%">Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php

                            @endphp
                            @foreach($chunkStocks as $stock)

                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td class="text-left">{{ $stock['name']}}</td>
                                    <td class="text-center">{{ $stock['unit']}}</td>
                                    <td class="">{{ $stock['qty'] }}</td>
                                    <td class="text-right">{{ $stock['price'] }}</td>
                                    <td class="text-right">{{ $stock['qty'] * $stock['price']}}</td>
                                </tr>
                                @php
                                    $total_quantity += $stock['qty'];
                                    $total_value += $stock['qty'] * $stock['price'];
                                @endphp
                            @endforeach
                            @if($total  == 0)
                            <tr>
                                <td><b>{{count($stocks)}}</b></td>
                                <td colspan="2"><b>Total Value</b></td>
                                <td  class="text-right"><b>{{ number_format($total_quantity,2) }}</b></td>
                                <td></td>
                                <td  class="text-right"><b>{{ number_format($total_value,2) }}</b></td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="page_break"></div>
    @endforeach

@else
    <section class="content no-print">
        <div class="container-fluid">

            <div class="col-lg-12 col-sm-8 col-md-6 col-xs-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <h4 class="text-center">No Data Found</h4>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>

@endif
@endif


@stop

@section('style')

@stop


@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $('.select2').select2()


        function getCategoryWise(){
            var category_id = $('#category_id').val();
            window.open('{{ url('all-categories-stock-summary') }}/'+category_id, '_parent');
        }
    </script>
@stop
