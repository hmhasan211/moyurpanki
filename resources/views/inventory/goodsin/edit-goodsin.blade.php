<link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
<link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' type='text/css'>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="product-status-wrap">
          <form method="post" action="{{ route('goodsin.update', $goods_in->id) }}" enctype="multipart/form-data" >
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">

                <div class="form-group">
                  <label for="product_id">Item</label><span style="color:red"> *</span>
                  <select name="product_id" id="product_id" onchange="setUnitForItem()" class="select2 form-control">
                    <option value="" required>Select Item</option>
                    @foreach($products as $product)
                    <option value="{{ $product->id }}" @if($goods_in->product_id == $product->id) selected @endif>{{ $product->item_code }} {{ $product->name }}</option>
                    @endforeach
                </select>
                </div>

                <div class="form-group">
                  <label for="product_unit_id">Unit</label><span style="color:red"> *</span>
                  <select name="product_unit_id" id="product_unit_id" class="select2 form-control">
                    <option value="">Select Unit</option>
                    @foreach($units as $unit)
                    <option value="{{ $unit->id }}" @if($goods_in->product_unit_id == $unit->id) selected @endif>{{ $unit->name }}</option>
                    @endforeach
                </select>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="qty">Qty</label><span style="color:red"> *</span>
                            <input type="number" required name="qty" value="{{ $goods_in->qty }}" id="qty" class="form-control">
                        </div> 
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="price">Price</label><span style="color:red"> *</span>
                          <input type="number" step="0.0001" required name="price" value="{{ $goods_in->price }}" id="price" class="form-control">
                        </div>
                    </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="modal-footer justify-content-between" id="quickViewModalFooter">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-info">Save Changes</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
        <!-- page script -->
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
<script type="text/javascript">
    $('.select2').select2();

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
})
    function setUnitForItem(){
        var item_id = $('#product_id').val();
        console.log(item_id)
        if(item_id != null){
            $.ajax({
                url: '{{ url('add-goodsin') }}/'+item_id+'/set-unit',
                type: 'GET',
                dataType: 'json',
            })
            .done(function(response) {
                $('#product_unit_id').val(response.item_unit).select2();
            });
            
        }else{
            //$('#unit_id').html('');
        }
    }
</script>
