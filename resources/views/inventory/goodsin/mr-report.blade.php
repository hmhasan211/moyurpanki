@extends('layouts.fixed')

@section('title','Item - Inventory WELL GROUP ')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">MR Report</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Item setup</a></li>
                        <li class="breadcrumb-item active">MR Report</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="row">

                {{--list  start --}}
                <div class="col-lg-12">
                    <div class="card card-success card-outline">
                        {{-- <div class="card-header">
                            <div class="card-title">
                                <h5 class="text-info">Inventory Item list</h5>
                            </div>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div> --}}
                        <!-- /.card-header -->
                        <div class="card-body" style="display: block;">
                            {{-- {{ Form::label('','Show Product according to Category ') }}

                            {{ Form::select("searching",\App\Group::pluck('name','id'),null,['class'=>'form-control department_id','placeholder'=>'Select Category']) }}
                            <hr> --}}
                            <div id="showTable">
                                 {{-- <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}"> --}}
                                <div class="table-responsive">
                                    <table class="table table-hover" id="productTable">
                                        <thead>
                                        <tr>
                                            <td> #SL</td>
                                            <td> MRR No</td>
                                            <td> Client Name</td>
                                            <td> Delivery Challan</td>
                                            <td> PO No</td>
                                            <td> Action</td>
                                        </tr>
                                        </thead>

                                        <tbody id="tbodyItem">
                                        @foreach($goods_in as $key=>$info)

                                            @php
                                                $purchase_product = App\PurchaseProduct::where('purchase_id', $info->purchase_id)->first();
                                                if($purchase_product){
                                                    $supplier_id = $purchase_product->supplier_id;
                                                    $supplier_name = App\Supplier::find($supplier_id)->name;
                                                }else{
                                                    $supplier_name = 'No Name';
                                                }
                                            @endphp
                                            <tr id="tr-{{ $info->id }}">
                                                <td> {{ $key + 1 }}</td>
                                                <td> {{ $info->mrr_no }}</td>
                                                <td>{{ $supplier_name }}</td>
                                                <td> {{ $info->chalan  }}</td>
                                                <td>PO - {{ $info ? $info->purchase ? $info->purchase->purchase_no : null : null}}</td>
                                                <td>
                                                    <a onclick="window.open('{{ url('mr-report') }}/{{ $info->id }}/print')" class="btn bg-primary"><i class="fa fa-print"></i>&nbsp;Print</a>
                                                    <button onclick="Delete({{ $info->id }})" class="btn bg-danger"><i class="fa fa-trash"></i>&nbsp;Delete</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>


                                </div>


                            </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                {{--list end--}}

            </div>
        </div>

    </section><!-- /.content -->
@stop

@section('style')
                                    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>

@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
          function Delete(id) {
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to delete ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        $.ajax({
                          headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' },
                          url: "{{url('mr-report')}}/"+id,
                          type: 'DELETE',
                          dataType: 'json',
                          data: {},
                          success:function(response) {
                            if(response.success){
                              $('#tr-'+id).fadeOut();
                            }else{
                              $.alert({
                                title:"Whoops!",
                                content:"<hr><strong class='text-danger'>Something Went Wrong!</strong><hr>",
                                type:"red"
                              });
                            }
                          }
                        });
                    },
                    cancel: function () {

                    }
                }
            });   
          }
                
        $(document).ready(function () {
            table = $("#productTable").DataTable();
        });
        var table;
        $(document).ready(function () {
            table = $("#productTable").DataTable();
        });

        $(document).on("change",".department_id",function () {
            var group_id = $(this).val();
            table.clear().draw();
            var row = "<tr> <td colspan='9' style='background: red;padding: 10px;color: #fff3cd;font-size: 1rem;text-align: center;'> Please wait item loading ............. </td>  </tr>";
            $("#tbodyItem").html(row);


            $.ajax({
                method:"post",
                url : "{{ route('groupwise.productList') }}",
                data : { group_id:group_id,_token:'{{ csrf_token() }}'},
                success:function (response) {
                    $('#showTable').html(response);
                    //var obj =  JSON.parse(response).length;
                    // console.log(response.success);

                    // for (var i=0;i<response.success.sl.length;i++){
                    //     table.row.add([
                    //         response.success.sl[i],
                    //         response.success.code[i],
                    //         response.success.name[i],
                    //         response.success.department[i],
                    //         response.success.group[i],
                    //         response.success.unit[i],
                    //         response.success.price[i],
                    //         response.success.description[i],
                    //         response.success.edit[i]
                    //     ]).draw();
                    // }

                }
            })
        });

    </script>
@stop
