@extends('layouts.fixed')
@section('title','Goods in History')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Goods In History</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Goods In Management</a></li>
                        <li class="breadcrumb-item active">Goods In History</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="row">
                {{--create/edit start --}}
                <div class="col-lg-12">
                    <div class=" bg-light">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title"> Goods In History</h3>
                                 <div class="card-tools">
                                     <button type="button" class="btn btn-tool" data-widget="collapse">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                             </div> <!-- /.card-header -->

                            <div class="card-body" style="display: block;">
                                {{--  {{ Form::open(['route'=>'goodsin.store','method'=>'post', 'class'=>'form-horizontal']) }}  --}}
                                <div class="row">
                                    {{--  @include('inventory.goodsin.form-goodsin')  --}}
                                    <hr>

                                    <div class="col-lg-12">
                                        <p style="font-weight: bold;"><span class="float-left"> {{ "Item Name here" }}</span> <span class="float-right">Unit: {{"Item Unit Here"}}</span></p>
                                        <div style="clear:both"></div>
                                        <hr>
                                        <table id="historyTable" class="table-hover table-bordered  table goodsinProList">
                                            <thead>
                                                <tr>
                                                    <th>SL</th>
                                                    <th>V. No</th>
                                                    <th>Date</th>
                                                    <th>V. Type</th>
                                                    <th>Client</th>
                                                    <th>Ref. No.</th>
                                                    <th>QTY</th>
                                                    <th>Rate</th>
                                                    <th>Value</th>
                                                    <th>Remarks</th>
                                                </tr>
                                            </thead>

                                            <tbody class="proList">
                                                @forelse($goodsin_products as $key => $goodsin_product)

                                                    <tr>
                                                        {{-- <td> {{(($goodsin_products->currentPage() - 1) * $goodsin_products->perPage() + $key+1)}} </td> --}}
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ "Goods In" }}</td>
                                                        <td>{{ $goodsin_product->created_at }}</td>
                                                        <td>{{ "Purchase" }}</td>
                                                        <td>{{ optional($goodsin_product->supplier)->name }}</td>
                                                        <td>{{'Invoice_no: ' . optional(optional($goodsin_product->purchase)->requisition)->invoice_no }}</td>
                                                        <td>{{ $goodsin_product->qty }}</td>
                                                        <td>{{ number_format($goodsin_product->price,2) }}</td>
                                                        <td>{{ number_format($goodsin_product->qty * $goodsin_product->price,2) }}</td>
                                                        <td>{{ $goodsin_product->remarks }}</td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="11" class="text-center bg-danger">No Product Information Found</td>
                                                    </tr>
                                                @endforelse

                                            </tbody>

                                        </table>
                                        {{-- <span class="float-right">{{ $goodsin_products->links() }}</span> --}}

                                    </div>

                                    <div class="col-lg-9"></div>


                                    {{--  <div class=" col-lg-offset-9  col-lg-12">
                                        <div class="form-group">
                                            {{ Form::label('','Goods in Note : ') }}
                                            {{ Form::textarea('note',null,['class'=>'form-control','placeholder'=>'Ex. Goods in Note ','rows'=>5,'cols'=>5  ]) }}
                                        </div>
                                    </div>  --}}


                                    <div class="col-lg-10"></div>

                                    {{--  <div class="col-lg-2">
                                        <div class="form-group">
                                            {{ Form::submit('Save Goods In',['class'=>'form-control btn btn-info']) }}
                                        </div>
                                    </div>  --}}
                                </div>
                                {{--  {{ Form::close() }}  --}}
                            </div>
                        </div>
                    </div>
                </div>
                {{--create/edit  start --}}



            </div>
        </div>
    </section><!-- /.content -->
@stop

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $(document).ready(function () {
            table = $("#historyTable").DataTable();
        });

        {{--  var gtable ;

        $(document).ready(function () {
          //  gtable = $(".goodsinProList").DataTable();
        });

        $(document).on('change','.purchase_id',function () {
            var purchase_id = $(this).val();
            var token = '{{ csrf_token() }}';

            if (purchase_id !=''){
               $.ajax({
                   method:"post",
                   url:'{{ route('purchase.productList') }}',
                   data:{purchase_id:purchase_id,_token:token},
                   dataType:'html',
                   success:function (res) {

                        $('.proList').html(res);
                   }
               })
            }

        });


        $(document).on('keyup','.qtyCheck',function () {
            var qty = $(this).attr('data-qty');
            var rcvqty = $(this).val();
            var rowId = $(this).attr('data-rowid');

            if (Number(rcvqty) <= Number(qty)){
                $(this).css({'border':"5px solid #17A05D"});
                balance = Number(qty)-Number(rcvqty);
                $("#rowId"+rowId).html(balance);
            }else{
                $(this).val(0);
                $(this).css({'border':"5px solid #DD5044"});
            }

        });  --}}

    </script>
@stop