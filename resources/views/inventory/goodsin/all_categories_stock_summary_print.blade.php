@extends('report.index')

@section('title')
Stock Summary
@endsection

@section('report_title')
Stock Summary
@endsection

@section('by')
Who prepared this
@endsection

@section('content')

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm" style="">
   {{--  <div id="logo" style="float:right; display: inline;">
      <img src="{{ asset('logo.png') }}" alt="Well Group" height="40" style="border-style: none;">
    </div> --}}
    <center>
      <h1>{{ unit_name() ?? app_info()->company_name }}</h1>
      <div style="margin-top: -20px; font-size: 20px">
          Inventory Stock Summary
      </div>
      <div>
         {{ date('d-F-Y') }}
      </div>
    </center>
    <br><br>
    <div style="margin-bottom: 5px;font-size: 16px;float: left">
         Group : {{ $category_name }}
    </div>
    {{-- <div style="margin-bottom: 5px;font-size: 16px;float: right">
      <strong>
        Date : {{ date('d-M-Y') }}
      </strong>
    </div> --}}
    <table class="table" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">
        <thead>
            <tr style="font-size: 13px">
                <th class="td text-left" width="35%">Name</th>
                <th class="td text-left">Unit</th>
                <th class="td text-right">Quantity</th>
                <th class="td text-right">Rate</th>
                <th class="td text-right">Value</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($groups))
            @php 
                $total = 0;
            @endphp
            @foreach($groups as $key => $group)
                @php
                    $product = \App\Product::with('unit')->find($key);
                    $qty = \App\GoodsinProduct::where(get_where())->where('product_id',$key)->sum('qty');
                    $rate = \App\GoodsinProduct::where(get_where())->where('product_id',$key)->sum('price');
                @endphp
                <tr style="font-size: 13px" class="text-center">
                    <td class="td text-left">{{ $product->name }}</td>
                    <td class="td text-left">{{ optional($product->unit)->name }}</td>
                    <td class="td text-right">{{ $qty }}</td>
                    <td class="td text-right">{{ number_format($rate,2) }}</td>
                    <td class="td text-right">{{ number_format($qty * $rate,2) }}</td>
                </tr>
                @php 
                    $total += $qty * $rate;
                @endphp
            @endforeach
            <tr>
                <td class="td text-right" colspan="4">Total</td>
                <td class="td text-right">{{ number_format($total,2) }}</td>
            </tr>
            @endif
        </tbody>
    </table>
    {{-- <div style="margin-top: 250px" class="signature">
        <table class=" table"  align="center" cellpadding="0" cellspacing="0" style="width: 100%;border:none; float:left;">
           <tr>
            <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 13px; margin-top:30px;" ><br>Prepare By</td>
            <td style="width: 15%;border:none;"></td>
            <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 13px; margin-top:30px;" ><br> Purchase Officer</td>
            <td style="width: 15%;border:none;"></td>
            <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 13px"><br>Head Of the Dept.</td>
            <td style="width: 12%;border:none;"></td>
            <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 13px"><br><span class="strong">ED(Tech)/ Director/ MD</span></td>
          </tr>
        </table>
    </div> --}}
{{-- <br><br><br> --}}
<center style="margin-top:50px"><a  id="print" class="btn-print print-none" onclick="window.print()" >Print</a></center>
</section>
 
@endsection
