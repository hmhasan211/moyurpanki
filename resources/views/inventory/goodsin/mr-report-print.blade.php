@extends('report.index')

@section('title')
MRR
@endsection

@section('report_title')
MRR
@endsection

@section('by')
Who prepared this
@endsection

{{-- <style>
    @media print{
        @page{
            /*size: legal landscape;*/
            size: A4 portrait;
            padding: 0 !important;
            margin: 0 !important;
            /*margin-top: 8mm ;!*Disable print Header (url and page number)*!*/
            /*margin-bottom: 8mm ;!*Disable print Footer (url and page number)*!*/

        }
        body {
            padding: 0 !important;
            margin: 0;
            zoom:100%; /*or whatever percentage you need, play around with this number*/
            font-family: serif;
            font-size:15px;
            font-weight: bold;
            background-color: #FFFFFF !important;
        }



    }

</style> --}}
@section('content')
    @php
        $purchase_product = App\PurchaseProduct::where('purchase_id', $goods_in->purchase_id)->first();
        if($purchase_product){
            $supplier_id = $purchase_product->supplier_id;
            $supplier_name = App\Supplier::find($supplier_id)->name;
            $supplier_address = App\Supplier::find($supplier_id)->address;
        }else{
            $supplier_name = 'No Name';
            $supplier_address = 'No Address';
        }
    @endphp

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">
    <div id="logo" style="float:right;">
      <img src="{{ asset('logo.png') }}" alt="Well Group" height="40" style="border-style: none;">
    </div>
    <div style="margin-left: 11%;float: center !important">
      <center><span style="font-weight: bold;font-size: 20px">{{ unit_name() ?? app_info()->company_name }}</span></center>
      <center><span style="font-size: 12px">{{auth_unit_address()}}</span></center>
      <center><span style="font-weight: bold;font-size: 18px; margin-right:0 !important;">Material Receiving Report (MRR)</span></center>
    </div>
    <br>
    <div style="margin-bottom: 5px;font-size: 14px;float: left">
        <table style="font-size: 13px;">
            <tr>
                <td><strong> MR No & Date </strong></td>
                <td>: {{ $goods_in->mrr_no }} , {{ dateFormat($goods_in->bill_date) }}</td>
            </tr>
            <tr>
                <td><strong> Client Name </strong></td>
                <td>: {{ $supplier_name }}</td>
            </tr>
            <tr>
                <td><strong>Address </strong></td>
                <td>: {{ $supplier_address }}</td>
            </tr>
            <tr>
                <td><strong> Bill No </strong></td>
                <td>: {{ $goods_in->bill_no }}</td>
            </tr>
            @if(($goods_in->bill_no != $goods_in->chalan) && ($goods_in->chalan != null))
            <tr>
                <td><strong> Delivery Challan </strong></td>
                <td>: {{ $goods_in->chalan }}</td>
            </tr>
            @endif
            <tr>
                <td><strong> Order No </strong></td>
                <td>: {{ $goods_in->purchase->purchase_no }}</td>
            </tr>
            <tr>
                {{-- <td><strong>Note </strong></td>
                <td>: {{ $goods_in->note }}</td> --}}
            </tr>
        </table>
      {{-- <strong> MR No & Date </strong>: {{ $goods_in->id }} {{ dateFormat($goods_in->created_at) }}<br>
      <strong> Client Name </strong>: {{ $supplier_name }}<br>
      <strong> Address </strong>: {{ $supplier_address }}<br>
      <strong> Delivery Challan </strong>: {{ $goods_in->chalan }}<br>
      <strong> Order No </strong>: {{ $goods_in->purchase_id }} --}}
    </div>
    <br><br>
    {{-- <div style="margin-bottom: 5px;font-size: 16px;float: right">
      <strong>
         Requisition No : {{ $requisition->id }}<br>
         Requisition Date : {{ dateFormat($requisition->created_at) }}
      </strong>
    </div> --}}
      <div style="min-height: 320px">
          <table class="table" align="center" cellpadding="0" cellspacing="0" border="0" style="">
              <thead>
              <tr style="font-size: 13px;height: 30px; background-color: #e3e1dc;" class="text-center">
                  <td class="td"> #SL</td>
                  <td class="td"> Code</td>
                  <td class="td" width="30%"> Item Description</td>
                  <td class="td"> Unit</td>
                  <td class="td"> Qty Received</td>
                  <td class="td"> Unit Price</td>
                  <td class="td"> Total Value</td>
              </tr>
              </thead>
              <tbody style="font-size: 13px">
              @if(isset($goods_in->products[0]->id))
                  @php $total = 0;
                  @endphp
                  @foreach($goods_in->products as $key => $product)
                      {{--@dd($goods_in->products);--}}
                      <tr>
                          <td class="td" >{{ $key + 1 }}</td>
                          <td class="td" >{{ $product->product->item_code }}</td>
                          <td class="td" >{{ $product->product->name  }}</td>
                          <td class="td" >{{ optional($product->product->unit)->name  }}</td>
                          <td class="td text-right" >{{ $product->qty }}</td>
                          <td class="td text-right" >{{ number_format($product->price, 2) }}</td>
                          <td class="td text-right" > {{ number_format($product->qty * $product->price, 2) }}</td>
                      </tr>
                      @php
                          $total += $product->qty * $product->price;
                      @endphp
                  @endforeach
                  <tr>
                      <td class="td text-right" colspan="6"><strong>Total Value</strong> </td>
                      <td class="td text-right"><strong>{{ number_format($total, 2) }}</strong></td>
                  </tr>
              @endif
              </tbody>
          </table>
          <div style="font-size: 13px">
            <strong>Note: </strong> <i>{{ $goods_in->note }}</i>
          </div>
      </div>

<div style="margin-top:250px" class="signature">
    <table class=" table"  align="center" cellpadding="0" cellspacing="0" style="width: 100%;border:none; float:left;">
      <tr>
        <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 13px; margin-top:30px;" ><br><strong>PREPARED BY</strong></td>
        <td style="width: 8%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 13px; margin-top:30px;" ><br> <strong>STORE OFFICER</strong></td>
        <td style="width: 8%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 13px"><br>  <strong>ACCOUNTS OFFICER</strong></td>
        <td style="width: 7%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 13px"><br><span class="strong"><strong>MANAGER/DIRECTOR</strong></span></td>
      </tr>
    </table>
</div>
<br><br><br>
<center><a id="print" class="btn-print print-none" onclick="window.print()" >Print</a></center>
</section>

@endsection
