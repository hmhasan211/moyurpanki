@extends('layouts.fixed')
@section('title','Stock Register report')
@section('css')
    <style>
        body {
            overflow-y: hidden;
            /*font-family: "Times New Roman";*/
            color: #000000;
        }
        .table-bordered td, .table-bordered th {
            border: 0.1rem solid rgba(8, 8, 8, 0.33) !important;
        !important;
        }

        h5.heading-title {
            text-transform: initial;
            font-weight: bold;
            letter-spacing: 1px;
        }

        tr th, tr td {
            line-height: 1.0;
            font-size: 15px;
            padding: 7px !important;

        }

        tr th, span strong, div span {
            text-transform: initial;
            font-weight: bold;
        }

        @media print {
            @page {
                size: A4 landscape;
                padding: 0 !important;
                margin: 0 !important;
            }
            .table-bordered td, .table-bordered th {
                border: 0.1rem solid rgba(8, 8, 8, 0.50) !important;
            }

            body{
                overflow: hidden !important;
                color: #000000;
            }

            div.card-body{
                margin: auto 20px;
            }

            div.card-body{
                padding-top: 0;
            }
            .table .thead-dark th {
                color: #000 !important;
                background-color: #dadada !important;
                /*border-color: #32383e !important;*/
            }
            /*body {*/
            /*-webkit-print-color-adjust: exact;*/
            /*}*/

        }
    </style>
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Stock Register</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Report</a></li>
                        <li class="breadcrumb-item active">Stock Register</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content no-print">
        <div class="container-fluid">
            {{--start --}}
            <div class="col-lg-12 col-sm-8 col-md-6 col-xs-12">
                <div class="card card-primary card-outline">
                    <div class="card-body " style="padding-bottom:0">
                        {{ Form::open(['route'=>'stock-register','method'=>'get']) }}

                        <div class="form-row">
                            <div class="form-group col-md-2" >
                                <label>From</label>
                                <input type="date" name="from" class="form-control" value="{{ $from }}">
                            </div>
                            <div class="form-group col-md-2" >
                                <label>To</label>
                                <input type="date" name="to" class="form-control" value="{{ $to }}">
                            </div>
                            <div class="form-group col-md-3" >
                                <label>Item</label>
                                {{ Form::select('product_id',$items,$item,['id'=>'product_id','class'=>'form-control','placeholder'=>'All Items']) }}
                            </div>
                            <div class="form-group col-md-2" >
                                <label>Party</label>
                                {{ Form::select('supplier_id',$suppliers,$supplier,['id'=>'supplier_id','class'=>'form-control','placeholder'=>'All Parties']) }}
                            </div>
                            <div class="form-group col-md-1" >
                                <label>Payment</label>
                                <select name="payment_method" class="form-control">
                                    <option value="">All</option>
                                    <option value="1" @if($payment_method == '1') selected @endif>Credit</option>
                                    <option value="2" @if($payment_method == '2') selected @endif>Cash</option>
                                </select>
                            </div>
                            <div class="form-group col-md-1" style="margin-top: 30px">
                                <button type="submit" class="btn btn-info btn-md btn-block">{{-- <i class="fa fa-search"></i>&nbsp; --}}Search</button>
                            </div>
                            <div class="form-group col-md-1" style="margin-top: 30px">
                                <button class="btn btn-success btn-md btn-block" onclick="window.print(); return false;"><i class="fa fa-print"></i>&nbsp;Print</button>
                            </div>

                        </div>
                        {{  Form::close() }}
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>{{--end --}}
    </section>

@if($goodsin_products)
@if(count($goodsin_products) >0)
    @php
        $total = count($goodsin_products);
        $total_quantity = 0;
        $total_value = 0;
        $pageNo= 1;
        $i=1;
    @endphp
    @foreach($goodsin_products->chunk(10) as $chunkStocks)

        @php
            $total -=count($chunkStocks);
        @endphp
        <section class="content">
            <div class="col-lg-12">
                <div class="card"><br>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12 text-right" style="margin:2px 30px 0 0">
                                <h6>Page No: {{$pageNo++}}</h6>
                            </div>
                            <div class="col-md-12 text-center">
                                <h3><b>  {{ is_numeric(Auth::user()->active_unit) ? \App\Unit::query()->findOrFail(Auth::user()->active_unit)->name : 'NULL' }}</b></h3>
                                <h5><b>Inventory Register</b></h5>
                                <h5>Transaction Type : Purchase</h5>
                                <h6>Report From {{ $from }} To {{ $to }}</h6>
                            </div>
                            <div class="col-md-6 text-left">
                                <h4>Item: {{ $item_name}}</h4>

                            </div>
                            <div class="col-md-6 text-left">
                                <h4>{{ $unit_name}}</h4>
                            </div>
                           {{--  <div class="col-md-6 text-right">
                                <span> Printed Time: {{ \Carbon\Carbon::now()->format('h:i:s A') }}</span>
                            </div> --}}
                        </div>
                        {{-- view start --}}
                        <table class="table table-hover table-bordered table-condensed text-center" width="100% ">
                            <thead>
                            <tr>
                                <th width="3%" class="text-center">#SL</th>
                                <th width="10%">Date</th>
                                <th width="15%">Party</th>
                                <th width="10%">Ref No</th>
                                <th width="20%">Item Name</th>
                                <th width="5%">Unit</th>
                                <th width="5%">Qty</th>
                                <th width="5%">Rate</th>
                                <th width="5%">Value</th>
                                <th width="10%">Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php

                            @endphp
                            @foreach($chunkStocks as $stock)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td class="text-left">{{ date('Y-m-d', strtotime($stock->created_at))}}</td>
                                    <td class="text-left">{{ optional($stock->supplier)->name }}</td>
                                    <td class="text-left">P.O - {{ optional(optional($stock->goodsin)->purchase)->purchase_no }}/Bill No - {{ optional($stock->goodsin)->bill_no }}</td>
                                    <td class="text-left">{{ optional($stock->product)->name }}</td>
                                    <td class="text-left">{{ optional($stock->product_unit)->name }}</td>
                                    <td class="text-right">{{ number_format($stock->qty,2) }}</td>
                                    <td class="text-right">{{ number_format($stock->price,2) }}</td>
                                    <td class="text-right">{{ number_format($stock->qty * $stock->price,2) }}</td>
                                    <td class="text-left">{{ $stock->remarks }}</td>
                                    
                                </tr>
                                @php
                                    $total_quantity += $stock->qty;
                                    $total_value += $stock->qty * $stock->price;
                                @endphp
                            @endforeach
                            @if($total  == 0)
                            <tr>
                                <td colspan="6" class="text-right"><b>Total Value</b></td>
                                <td class="text-right"><b>{{ number_format($total_quantity,2) }}</b></td>
                                <td></td>
                                <td class="text-right"><b>{{ number_format($total_value,2) }}</b></td>
                                <td></td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="page_break"></div>
    @endforeach

@else
    <section class="content no-print">
        <div class="container-fluid">

            <div class="col-lg-12 col-sm-8 col-md-6 col-xs-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <h4 class="text-center">No Data Found</h4>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>

@endif
@endif


@stop

@section('style')

@stop


@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $('.select2').select2()
    </script>
@stop
