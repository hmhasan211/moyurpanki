<div class="row">
    <div class="col-lg-12">
        <div class="form-group {{   $errors->has('goodsin_id') ? 'has-error' : '' }} ">
            {{ Form::label('','L.C No : ') }}
            {{ Form::select('goodsin_id',$lcs,null,['class'=>'form-control','placeholder'=>"Select LC No"]) }}

            @if($errors->has('goodsin_id'))
                <span class="">
                    <strong>{{ $errors->get('goodsin_id') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="col-lg-12">
        <div class="form-group {{   $errors->has('cost_id') ? 'has-error' : '' }} ">
            {{ Form::label('','Cost Type : ') }}
            {{ Form::select('cost_id',$costs,null,['class'=>'form-control date','placeholder'=>"Select Cost Type"]) }}

            @if($errors->has('cost_id'))
                <span class="">
                    <strong>{{ $errors->get('cost_id') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-lg-12">
        <div class="form-group {{   $errors->has('amount') ? 'has-error' : '' }} ">
            {{ Form::label('','Cost Amount : ') }}
            {{ Form::number('amount',null,['class'=>'form-control','placeholder'=>"Ex. 500 tk"]) }}

            @if($errors->has('amount'))
                <span class="">
                    <strong>{{ $errors->get('amount') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="col-lg-12">
        <div class="form-group {{   $errors->has('note') ? 'has-error' : '' }} ">
            {{ Form::label('','Cost Note : ') }}
            {{ Form::textarea('note',null,['class'=>'form-control','placeholder'=>"Ex. Cost Note",'cols'=>5,'rows'=>5]) }}

            @if($errors->has('note'))
                <span class="">
                    <strong>{{ $errors->get('note') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="col-lg-12">
        <div class="form-group {{   $errors->has('amount') ? 'has-error' : '' }} ">
            {{ Form::submit($buttonText,['class'=>'form-control btn btn-success']) }}
        </div>
    </div>


</div>