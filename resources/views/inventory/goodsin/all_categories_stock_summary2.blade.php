@extends('layouts.fixed')
@section('title','Goods in ')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Stock Summary</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Report</a></li>
                        <li class="breadcrumb-item active">Stock Summary</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="row">
                {{--create/edit start --}}
                <div class="col-lg-12">
                    <div class=" bg-light">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <select onchange="getCategoryWise()" style="max-width: 250px !important" name="category_id" id="category_id" class="form-control">
                                        <option value="0" @if(isset($category_id)) @if($category_id == 0)) selected @endif @endif>All Categories</option>
                                        @foreach($categories as $cat)
                                        <option @if(isset($category_id)) @if($category_id == $cat->id)) selected @endif @endif value="{{ $cat->id }}">{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </h3>
                                <div class="card-tools">
                                    <button class="btn btn-success" onclick="window.open('{{ url('all-categories-stock-summary') }}/{{ $category_id }}/print', '_blank')"><i class="fa fa-print"></i> Print
                                    </button> 
                                </div>
                            </div> <!-- /.card-header -->

                            <div class="card-body" style="display: block;">
                                {{ Form::open(['route'=>'goodsin.store','method'=>'post', 'class'=>'form-horizontal']) }}
                                <div class="row">
                                    {{-- @include('inventory.goodsin.form-goodsin')
                                    <hr> --}}

                                    <div class="col-lg-12">
                                        <h6 class="text-center bg-info" style="padding:5px 0;font-size: 1.5rem;font-weight: bold;">Stock Summary</h6>
                                        <hr>
                                        <table class="table-hover table-bordered  table goodsinProList">
                                            <thead>
                                                <tr>
                                                    <th width="35%">Name</th>
                                                    <th >Unit</th>
                                                    <th>Quantity</th>
                                                    <th>Rate</th>
                                                    <th>Value</th>
                                                </tr>
                                            </thead>

                                            <tbody class="proList">
                                                @if(isset($groups))
                                                @php 
                                                    $total = 0;
                                                @endphp
                                                @foreach($groups as $key => $group)
                                                    @php
                                                        $product = \App\Product::with('unit')->find($key);
                                                        $qty = \App\GoodsinProduct::where(get_where())->where('product_id',$key)->sum('qty');
                                                        $rate = \App\GoodsinProduct::where(get_where())->where('product_id',$key)->sum('price');
                                                    @endphp
                                                    <tr>
                                                        <td>{{ $product->name }}</td>
                                                        <td>{{ optional($product->unit)->name }}</td>
                                                        <td class="text-right">{{ $qty }}</td>
                                                        <td class="text-right">{{ number_format($rate,2) }}</td>
                                                        <td class="text-right">{{ number_format($qty * $rate,2) }}</td>
                                                    </tr>
                                                    @php 
                                                        $total += $qty * $rate;
                                                    @endphp
                                                @endforeach
                                                <tr>
                                                    <td class="text-right" colspan="4">Total</td>
                                                    <td class="text-right">{{ number_format($total,2) }}</td>
                                                </tr>
                                                @endif
                                            </tbody>

                                        </table>
                                    </div>

                                    <div class="col-lg-9"></div>
                                    
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                {{--create/edit  start --}}



            </div>
        </div>
    </section><!-- /.content -->
@stop

@section('style')
    <!-- DataTables -->

    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css' rel='stylesheet' type='text/css'>
    
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
          theme: 'bootstrap4'
        })

        function getCategoryWise(){
            var category_id = $('#category_id').val();
            window.open('{{ url('all-categories-stock-summary') }}/'+category_id, '_parent');
        }
    </script>
@stop