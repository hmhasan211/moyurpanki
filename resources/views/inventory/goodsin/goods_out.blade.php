@extends('layouts.fixed')
@section('title','Goods in ')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage  Goods Out </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Goods Out Management</a></li>
                        <li class="breadcrumb-item active">Add Goods In </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="row">
                {{--create/edit start --}}
                <div class="col-lg-12">
                    <div class=" bg-light">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <div class="card-title">Item Section</div>
                                <div class="card-tools">
                                    {{--  <button type="button" class="btn btn-tool" data-widget="collapse">
                                        <i class="fas fa-plus"></i>
                                    </button>  --}}
                                </div>
                            </div> <!-- /.card-header -->
                            <div class="card-body" style="display: block;">
                                <div class="row">
                                    {{-- @include('inventory.goodsin.form-goodsin')
                                    <hr> --}}
                                    {{ Form::open(['route'=>'goodsin.store','method'=>'post', 'class'=>'form-horizontal','enctype' => 'multipart/form-data']) }}
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Department</label>
                                                    <select name="department_id" id="department_id" onchange="getCategory()" class="select2 form-control">
                                                        <option value="">Select Department</option>
                                                        @foreach($store as $sto)
                                                            <option value="{{ $sto->id }}">{{ $sto->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Category</label>
                                                    <select name="category_id" id="assign_category_id" onchange="get_Sub_Category()" class="select2 form-control">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Sub Category</label>
                                                    <select name="sub_category_id" id="assign_sub_category_id" class="select2 form-control">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Item</label>
                                                    <select required name="item" id="item_id" onchange="setUnit()" class="select2 form-control">
                                                        <option value="" required>Select Item</option>
                                                        @foreach($products as $product)
                                                            <option value="{{ $product->id }}">{{ $product->item_code }} {{ $product->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Item Code</label>
                                                    <input type="number" readonly name="item_code" id="qty" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="" class="label-control">QTY</label>
                                                    <input type="number" required name="qty" onkeyup="totalPrice()" id="requiredQty" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Unit</label>
                                                    <select name="product_unit_id" id="unit_id" class="select-2 form-control">
                                                        <option value="">Select Unit</option>
                                                        @foreach($units as $unit)
                                                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="price" class="label-control">Rate (Per Unit)</label>
                                                    <input required type="text" name="price" onkeyup="totalPrice()" class="form-control" id="unitPer" placeholder="price">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="price" class="label-control">Total Price</label>
                                                    <input readonly type="text" name="total_price" class="form-control" id="total_price_goods" placeholder="Total Price">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="price" class="label-control">Purchase Order Number</label>
                                                    <input required type="text" name="purchase_order" class="form-control" id="purchase_order_number" placeholder="Purchase Order">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Locker</label>
                                                    <select required name="locker_id" id="locker_id" onchange="setDeck()" class=" select-2 form-control">
                                                        <option  value="" >Select Locker</option>
                                                        @foreach($lockers as $locker)
                                                            <option value="{{ $locker->id }}">{{ $locker->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Deck</label>
                                                    <input type="text" readonly class="form-control" id="deck_id" placeholder="Deck">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="" class="label-control">Supplier</label>
                                                    <select name="supplier_id" id="" class="select2 form-control">
                                                        <option value="">Select Suppler</option>
                                                        @foreach($supplier as $sup)
                                                            <option value="{{ $sup->id }}">{{ $sup->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label for="attachment" class="label-control">Attachment</label>
                                                    <input type="file" name="image"  class="form-control" id="attachment" placeholder="attachment">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="attachment" class="label-control">Remarks</label>
                                                <textarea class="form-control" name="remarks" id="" cols="30" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>




                                    {{-- <div class=" col-lg-offset-9  col-lg-12">
                                        <div class="form-group">
                                            {{ Form::label('','Goods in Note : ') }}
                                            {{ Form::textarea('note',null,['class'=>'form-control','placeholder'=>'Ex. Goods in Note ','rows'=>5,'cols'=>5  ]) }}
                                        </div>
                                    </div> --}}




                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            {{ Form::submit('Save Goods In',['class'=>'form-control btn btn-info']) }}
                                        </div>
                                    </div>


                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                {{--create/edit  start --}}



            </div>

        </div>
        <hr>
        <div class="col-lg-12">
            <div class="row">

                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <h6 class="text-center bg-info" style="padding:5px 0;font-size: 1.5rem;font-weight: bold"><i class="fa fa-list"></i> ALL GOODS IN</h6>
                            <hr>
                            <div class="table-responsive">
                                <table class="table-hover table-bordered  table goodsinProList" id="goodsinProList">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th width="30%">Item</th>
                                        <th width="15%">Unit</th>
                                        <th class="text-right">Qty</th>
                                        <th class="text-right">Price</th>
                                        <th class="text-right">Total Price</th>
                                        <th class="text-right">Locker</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody class="proList">
                                    @if(isset($goods_in[0]->id))
                                        @foreach($goods_in as $key => $product)
                                            <tr id="tr-{{ $product->id }}">
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $product->product->item_code }} {{ $product->product->name }}</td>
                                                <td>{{ optional($product->product_unit)->name }}</td>
                                                <td class="text-right">{{ $product->qty }}</td>
                                                <td class="text-right">{{ number_format($product->price, 2) }}</td>
                                                <td class="text-right">{{ number_format($product->price*$product->qty, 2) }}</td>
                                                <td class="text-right">{{ $product->locker ? $product->locker->name : '' }}</td>
                                                <td class="text-center">
                                                    <button onclick="editPage('{{ $product->id }}')" class="btn bg-primary btn-sm"><i class="fa fa-edit"></i></button>
                                                    <button onclick="Delete('{{ $product->id }}')" class="btn bg-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                {{--create/edit  start --}}



            </div>

        </div>
        <div class="modal fade" id="quickViewModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="quickViewModalTitle">Edit Modal</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="quickViewModalBody">

                    </div>
                    <div class="modal-footer justify-content-between" id="quickViewModalFooter">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@stop

@section('style')
    <!-- DataTables -->

    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css' rel='stylesheet' type='text/css'>

@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
        $(document).ready(function () {
            table = $("#goodsinProList").DataTable();
        });
        function Delete(id) {
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to delete ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        $.ajax({
                            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' },
                            url: "{{url('add-goodsin')}}/"+id+'/delete',
                            type: 'DELETE',
                            dataType: 'json',
                            data: {},
                            success:function(response) {
                                if(response.success){
                                    $('#tr-'+id).fadeOut();
                                }else{
                                    $.alert({
                                        title:"Whoops!",
                                        content:"<hr><strong class='text-danger'>Something Went Wrong!</strong><hr>",
                                        type:"red"
                                    });
                                }
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }

        function editPage(id){
            var modal=$('#quickViewModal');
            var modalTitle=$('#quickViewModalTitle');
            var modalBody=$('#quickViewModalBody');
            var modalFooter=$('#quickViewModalFooter');
            modal.modal('toggle');
            modalFooter.hide();

            //modalBody.html('<center><img src="{{url('public/loader.gif')}}"></center>');
            $.ajax({
                url: "{{url('/add-goodsin')}}/"+id+"/edit",
                type: 'GET',
                data: {},
            })
                .done(function(response) {
                    modalTitle.html('Edit Goods In');
                    modalBody.html(response);
                    //modalFooter.show();
                })
                .fail(function() {
                    modal.modal('toggle');
                });
        }

        function setDeck() {
            var locker_id = $('#locker_id').val();
            if(locker_id != null){
                $.ajax({
                    url: '{{ url('add-goodsin') }}/'+locker_id+'/set-deck',
                    type: 'GET',
                    dataType: 'json',
                })
                    .done(function(response) {
                        console.log(response)
                        $('#deck_id').val(response.deck.name);
                        // data = '<option value="">Select Unit</option>';
                        // selected = '';
                        // $.each(response.units, function(index, val) {
                        //     if(val.id == response.item_unit){
                        //         selected = 'selected';
                        //     }
                        //     data += '<option value='+val.id+' '+selected+'>'+val.name+'</option>';
                        // });
                        // $('#unit_id').html(data);
                    });

            }else{
                //$('#unit_id').html('');
            }

        }

        function setUnit(){
            var item_id = $('#item_id').val();
            console.log(item_id)
            if(item_id != null){
                $.ajax({
                    url: '{{ url('add-goodsin') }}/'+item_id+'/set-unit',
                    type: 'GET',
                    dataType: 'json',
                })
                    .done(function(response) {
                        $('#unit_id').val(response.item_unit).select2();
                        // data = '<option value="">Select Unit</option>';
                        // selected = '';
                        // $.each(response.units, function(index, val) {
                        //     if(val.id == response.item_unit){
                        //         selected = 'selected';
                        //     }
                        //     data += '<option value='+val.id+' '+selected+'>'+val.name+'</option>';
                        // });
                        // $('#unit_id').html(data);
                    });

            }else{
                //$('#unit_id').html('');
            }
        }
        var gtable ;

        $(document).ready(function () {
            //  gtable = $(".goodsinProList").DataTable();
        });

        $(document).on('change','.purchase_id',function () {
            var purchase_id = $(this).val();
            var token = '{{ csrf_token() }}';

            if (purchase_id !=''){


                $.ajax({
                    method:"post",
                    url:'{{ route('purchase.productList') }}',
                    data:{purchase_id:purchase_id,_token:token},
                    dataType:'html',
                    success:function (res) {

                        $('.proList').html(res);
                    }
                })
            }

        });


        $(document).on('keyup','.qtyCheck',function () {
            var qty = $(this).attr('data-qty');
            var rcvqty = $(this).val();
            var rowId = $(this).attr('data-rowid');

            if (Number(rcvqty) <= Number(qty)){
                $(this).css({'border':"5px solid #17A05D"});
                balance = Number(qty)-Number(rcvqty);
                $("#rowId"+rowId).html(balance);
            }else{
                $(this).val(0);
                $(this).css({'border':"5px solid #DD5044"});
            }

        });
    </script>
    <script>
        function getCategory(){
            var department_id = $('#department_id').val();
            console.log(department_id)
            if(department_id != null){
                $.ajax({
                    url: '{{ url('get/department/category') }}/'+department_id,
                    type: 'GET',
                    dataType: 'json',
                })
                    .done(function(response) {
                        console.log(response)
//                    $('#sub_category_id').val(response).select2();
                        data = '<option value="">Select Category</option>';
                        selected = '';
                        $.each(response, function(index, val) {
//                         if(val.id == response.item_unit){
//                             selected = 'selected';
//                         }
                            console.log('ok');
                            data += '<option value='+val.id+' '+selected+'>'+val.name+'</option>';
                        });
                        $('#assign_category_id').html(data);
                    });

            }else{
                //$('#unit_id').html('');
            }
        }
    </script>
    <script>
        function get_Sub_Category(){
            var category_id = $('#assign_category_id').val();
            console.log(category_id)
            if(category_id != null){
                $.ajax({
                    url: '{{ url('get/subcategory') }}/'+category_id,
                    type: 'GET',
                    dataType: 'json',
                })
                    .done(function(response) {
                        console.log(response)
//                    $('#sub_category_id').val(response).select2();
                        data = '<option value="">Select Sub Category</option>';
                        selected = '';
                        $.each(response, function(index, val) {
//                         if(val.id == response.item_unit){
//                             selected = 'selected';
//                         }
                            console.log('ok');
                            data += '<option value='+val.id+' '+selected+'>'+val.name+'</option>';
                        });
                        $('#assign_sub_category_id').html(data);
                    });

            }else{
                //$('#unit_id').html('');
            }
        }
    </script>
    {{--total price section--}}
    <script>
        function totalPrice() {
            var qty = $('#requiredQty').val();
            var unit_price = $('#unitPer').val();
            var totalPrice = qty*unit_price;

            $('#total_price_goods').val(totalPrice);
        }
    </script>
@stop