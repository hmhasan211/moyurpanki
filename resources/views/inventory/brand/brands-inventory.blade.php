<div class="table-responsive">
    <table class="table table-hover" id="ProbrandTable">
        <thead>
            <tr>
                <td> #SL</td>
                <td> Name</td>
                <td> Description</td>
                <td> Action</td>
            </tr>
        </thead>

        <tbody>
            @php $i=0; @endphp
            @foreach($brands as $brand)
                <tr>
                    <td> {{ ++$i }}</td>
                    <td> {{ $brand->name  }}</td>
                    <td> {{ $brand->description  }}</td>
                    <td>
                        <a href="{{ route('inventory.brand.edit',$brand->id) }}" class="btn btn-edit btn-success btn-sm far fa-edit"></a>
                        {{-- {{ Form::button('',['class'=>'btn btn-danger fas fa-trash-alt erase','data-id'=>$brand->id,'data-url'=>route('inventory.brand.destroy')]) }} --}}

                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
</div>