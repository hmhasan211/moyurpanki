@extends('layouts.master')

@section('title','Requisition Inventory')
@section('style')
    <style>
        @media print {
            @page {
                size: A4
                 landscape
            }
            .purchasePrint {
                display: none;
            }
            body{
                padding: 10px;
            }
        }
    </style>
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('content')

    <!-- Content Header (Page header) -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-12">
                                <address style="text-align:center">
                                    <h1>{{ unit_name() ?? app_info()->company_name }}</h1>
                                    <h3>Industrial estate</h3>
                                    <h3>Chittagong</h3>
                                </address>
                                {{--  <h4>
                                    <i class="fas fa-globe"></i> Purchase Order
                                    <small class="float-right">Date: {{ \Carbon\Carbon::now()->format('Y/m/d')}}</small>
                                </h4>  --}}
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row">
                            <div width="50%" style="float:left">
                                Bill For
                                <address>
                                    <strong> {{ optional(optional($purchase)->supplier)->name }} </strong><br>
                                    Address: {{ optional(optional($purchase)->supplier)->address }}<br>
                                    Phone:  {{ optional(optional($purchase)->supplier)->phone }}<br>
                                    Email: {{ optional(optional($purchase)->supplier)->email ?? "N/A" }}
                                </address>
                            </div>
                            <!-- /.col -->
                            <div width="50%"  style="float:right">
                                From
                                <address>
                                    <strong>{{ unit_name() ?? app_info()->company_name }}</strong><br>
                                    Bscic Industrial estate,<br>
                                    Kalurghat,Chittagong<br>
                                    Phone: +8801810-101010<br>
                                    Email: support@wellbd.com
                                </address>
                            </div>
                            <br>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <div class="row invoice-col" style="clear:both">
                        {{-- <b>Invoice #007612</b><br>--}}
                         <br><br>
                         <span style="float:left"><b>Order No :</b> {{ $purchase->id }}</span>
                         <span style="float:right"><b>Order Date :</b> {{ \Carbon\Carbon::parse($purchase->created_at)->format('d-M-Y') }}</span><br>
                     </div><br>
                     <!-- /.col -->
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped table_border_print" border="1" cellspacing=0 cellpadding=5 width=100%>
                                    <thead>
                                        <tr>
                                            <th>Serial #</th>
                                            <th>Item Code</th>
                                            <th>Product / Item Name</th>
                                            <th>Unit </th>
                                            <th>Price </th>
                                            <th>Qty</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=0; $total=0; @endphp
                                        @foreach($purchase->details as $product)
                                            @php $total+=$product->price * $product->qty; @endphp
                                            <tr>
                                                <td style="text-align: center"> {{ ++$i }} </td>
                                                <td style="text-align: center"> {{ optional($product->product)->item_code }} </td>
                                                <td> {{ optional($product->product)->name }} </td>
                                                <td style="text-align: center"> {{ optional($product->product_unit)->name }} </td>
                                                <td style="text-align: right"> {{ number_format($product->price,2) }} Tk</td>
                                                <td style="text-align: right"> {{ number_format($product->qty,2) }} </td>
                                                <td style="text-align: right"> {{ number_format($product->price * $product->qty,2) }} Tk</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <!-- accepted payments column -->
                            {{--  <div class="col-lg-9">
                                <h3 class="text-left">Note :</h3>
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                   {{ $purchase->note }}
                                </p>
                            </div>  --}}
                            <!-- /.col -->
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table" width="100%">
                                        <tr>
                                            <th width="80%">
                                                {{--  <h3 class="text-left" ></h3>  --}}
                                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                                    <span style="float:left" class="text-left"> Note : </span>{{ $purchase->note }}
                                                </p>
                                            </th>
                                            <td  width="20%"><span style="float:right"><strong>Total:</strong> {{ number_format($total,2) }} TK</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
            <a href="#" target="_self" onclick="window.print()" class="btn btn-primary purchasePrint"> Print Purchase Order</a>
        </div><!-- /.container-fluid -->
    </section>
@stop
@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@stop

@section('script')
 
@stop
