@extends('layouts.master')

@section('title','Manage Requisition - Purcahse Requisition')

@section('content')
    <style type="text/css">
        
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    {{--  <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Comparative Statement according to Quotation</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Manage Quotation Taken Lists</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>  --}}


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-12">
                    <address style="text-align:center">
                        <h3>{{ unit_name() ?? app_info()->company_name }}</h3>
                        <h5>Industrial estate</h5>
                        <h5>Chittagong</h5>
                        <b>Comparative Statement according to Quotation</b>
                    </address>
                    {{--  <h4>
                        <i class="fas fa-globe"></i> Purchase Order
                        <small class="float-right">Date: {{ \Carbon\Carbon::now()->format('Y/m/d')}}</small>
                    </h4>  --}}
                </div>
                <!-- /.col -->
            </div>
            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        {{-- requisition  lists start --}}
                        <div class="col-lg-12 table-responsive">

                                {{-- Group By Quotation start --}}
                                <h4 class="text-left">Purchase Requisition : {{ $requisition->id }}, Dt - {{ date('d-m-Y', strtotime($requisition->created_at)) }} <p style="margin: 0" class="pull-right"> Date : {{ \Carbon\Carbon::parse($requisition->created_at)->format('d-m-Y')}} </p>  </h4>
                                <br>
                                <table class="table-bordered table table_border_print" id="myTable">
                                    <thead>
                                    <tr>
                                        <th class="td"colspan="2">Supplier Name></th>
                                        @php $counterStopAfterThirdSuppliername = 0; @endphp
                                        @foreach($suppliers as $sup=>$supplier)
                                            @php $counterStopAfterThirdSuppliername++ @endphp
                                            <td class="text-center"> {{ \App\Supplier::find($supplier->first()->supplier_id)['name'] ?? "Unknown" }}  </td>
                                            {{-- @if($counterStopAfterThirdSuppliername == 3)
                                                @break
                                            @endif --}}
                                        @endforeach
                                        <td rowspan="2" class="text-center">
                                            Last Purchase<br>Tk, Dt, Brand, Origin
                                        </td>
                                        <td width="10%" rowspan="2" class="text-center">Remarks</td>
                                    </tr>
                                    <tr>
                                        <th class="td text-center">Item</th>
                                        <th class="td text-center">Qty</th>
                                            
                                                @if($requisition->purchased_status == 1)
                                                @foreach($products as $productskey=>$cs)
                                                @foreach($suppliers as $sup=>$supplier)
                                                @php
                                                    $pricing = App\QuotationProduct::where('supplier_id', $sup)->where('requisition_product_id',$productskey)->first();
                                                    $purchase_product = App\PurchaseProduct::where('quotation_id',$pricing->id)->first();
                                                @endphp
                                                @if(isset($purchase_product->id))
                                                <th class="td">
                                                    PO No - {{ $purchase_product->purchase->id }}
                                                </th>
                                                @endif
                                                @endforeach
                                                @endforeach
                                                @else
                                                @foreach($suppliers as $sup=>$supplier)
                                                <th class="td">
                                                    PO No - 
                                                </th>
                                                @endforeach
                                                @endif
                                            </th>
                                    </tr>
                                    {{-- <tr>
                                        <th>#SL</th>
                                        <th>Item Code </th>
                                        <th>Item Name </th>
                                        @php $counterStopAfterThirdSuppliername = 0; @endphp
                                        @foreach($suppliers as $sup=>$supplier)
                                            @php $counterStopAfterThirdSuppliername++ @endphp
                                            <th> {{ \App\Supplier::find($supplier->first()->supplier_id)['name'] ?? "Unknown" }}  </th>
                                            @if($counterStopAfterThirdSuppliername == 3)
                                                @break
                                            @endif
                                        @endforeach
                                        <th>Quotation Generate By</th>
                                    </tr> --}}
                                    </thead>
                                    <tbody>
                                        @php $i =0; $count = 0; @endphp

                                            @if($requisition->purchased_status == 1)
                                            @foreach($products as $productskey=>$cs)
                                            <tr>
                                                @if($count == 0)
                                                    {{-- <td>{{ ++$i }}</td> --}}
                                                    {{-- <td>{{ $cs[$count]->requisitionProduct->product->item_code }}</td> --}}
                                                    <td class="td text-center">{{ $cs[$count]->requisitionProduct->product->name }}</td>
                                                    <td class="td text-center">{{ $cs[$count]->qty }} Pcs</td>
                                                        @php 
                                                        $counterStopAfterThirdSupplier = 0; 
                                                        
                                                        @endphp
                                                            @foreach($suppliers as $sup=>$supplier)

                                                                @php
                                                                    $counterStopAfterThirdSupplier++;
                                                                    // $pricing = \App\QuotationProduct::where('requisition_id',$requisition->id)->where('supplier_id',$supplier[0]->supplier_id)->where('requisition_product_id',$cs[$count]->requisition_product_id)->orderBy('price')->first();

                                                                    $pricing = App\QuotationProduct::where('supplier_id', $sup)->where('requisition_product_id',$productskey)->first();
                                                                     $purchase_product = App\PurchaseProduct::where('quotation_id',$pricing->id)->first();
                                                                    
                                                                @endphp

                                                                {{-- product pricing show according to supplier start --}}
                                                                    {{--  @foreach($pricing as $price)  --}}
                                                                    
                                                                    @if(isset($purchase_product->id))
                                                                    <td style="background-color: #cedbd2">
                                                                    Tk-{{ $pricing->price }}/- Per Pc Credit<br>
                                                                    Brand -  {{ optional(optional($cs[$count]->product)->brand)->name }} <br>
                                                                    Origin - 
                                                                    @php
                                                                     $last_purchase_price = $pricing->price;
                                                                     $last_purchase_brand = optional(optional($cs[$count]->product)->brand)->name;
                                                                    @endphp
                                                                    </td>
                                                                    @else
                                                                    <td>
                                                                        Tk-{{ $pricing->price }}/- Per Pc Credit<br>
                                                                        Brand -  {{ optional(optional($cs[$count]->product)->brand)->name }} <br>
                                                                        Origin - 
                                                                    </td>
                                                                    @endif
                                                                    
                                                                    {{--  @endforeach  --}}
                                                                {{-- product pricing show according to supplier End --}}
                                                                @if($counterStopAfterThirdSupplier == 3) 
                                                                    @break
                                                                @endif
                                                            @endforeach
                                                    <td class="text-center"> 
                                                        Tk-{{ $last_purchase_price }}/- Per Pc Credit<br>
                                                        Brand -  {{ $last_purchase_brand }} <br>
                                                        Origin - </td>
                                                    <td>  C/S No - <br>Dt -   </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        @else
                                        @foreach($products as $productskey=>$cs)
                                            <tr>
                                                @if($count == 0)
                                                    {{-- <td>{{ ++$i }}</td> --}}
                                                    {{-- <td>{{ $cs[$count]->requisitionProduct->product->item_code }}</td> --}}
                                                    <td class="text-center">{{ $cs[$count]->requisitionProduct->product->name }}</td>
                                                    <td class="text-center">{{ $cs[$count]->qty }} Pcs</td>
                                                        @php $counterStopAfterThirdSupplier = 0; @endphp
                                                            @foreach($suppliers as $sup=>$supplier)
                                                                @php
                                                                    $counterStopAfterThirdSupplier++;
                                                                    $pricing = App\QuotationProduct::where('supplier_id', $sup)->where('requisition_product_id',$productskey)->first()->price;
                                                                @endphp

                                                                {{-- product pricing show according to supplier start --}}
                                                                    {{--  @foreach($pricing as $price)  --}}
                                                                    <td> 
                                                                    Tk-{{ $pricing }}/- Per Pc Credit<br>
                                                                    Brand -  {{ optional(optional($cs[$count]->product)->brand)->name }} <br>
                                                                    Origin - 
                                                                    </td>
                                                                    {{--  @endforeach  --}}
                                                                {{-- product pricing show according to supplier End --}}
                                                                {{-- @if($counterStopAfterThirdSupplier == 3) 
                                                                    @break
                                                                @endif --}}
                                                            @endforeach
                                                    <td class="text-center"> New </td>
                                                    <td> C/S No - <br>Dt -  </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                <table class="table text-center" style="margin-top:100px;">
                                    <tr>
                                        <td></td>
                                        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">Prepared By</td>
                                        <td></td>
                                        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">Purchase Officer</td>
                                        <td></td>
                                        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">Purcahse Manager</td>
                                        <td></td>
                                        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">ED(Tech)/Director/MD</td>
                                        <td></td>
                                    </tr>
                                </table>
                                {{-- Group By Quotation End --}}

                        </div>
                    </div>
                    <button class="btn-primary btn cs" type="button" onclick="window.print()"> <i class="fa fa-print"></i> Print Comparative Statement</button>
                </div>

            </div>

        </div>
    </section>
@stop

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
    <style>
        @media print {
            .cs {
                display: none;
            }
        }
    </style>

@stop


@section('plugin_for_datatable_buttons')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

    {{--load script for data table by Ahmed --}}
    <script src="{{ asset('/js/datatable/jquery-3.3.1.js') }}"></script>
    <script src="{{ asset('/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.print.min.js') }}"></script>
@stop
