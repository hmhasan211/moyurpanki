@extends('layouts.fixed')

@section('title','Manage Requisition - Purcahse Requisition')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Quotation taken for  Requisition No : {{ $requisition->requisition->requisition_id }} </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Manage Quotation Taken Lists</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <div class="row">
                <div class="col-12">
                    {{--  <address style="text-align:center">
                        <h3>{{ unit_name() ?? app_info()->company_name }}</h3>
                        <h5>Industrial estate</h5>
                        <h5>Chittagong</h5>
                        <b>Comparative Statement according to Quotation</b>
                    </address>  --}}
                    {{--  <h4>
                        <i class="fas fa-globe"></i> Purchase Order
                        <small class="float-right">Date: {{ \Carbon\Carbon::now()->format('Y/m/d')}}</small>
                    </h4>  --}}
                </div>
                <!-- /.col -->
                <div class="col-12">
                    <a href="" class="btn btn-info float-right no-print" onclick="window.print()">Print</a>
                    <a href="{{route('purchase_supplier_quatations_excel',$requisition->id) }}"class="btn btn-info float-right no-print">Excel</a> </div>
            </div><br>
            {{--  <h1 class="text-center">Quotation taken for  Requisition No : {{ $requisition->id }} <a href="" class="btn btn-info float-right no-print" onclick="window.print()">Print</a></a></h1>  --}}
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <form action="{{ route('inventory.purchase.store') }}" method="post" class="form-inline float-right ">
                            @csrf
                            {{-- requisition  lists start --}}
                            <div class="col-md-12 table-responsive">
                                <div class="row">
                                    @php
                                        $count_quotation = count($quotations);
                                    @endphp

                                    @foreach($quotations as $supplier => $supplier_quotations)
                                        @php
                                            $paymnet_type_status = 0;
                                            if($supplier_quotations->first()->quotation->purchase){
                                                $paymnet_type_status = $supplier_quotations->first()->quotation->purchase->payment_type;
                                            }
                                        @endphp
                                        <div @if($count_quotation > 1) class="col-md-6" @else class="col-md-12" @endif>
                                            <h4 class="bg-info" style="padding: 5px 10px;">
                                                @if( $supplier_quotations->first()->supplier )
                                                    Supplier: {{ $supplier_quotations->first()->supplier->name }} -- {{ $supplier_quotations->first()->supplier->phone }}
                                                @endif
                                                <div class="form-inline float-right ">
                                                    <select onchange="changeSupplier('{{ $supplier_quotations->first()->requisition_id }}','{{ $supplier_quotations->first()->quotation_id }}')" class="form-control" name="supplier_id" id="supplier_id_{{ $supplier_quotations->first()->quotation_id }}">
                                                        <option value="">Change Supplier</option>
                                                        @foreach($suppliers as $individual_supplier)
                                                            <option value="{{ $individual_supplier->id }}">{{ $individual_supplier->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <select onchange="setPayment('{{ $supplier_quotations->first()->requisition_id }}','{{ $supplier_quotations->first()->supplier_id }}')" id="payment_type_{{ $supplier_quotations->first()->supplier_id }}" name="payment_type[{{ $supplier_quotations->first()->supplier_id }}]" class="form-control">


                                                        <option value="1" @if($paymnet_type_status == 1) selected @endif>Credit</option>
                                                        <option value="2" @if($paymnet_type_status == 2) selected @endif>Cash</option>
                                                    </select>
                                                </div>
                                            </h4>
                                            <table class="table table-hover table_border_print">
                                                <thead>
                                                <tr>
                                                    <th>SL</th>
                                                    <th>PO</th>
                                                    <th>Item Name</th>
                                                    <th>Item Code</th>
                                                    <th>Recruit. Qty</th>
                                                    <th>PO Qty</th>
                                                    <th>Price</th>
                                                    <th>Action <br>
                                                        <button onclick="deleteQuotation('{{  $supplier_quotations->first()->quotation_id }} ')" type="button" class="btn bg-danger btn-sm">Delete</button>
                                                        {{-- <button onclick="resetPO('{{  $supplier_quotations->first()->quotation_id }} ')" type="button" class="btn bg-danger btn-sm">Reset PO</button> --}}
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $i=0; @endphp
                                                @foreach($supplier_quotations as $product)
                                                    {{--@dd($product)--}}
                                                    @php
                                                        //dd($supplier->id);
                                                        $newRequisitionProduct = App\NewRequisitionProduct::query()->where('id',$product->requisition_product_id)->first();
                                                        $purchase_product = App\PurchaseProduct::query()->where('quotation_product_id',$product->id)->where('supplier_id', $supplier)->first();
                                                    @endphp
                                                    <tr id="tr-{{ $product->id }}">
                                                        <td>{{ ++$i }} </td>
                                                        <td><input style="width: 18px;height: 18px" type="checkbox" class="form-control-md" name="purchase[]" id="purchase_{{ $product->quotation_id }}_{{ $product->id }}" {{-- onclick="getPurchase('{{ $product->quotation_id }}','{{ $product->id }}')" --}} value="{{ $product->id }}" @if(isset($purchase_product->id)) checked @endif>@if(isset($purchase_product->id)) [{{ $purchase_product->purchase->purchase_no }}] @endif</td>
                                                        <td> @if($product->supplier)
                                                                {{ $product->requisitionProduct ? $product->requisitionProduct->product->name : null}}
                                                            @endif
                                                        </td>
                                                        <td> @if($product->supplier)
                                                                {{ $product->requisitionProduct ? $product->requisitionProduct->product->item_code : null}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($product->supplier)
                                                                <input type="text" class="form-control" readonly value="{{ $newRequisitionProduct ? $newRequisitionProduct->qty : null-  $newRequisitionProduct->purchase_products->sum('qty')}}" style="width: 80px !important">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($product->supplier)
                                                                <input type="text" class="form-control" name="purchase_qty[{{ $product->id }}]" value="0" style="width: 80px !important">
                                                            @endif
                                                        </td>
                                                        <td> @if($product->supplier)
                                                                <input type="text" id="purchase_price_{{ $product->id }}" class="form-control" name="purchase_price[{{ $product->id }}]" value="{{ $product->price }}" style="width: 100px !important">
                                                            @endif
                                                        </td>
                                                        <td> @if($product->supplier)
                                                                <button type="button" onclick="saveQuotationProduct('{{ $product->id }}')" class="btn btn-sm btn-primary">Save Price</button>

                                                                @if($product->purchase_product == null)

                                                                    <button type="button" title="Delete" onclick="Delete('{{ $product->id }}')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                                                @else
                                                                    <button type="button" onclick="resetIndividualPO('{{ $product->requisition_product_id }}','{{ $product->id }}','{{ $product->product_id }}','{{ $product->purchase_product->id }}')" class="btn btn-sm btn-danger">Reset PO
                                                                    </button>
                                                                @endif

                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{--  <div class="col-md-12">
                                                 @php
                                                     $purchased_status = $supplier_quotations[0]->quotation->status;
                                                     $note = '';
                                                     if($purchased_status == 1){
                                                         $note = App\Purchase::where('quotation_id')
                                                     }
                                                 @endphp
                                                 <label>Note: </label>
                                                 <textarea style="width: 100% !important" class="form-control"></textarea>
                                             </div> --}}
                                        </div>
                                    @endforeach


                                </div>
                            </div>
                            <input type="hidden" value="{{ $requisition->id }}" name="requisition_id">
                            <div class="col-md-12">
                                <center><button type="submit" class="btn bg-info">Make Purchase Order</button></center>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@stop

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
    <!-- page script -->
    <script type="text/javascript">

        function resetIndividualPO(requisition_product_id, quotation_product_id, product_id,purchase_product_id){
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to reset PO ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        window.open('{{ url('Inventory/reset-individual-po') }}/'+requisition_product_id+'&'+quotation_product_id+'&'+product_id+'&'+purchase_product_id,'_parent');
                    },
                    cancel: function () {

                    }
                }
            });

        }


        function deleteQuotation(quotation_id){
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to delete this quotation ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        window.open('{{ url('Inventory/delete-supplier-quotation') }}/'+quotation_id,'_parent');
                    },
                    cancel: function () {

                    }
                }
            });

        }

        function changeSupplier(new_requisition_id,quotation_id){
            var supplier_id = document.getElementById('supplier_id_'+quotation_id).value;
            if(supplier_id > 0){
                window.open('{{ url('Inventory/change-quotation-supplier') }}/'+new_requisition_id+'&'+supplier_id+'&'+quotation_id,'_parent');
            }
        }

        function setPayment(new_requisition_id,supplier_id){
            $.ajax({
                url: '{{ url('Inventory/set-supplier-payment') }}/'+new_requisition_id+'&'+supplier_id+'&'+$('#payment_type_'+supplier_id).val(),
                type: 'GET',
                dataType: 'json',
            })
                .done(function() {
                    console.log("success");
                });
        }

        function saveQuotationProduct(id){
            var price = document.getElementById('purchase_price_'+id).value;
            if(price > 0){
                window.open('/Inventory/save-quotation-product-price/'+id+'&'+price,'_parent');
            }

        }
        function getPurchase(quotation_id, product_id){
            var i = 0;
            if ($('#purchase_'+quotation_id+'_'+product_id).is(':checked')){
                i++;
                console.log(i);
            }else{
                i--;
                console.log(i);
            }
        }

        function resetPO(id) {
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to reset PO ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        window.open('{{ url('Inventory/reset-po') }}/'+id, '_parent')
                    },
                    cancel: function () {

                    }
                }
            });
        }

        function Delete(id) {
            $.confirm({
                title: 'Confirm!',
                content: '<hr><strong class="text-danger">Are you sure to delete ?</strong><hr>',
                buttons: {
                    confirm: function () {
                        $.ajax({
                            headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' },
                            url: "{{url('Inventory/delete-quotation')}}/"+id,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {},
                            success:function(response) {
                                if(response.success){
                                    $('#tr-'+id).fadeOut();
                                }else{
                                    $.alert({
                                        title:"Whoops!",
                                        content:"<hr><strong class='text-danger'>Something Went Wrong!</strong><hr>",
                                        type:"red"
                                    });
                                }
                            }
                        });
                    },
                    cancel: function () {

                    }
                }
            });
        }
    </script>
@stop
