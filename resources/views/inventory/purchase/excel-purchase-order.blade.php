<table>
    <tr>
        <td><b>Purchase Order</b></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td> Date:{{ \Carbon\Carbon::now()->format('Y/m/d')}}</td>
    </tr>
    <tr>
        <td>
            Bill For
            <address>
                <strong> {{ $purchase->quotation->supplier->name }} </strong><br>
                Address : {{ $purchase->quotation->supplier->address }}<br>
                Phone: {{ $purchase->quotation->supplier->phone }}<br>
                Email: {{ $purchase->quotation->supplier->email ?? "N/A" }}
            </address>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>
            From
            <address>
                <strong>{{ unit_name() ?? app_info()->company_name }}</strong><br>
                Bscic Industrial estate,<br>
                Kalurghat,Chittagong<br>
                Phone: +8801810-101010<br>
                Email: support@wellbd.com
            </address>
        </td>
    </tr>
    <tr>
        <td>
            <b>Order No :</b> {{ $purchase->id }}
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><b>Order Date :</b> {{ \Carbon\Carbon::parse($purchase->created_at)->format('d-M-Y') }}
        </td>
    </tr>
    <tr>
        <th>Serial #</th>
        <th>Item Code</th>
        <th>Product / Item Name</th>
        <th>Unit </th>
        <th>Price </th>
        <th>Qty</th>
        <th>Subtotal</th>
    </tr>
    @php $i=0; $total=0; @endphp
    @foreach($purchase->quotation->quotationProduct as $product)
    @php $total+=$product->price; @endphp
    <tr>
        <td> {{ ++$i }} </td>
        <td> {{ $product->requisitionProduct->product->item_code }} </td>
        <td> {{ $product->requisitionProduct->product->name }} </td>
        <td> {{ $product->requisitionProduct->product->unit->name }} </td>
        <td> {{ $product->price }} Tk</td>
        <td> {{ $product->qty }}</td>
        <td> {{ $product->price }} Tk</td>
    </tr>
    @endforeach
    <tr></tr>
    <tr></tr>
    <tr>
        <td>Note :</td>
        <td>{{ $purchase->note }}</td>
        <td></td>
        <td></td>
        <td></td>
        <td>Total:</td>
        <td>{{ $total }} TK</td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr>
        <td></td>
        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">Prepared By</td>
        <td></td>
        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">Store
            Manager/In-Charge</td>
        <td></td>
        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">Head Of
            Department</td>
        <td></td>
        <td style="border-top:1px solid black; margin-right:2px; padding-top:50px">Director/MD</td>
        <td></td>
    </tr>
</table>
