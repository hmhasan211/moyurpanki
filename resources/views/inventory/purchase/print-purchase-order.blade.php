@extends('report.index')

@section('title','PO Report')


    <style>

        @media print{
            @page{
                size: A4 portrait;
                /*size: legal landscape;*/
                padding: 0 !important;
                margin: 0 !important;
                /*margin-top: 8mm ;!*Disable print Header (url and page number)*!*/
                /*margin-bottom: 8mm ;!*Disable print Footer (url and page number)*!*/

            }
            body {
                zoom:95%; /*or whatever percentage you need, play around with this number*/
                /*font-family: serif;*/
                font-size:20px;
                /*font-weight: bold;*/
                /*background-color: #FFFFFF !important;*/
            }


        }


    </style>



@section('content')

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm" style="">
    <div id="logo" style="float:right; display: inline;">
      <img src="{{ asset('logo.png') }}" alt="Well Group" height="40" style="border-style: none;">
    </div>
    <center><h1 style="margin-left: 10%">PURCHASE ORDER <button onclick="window.open('{{ url('Inventory/view-requisition-quotation') }}/{{ $purchase->requisition_id }}','_parent')" class="print-none" type="button" style="font-size: 25px">Edit</button></h1></center>

<table class="table border-bottom-none" align="center" cellpadding="0" cellspacing="0" style=" background: #dEe; padding: 7px; padding-bottom: 13px">
  <tbody style="font-size: 12px">
    <tr>
        <td class="td border-right-none border-bottom-none text-left" style="width: 50%;font-size: 16px"><strong>Purchase Order No &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;</strong> {{ $purchase->purchase_no }}, <strong>C/S No</strong> - {{$purchase->new_requisition->cs_no}}</td>
        <td class="td border-right-none border-bottom-none text-left" style="width: 50%"><strong>Order Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp&nbsp;&nbsp;</strong>{{ \Carbon\Carbon::parse($purchase->created_at)->format('d/m/Y') }}</td>
    </tr>
    <tr>
        <td class="td border-right-none border-bottom-none text-left" style="width: 50%"><strong>Order Reference No&nbsp;&nbsp;:&nbsp;&nbsp;
            </strong> PR-{{ $purchase->new_requisition ? $purchase->new_requisition->requisition->requisition_id : null }},
            PR Date - {{ date('d/m/Y', strtotime($purchase->requisition ? $purchase->requisition->created_at :null)) }}
        </td>
        <td class="td border-right-none border-bottom-none text-left" style="width: 50%"><strong>Expected Delivery Date&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</strong>{{ date('d/m/Y', strtotime($purchase->created_at.'+7 days')) }}</td>
    </tr>

  </tbody>
</table>
<table class="table" align="center" cellpadding="0" cellspacing="0" style="margin-top: -1.2%; background: #dEe; padding: 7px; padding-bottom: 10px">
  <tbody style="font-size: 12px">
     <tr>
        <td class="td  border-bottom-none text-left" style="width: 50%"><strong>Supplier: </strong><br>
            <strong style="font-size: 18px"> {{ optional($purchase->details[0]->supplier)->name }} </strong><br>
            <br><span style="font-size: 14px"><strong>Payment Type :</strong> {{ $purchase->payment_type == 2 ? 'Cash' : 'Credit' }}</span><br>
            {{ optional($purchase->details[0]->supplier)->address }}<br>
        </td>
        <td class="td border-right-none border-bottom-none text-left" style="width: 50%"><strong> Deliver To: </strong><br>
            <strong style="font-size: 18px">{{ unit_name() ?? app_info()->company_name }}</strong><br>{{auth_unit_address()}}<br>
    </tr>

  </tbody>
</table>

<table class="table" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">

    <thead>
        <tr style="font-size: 12px;height: 30px; background-color: #e3e1dc;" class="text-center">
            <th style="width: 10%" class="td text-center"> Item Code </th>
            <th style="width: 20%" class="td text-center"> Item Name </th>
             <th style="width: 10%" class="td text-center"> Brand </th>
            <th style="width: 10%" class="td text-center"> Origin </th>
            <th style="width: 10%" class="td text-center"> Quantity </th>
            <th style="width: 10%" class="td text-center"> Unit Price </th>

            <th style="width: 10%" class="td text-center">Total Amount </th>
      </tr>
    </thead>
    <tbody style="font-size: 12px">
        @php $i=0; $total=0; @endphp

          @foreach($purchase->details as $productKey => $product)

          @php $total+= $product['qty'] * $product['price']; @endphp
            <tr>
              <td class="td text-center">{{ optional($product->product)->item_code }}</td>
              <td class="td text-center">{{ optional($product->product)->name }}</td>
              <td class="td text-center">{{ optional(optional($product->product)->brand)->name }}</td>
              <td class="td text-center">{{ optional(optional($product->product)->unit)->name }}</td>
              <td class="td text-center">{{ $product['qty']}}</td>
              <td class="td text-right">{{ $product['price'] }} Tk</td>

              <td class="td text-right">{{ $product['qty'] * $product['price'] }} Tk</td>
            </tr>
          @endforeach


        <tr>
            <td class="td" colspan="5"></td>
            <td class="td text-right"> <strong>Total</strong></td>
            <td class="td text-right"> <strong>{{ $total }} Tk</strong></td>
        </tr>
    </tbody>
</table>
<div style="font-size: 12px">
  {{--<strong>Description: </strong> <i><span>Order By <strong>{{$purchase->user->name}}</strong></span></i>--}}
</div>

<div style="margin-top: 250px" class="signature">
    <table class=" table"  align="center" cellpadding="0" cellspacing="0" style="width: 100%;border:none; float:left;">
      <tr>
        <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 12px; margin-top:30px;" ><br>Prepare By</td>
        <td style="width: 20%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 12px; margin-top:30px;" ><br> Purchase Officer</td>
        <td style="width: 20%;border:none;"></td>
        <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 12px"><br>Head Of the Dept.</td>
        <td style="width: 20%;border:none;"></td>
        {{-- <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 12px"><br><span class="strong">ED/ Director</span></td> --}}
      </tr>
    </table>
</div>
{{-- <br><br><br> --}}
<center><a id="print" class="btn-print print-none" onclick="window.print()" >Print</a></center>
</section>

@endsection
