@extends('layouts.fixed')

@section('title','Purchase Order')
@section('css')
    <style>
        @media print{
            .no-print{
                display:non;
            }
        }
    </style>
@stop
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Purchase Order</h1>
                </div>
                <div class="col-sm-6">
                    <button onclick="window.open('{{ url('Inventory/print-po-no') }}','_blank')" style="float: right" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <div class="card"><br>
                <div class="card-body">

                    <div class="row">

                        <div class="col-lg-12 table-responsive">

                            <div class="table-responsive">

                                <table class="table table-hover table-striped table-bordered text-center" id="myTable">
                                    <thead>
                                        <tr class="bg-info">
                                            <td> #SL</td>
                                            <td> Purchased No</td>
                                            <td> Requisition No</td>
                                            {{-- <td> Supplier / Party</td>
                                            <td> Quotation No </td> --}}
                                            <td> Authorized By</td>
                                            <td> Date & Time</td>
                                            <td> Ordered Duration </td>
                                            <td class="no-print"> Action </td>
                                        </tr>
                                    </thead>

                                    <tbody class="bodyItem" id="requisition_items">
                                    @if($purchases->count()>0)
                                        @foreach($purchases as $key => $purchase)
                                            {{--@dd($purchase->new_requisition);--}}
                                            <tr id="tr-{{ $purchase->id }}">
                                                <td> {{(($purchases->currentPage() - 1) * $purchases->perPage() + $key+1)}} </td>
                                                <td> {{ $purchase->purchase_no }} </td>
                                                <td> {{ $purchase->new_requisition ? $purchase->new_requisition->requisition->requisition_id : null}} </td>
                                                {{-- <td>
                                                    <form action="{{ route('inventory.order.purchase.update',[$purchase->id,$purchase->requisition_id, $purchase->quotation_id]) }}" method="get">
                                                        <button type="submit" class="btn btn-dark btn-block" onclick="return confirm('Are you sure to update Purchase Order!');" title="Click here to update Purchase order.">
                                                            {{ optional(optional($purchase->quotation)->supplier)->name ." -- ".optional(optional($purchase->quotation)->supplier)->phone }}
                                                        </button>
                                                    </form>
                                                </td>
                                                <td> {{ $purchase->quotation_id }} </td> --}}
                                                <td class="text-center"> <label class="badge badge-success"> {{  $purchase->user? $purchase->user->name :'' }} </label> </td>
                                                <td> {{ \Carbon\Carbon::parse($purchase->created_at)->format('d-M-Y -- h:i:s A') }} </td>
                                                <td> {{ $purchase->created_at->diffForHumans() }} </td>
                                                <td class="no-print">
                                                    <a href="{{ route('inventory.purchase.mail',$purchase->id) }}" class="btn btn-info"><i class="fa fa-envelope"></i> Mail</a>

                                                    <a href="{{ route('inventory.purchase.print',$purchase->id) }}" target="_blank" class="btn btn-info"><i class="fa fa-info-circle"></i> Details</a>
                                                    @if(Auth::id() == 28)
                                                        @if($purchase->mrr_status == 0)
                                                            <button onclick="Delete('{{ $purchase->id }}')" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                                        @endif
                                                    @endif
                                                    {{-- <a href="{{ route('inventory.purchase.details',$purchase->id) }}" target="_blank" class="btn btn-info">Details</a> --}}
                                                    {{--<a href="{{ route('purchase_order_excel',$purchase->id) }}" class="btn btn-info">Excel</a>--}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="12" class=" bg-danger text-center"> No Purchase Record found</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <span class="float-right">{{ $purchases->links() }}</span>
                                <br>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop

@section('style')
    <!-- DataTables -->
    {{--  <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">  --}}
@stop

@section('plugin_for_datatable_buttons')
    {{--  <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

    <script src="{{ asset('/js/datatable/jquery-3.3.1.js') }}"></script>
    <script src="{{ asset('/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.print.min.js') }}"></script>  --}}
@stop
@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
@stop
@section('script')
    <!-- page script -->
    <script type="text/javascript">

        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").slideUp(1000, function(){
                    $(this).remove();
                });
            }, 1200);

        });

      function Delete(id) {
        $.confirm({
            title: 'Confirm!',
            content: '<hr><strong class="text-danger">Are you sure to delete ?</strong><hr>',
            buttons: {
                confirm: function () {
                    $.ajax({
                      headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' },
                      url: "{{url('Inventory/delete-purchase-summary')}}/"+id,
                      type: 'DELETE',
                      dataType: 'json',
                      data: {},
                      success:function(response) {
                        if(response.success){
                          $('#tr-'+id).fadeOut();
                        }else{
                          $.alert({
                            title:"Whoops!",
                            content:"<hr><strong class='text-danger'>Something Went Wrong!</strong><hr>",
                            type:"red"
                          });
                        }
                      }
                    });
                },
                cancel: function () {

                }
            }
        });   
      }
    </script>

@stop
