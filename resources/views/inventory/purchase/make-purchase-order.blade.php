@extends('layouts.fixed')

@section('title','Make PO')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Make Purchase Order</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Make Purchase Order</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">

            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        {{-- requisition  lists start --}}
                        <div class="col-lg-12 table-responsive" >

                            <h4 class="text-left " >Requisition No :  <label style="padding: 5px 15px;" class="label-info">  {{ $invoice_requisition->requisition->requisition_id }} </label>
                                {{--<br> Requisition Invoice : <label  style="padding: 5px 15px;" class="label-info">  {{ $invoice_requisition->invoice_no }} </label>--}}
                            </h4>
                            <br>
                                <table class="table table-hover table-bordered capitalize display hide_action_column" id="myTable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th> #SL</th>
                                            <th width="20%"> Item Name</th>
                                            <th width="10%"> Item Code</th>
                                            <th width="15%"> Group</th>
                                            <th width="5%"> Unit</th>
                                            <th  width="5%"> Qty</th>
                                            <th  width="10%"> Price</th>
                                            <th  width="20%"> Supplier</th>
                                            <th  width="15%"> Payment Type</th>
                                            {{--<th  width="20%">Remarks</th>--}}
                                        </tr>
                                    </thead>
                                <tbody>
                                {{ Form::open(['route'=>['inventory.po.store',$invoice_requisition->id]]) }}
                                @php $i=0; @endphp
                                @if($products->count()>0)
                                    @foreach($products as $requisition)
                                        <tr>
                                            <td> {{ ++$i }} </td>
                                            <td> {{ $requisition->product ? $requisition->product->name : "N/A" }} </td>
                                            <td> {{ $requisition->product ? $requisition->product->item_code : "N/A" }} </td>
                                            <td> {{ $requisition->product->group ? $requisition->product->group->name : "N/A" }} </td>
                                            <td> {{ $requisition->product->unit ? $requisition->product->unit->name : "N/A" }} </td>
                                            <td>
                                                {{ $requisition->quotation_qty }}
                                            </td>
                                            <td>
                                                <input type="hidden" name="requisition_product_id[]" value="{{ $requisition->id }}" class="form-control form-control-sm" />
                                                <input type="hidden" name="product_id[]" value="{{ $requisition->product_id }}" class="form-control form-control-sm" />
                                                <input type="hidden" name="qty[]" value="{{ $requisition->qty }}" class="form-control form-control-sm" />
                                                <input requied type="number" step="0.000001" name="price[]" value="{{ $requisition->price }}" class="form-control"/>
                                            </td>
                                            <td>
                                                <select required class="form-control select-2" name="supplier_id[]">
                                                    <option value="">Select Supplier</option>
                                                    @foreach($suppliers as $supplier)
                                                        <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                            <select name="payment_type[]" class="form-control">
                                                <option value="1">Credit</option>
                                                <option value="2">Cash</option>
                                            </select>
                                            </td>
                                            {{--<td><input type="text" name="product_remarks[{{ $requisition->id }}]" placeholder="Brand:Japanese, Origin:Japan etc." class="form-control"/></td>--}}
                                        </tr>
                                    @endforeach
                                @endif
                                <tbody>

                            </table>
                            <br>

                        </div>
                        {{-- requisition  lists End --}}
                        <div class="col-lg-12">
                            {{ Form::textarea('note',null,['class'=>'form-control','rows'=>5,'cols'=>5,'placeholder'=>'PO Note ']) }}
                            <br>
                        </div>


                        <div class="col-lg-4">

                        </div>
                        <div class="col-lg-4">
                            <center>{{ Form::submit('Make Purchase Order',['class'=>'form-control btn-info']) }}</center>
                        </div>
                        <div class="col-lg-4">

                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop

@section('style')
    <!-- DataTables -->
@stop
@section('css')
    <style>
        .capitalize label,.capitalize th{
            text-transform: capitalize!important;
        }
        .table td, .table th {
            padding: 0.40rem;
            vertical-align: top;
            FONT-SIZE: 14PX;
            border-top: 1px solid #dee2e6;
            font-family: monospace;
            font-weight: normal;
        }
        table.dataTable thead > tr > th.sorting_asc,
        table.dataTable thead > tr > th.sorting_desc,
        table.dataTable thead > tr > th.sorting,
        table.dataTable thead > tr > td.sorting_asc,
        table.dataTable thead > tr > td.sorting_desc,
        table.dataTable thead > tr > td.sorting {
            padding-right: 30px;
            font-size: 13px;
            background: #000;
            color: #fff;
        }
    </style>
@stop
@section('plugin_for_datatable_buttons')
    {{--  <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>  --}}

    {{--load script for data table by Ahmed --}}
    {{--<script src="{{ asset('/js/datatable/jquery-3.3.1.js') }}"></script>--}}
    {{--  <script src="{{ asset('/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.print.min.js') }}"></script>  --}}

@stop
