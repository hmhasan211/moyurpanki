@extends('report.index')

@section('title')
    CS Report
@endsection

{{--@section('report_title')--}}
    {{--PO Report--}}
{{--@endsection--}}

{{--@section('by')--}}
    {{--Who prepared this--}}
{{--@endsection--}}

@section('content')
<style>
    .cs-td {
        height: 55px !important;
    }
    @media print{

        .purchased {
            background-color: white !important;
            color: black !important;
            font-weight: bold !important;
        }
        .unpurchased{
            font-weight: lighter !important;
        }
    }

</style>

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <section class="sheet padding-10mm" style="">
        {{--  <div id="logo" style="float:right; display: inline;">
           <img src="{{ asset('logo.png') }}" alt="Well Group" height="40" style="border-style: none;">
         </div> --}}
        <center>
            <h1>Comparative Statement</h1>
            <div style="margin-top: -20px;font-size: 18px">
                <strong>{{ unit_name() ?? app_info()->company_name }}</strong>
            </div>
        </center>
        <br><br>
        <div style="margin-bottom: 5px;font-size: 16px;float: left">
            <strong>C/S No : {{ $requisition->cs_no }}<br>
                Purchase Requisition : {{ $requisition->requisition->requisition_id }}, Dt - {{ date('d-m-Y', strtotime($requisition->created_at)) }}
            </strong>
        </div>
        <div style="margin-bottom: 5px;font-size: 16px;float: right">
            <strong>
                Date : {{ \Carbon\Carbon::parse($requisition->created_at)->format('d-m-Y')}}
            </strong>
        </div>
        <br>
        <table class="table" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">
            <thead>
            <tr style="font-size: 13px;height: 30px;" class="text-center">
                <th class="td"colspan="2">Supplier Name></th>
                @php $counterStopAfterThirdSuppliername = 0; @endphp
                @foreach($suppliers as $sup=>$supplier)
                    @php $counterStopAfterThirdSuppliername++ @endphp
                    <td style="padding: 3px 0px 3px 0px" class="td text-center"> {{ \App\Supplier::find($supplier->first()->supplier_id)['name'] ?? "Unknown" }}  </td>
                @endforeach
                <td rowspan="2" class="td text-center">
                    Last Purchase<br>Tk, Dt, Brand, Origin
                </td>
                <td width="18%" rowspan="2" class="td text-center">Remarks</td>
            </tr>
            <tr style="font-size: 13px;height: 30px; " class="text-center">
                <th class="td text-center">Item</th>
                <th class="td text-center">Qty</th>
                @if($requisition->purchased_status == 1)
                    @php
                        $count_suppliers = count($suppliers);
                        $count_products = count($products);
                        $supplier_number = 0;
                        $po_array = array();
                    @endphp
                    @foreach($suppliers as $sup=>$supplier)
                        @php
                            // dd($sup);
                            $po_no = App\Purchase::query()->where(get_where())->where('requisition_id',$requisition->id)->where('supplier_id',$sup)->first();

                        @endphp
                        <th class="td">
                            PO No -{{$po_no ? $po_no->purchase_no: null}}
                        </th>
                    @endforeach
                @else
                    @foreach($suppliers as $sup=>$supplier)
                        <th class="td">
                            PO No -
                        </th>
                    @endforeach
                @endif
            </tr>
            </thead>
            <tbody>
            @php $i =0; $count = 0; $j = 0 @endphp

            @if($requisition->purchased_status == 1)
                @foreach($products as $productskey=>$cs)
                    <tr style="font-size: 13px" class="text-center">
                        @if($count == 0)
                            <td class="td cs-td">{{ $cs[$count]->requisitionProduct->product->name }}</td>
                            <td class="td cs-td">{{ $cs[$count]->qty }} {{ optional(optional($cs[$count]->product)->unit)->name }}</td>
                            @php
                                $counterStopAfterThirdSupplier = 0;

                            @endphp
                            @foreach($suppliers as $sup=>$supplier)

                                @php
                                    $counterStopAfterThirdSupplier++;
                                    $pricing = App\QuotationProduct::where('supplier_id', $sup)->where('requisition_product_id',$productskey)->first();
                                    if($pricing){
                                     $purchase_product = App\PurchaseProduct::where('quotation_id',$pricing->id)->first();
                                    }
                                @endphp

                                @if(isset($purchase_product->id) )
                                    @if($pricing)
                                    <td class="td cs-td purchased"  style="padding: 3px 0px 3px 0px;background-color: #687b7d; color: white;">
                                        
                                        Tk-{{ $pricing->price }}/- Per {{ optional(optional($cs[$count]->product)->unit)->name }}<br>
                                        {!! $pricing->product_remarks !!}
                                        @php
                                            //$last_purchase_price = $pricing->price;
                                            //$last_purchase_brand = optional(optional($cs[$count]->product)->brand)->name;
                                            $j++;
                                        @endphp
                                        
                                    </td>
                                    @else
                                    <td class="td cs-td unpurchased" style="padding: 3px 0px 3px 0px">
                                        
                                    </td>
                                    @endif
                                @else
                                    <td class="td cs-td unpurchased" style="padding: 3px 0px 3px 0px">
                                        @if($pricing)
                                            Tk-{{ $pricing->price }}/- Per {{ optional(optional($cs[$count]->product)->unit)->name }}<br>
                                            {!! $pricing->product_remarks !!}
                                        @endif
                                    </td>
                                @endif

                                {{-- @if($counterStopAfterThirdSupplier == 3)
                                    @break
                                @endif --}}
                            @endforeach
                            <td class="td cs-td" style="padding: 3px 0px 3px 0px">
                                @php
                                    $last_purchase = App\PurchaseProduct::with('quotation_product')->where('product_id', $cs[$count]->product_id)->orderBy('id','desc')->skip(1)->take(1)->first();
                                @endphp

                                @if($last_purchase)
                                    Tk-{{ optional($last_purchase)->price }}/- Per {{ optional($last_purchase->product_unit)->name }}<br>
                                    {!! optional($last_purchase->quotation_product)->product_remarks !!}
                                @else
                                    New
                                @endif

                            </td>
                            <td class="td cs-td">

                            </td>

                        @endif
                    </tr>
                @endforeach
            @else
                @foreach($products as $productskey=>$cs)
                    <tr style="font-size: 13px">
                        @if($count == 0)
                            <td class="td cs-td text-center">{{ $cs[$count]->requisitionProduct->product->name }}</td>
                            <td class="td cs-td text-center">{{ $cs[$count]->qty }} {{ optional(optional($cs[$count]->product)->unit)->name }}</td>
                            @php $counterStopAfterThirdSupplier = 0; @endphp
                            @foreach($suppliers as $sup=>$supplier)
                                @php
                                    $counterStopAfterThirdSupplier++;
                                    $pricing = App\QuotationProduct::where('supplier_id', $sup)->where('requisition_product_id',$productskey)->first();
                                @endphp
                                <td class="td cs-td" style="padding: 3px 0px 3px 0px">
                                    @if(isset($pricing->id))
                                    Tk-{{ $pricing->price }}/- {{ optional(optional($cs[$count]->product)->unit)->name }}<br>
                                    {!! $pricing->product_remarks !!}
                                    @endif
                                </td>
                            @endforeach
                            <td class="td cs-td text-center">
                                @php
                                    $last_purchase = App\PurchaseProduct::with('quotation_product')->where('quotation_product_id', $cs[$count]->product_id)->orderBy('id','desc')->first();
                                @endphp

                                @if($last_purchase)
                                    Tk-{{ optional($last_purchase)->price }}/- Per {{ optional($last_purchase->product_unit)->name }}<br>
                                    {!! optional($last_purchase->quotation_product)->product_remarks !!}
                                @else
                                    New
                                @endif
                            </td>
                            <td class="td cs-td"></td>
                        @endif
                    </tr>
                @endforeach
            @endif

            {{-- <tr style="font-size: 13px" class="text-center">
                <td class="td">sakib</td>
                <td class="td">sakib</td>
                <td class="td">sakib</td>
                <td class="td">sakib</td>
                <td class="td">sakib</td>
                <td class="td">sakib</td>
                <td class="td">sakib</td>
                <td class="td">sakib</td>
            </tr> --}}
            </tbody>
        </table>
        <div style="margin-top: 250px" class="signature">
            <table class=" table"  align="center" cellpadding="0" cellspacing="0" style="width: 100%;border:none; float:left;">
                <tr>
                    <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 13px; margin-top:30px;" ><br>Prepare By</td>
                    <td style="width: 20%;border:none;"></td>
                    <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 13px; margin-top:30px;" ><br> Purchase Officer</td>
                    <td style="width: 20%;border:none;"></td>
                    <td style="border-top: 1px solid black;width: 20%;text-align: center;font-size: 13px"><br>Head Of the Dept.</td>
                    {{-- <td style="width: 12%;border:none;"></td>
                    <td style="border-top: 1px solid black;width: 15%;text-align: center;font-size: 13px"><br><span class="strong">ED(Tech)/ Director/ MD</span></td> --}}
                </tr>
            </table>
        </div>
        {{-- <br><br><br> --}}
        <center><a id="print" class="btn-print print-none" onclick="window.print()" >Print</a></center>
    </section>

@endsection
