@foreach($products as $productKey => $product)
	<tr>
		<td>{{ $productKey + 1 }}</td>
		<td>{{ optional($product->product)->name }}</td>
		<td>{{ optional($product->product)->item_code }}</td>
		<td>{{ optional(optional($product->product)->group)->name }}</td>
		<td>{{ optional(optional($product->product)->unit)->name }}</td>
		<td>{{ $product->qty }}</td>
		<td>{{ $product->price }}</td>
		
		<td>
			<select required class="form-control select2" name="supplier_id[]" id="supplier_id">
				<option value="">Select Supplier</option>
				@foreach($suppliers as $supplier)
				<option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
				@endforeach
			</select>
		</td>
		<td>{{ $product->note }}</td>
	</tr>
@endforeach
@section('style')
    <!-- DataTables -->

    <link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' type='text/css'>
    
@stop

@section('plugin')
    <!-- DataTables -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
          theme: 'bootstrap4'
        });
    </script>
@stop
