@extends('layouts.fixed')

@section('title','Manage Requisition - Purcahse Requisition')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Comparative Statements</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Manage Quotation Taken Lists</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">

            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        {{-- requisition  lists start --}}
                        <div class="col-lg-12 table-responsive">

                            <table id="comparativeStatementsTable" class="table-bordered table-striped table">
                                <thead>
                                    <tr class="bg-info">
                                        <th>#SL</th>
                                        <th>C/S No</th>
                                        <th>Requisition No</th>
                                        <th>R. Date</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                @if($requisitions->count()>0)
                                    @php $i = 0; @endphp
                                    @foreach($requisitions as $key =>$requisition)
                                        
                                        @if(count($requisition->quotations) > 0)
                                        @php $i++; @endphp
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>C/S No - {{ $requisition->cs_no }}</td>
                                            <td width="11%">{{ $requisition->requisition->requisition_id }}</td>
                                            <td>
                                                {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')   }}  -- {{ $requisition->created_at->diffForHumans() }}
                                            </td>
                                            <td class="text-center">
                                                @if($requisition->sent_for == 2)
                                                <a class=" btn bg-warning" > Without Quotation </a>
                                                @else
                                                    <a class=" btn btn-info" target="_blank" href="{{ route('quotations.comparative-statement',$requisition->id) }}">View Comparative Statement </a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif

                                    @endforeach

                                @endif
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <!-- page script -->
    <script type="text/javascript">
        $(document).ready(function () {
            table = $("#comparativeStatementsTable").DataTable();
        });
    </script>
@stop
