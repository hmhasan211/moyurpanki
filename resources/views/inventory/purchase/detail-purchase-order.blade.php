@extends('layouts.fixed')

@section('title','Purchase Order | Invoice')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Purchase Order</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Purchase Order</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6 text-right">
                    {{-- <a href="{{route('purchase_requisitions_single_excel',$requisition->id)}}" class="btn btn-info"><i class="fa fa-file-excel"></i> Excel</a> --}}
                    <a onclick="window.open('{{ route('inventory.purchase-requisition.single-print',$requisition->id) }}')" class="btn bg-success"> <i class="fa fa-print"></i> Print</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                        <div class="row">
                            <div class="col-12">
                                <h4>
                                    <i class="fas fa-globe"></i> Purchase Order : {{ $purchase->id }} , Requisition Invoice : {{ $purchase->requisition->invoice_no }}
                                    <small class="float-right"> Date: <label class="label-success" style="padding: 5px 15px"> {{ \Carbon\Carbon::parse($requisition->created_at)->format('D-d-M-Y')  }}</label> </small>
                                </h4>
                                <br>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- Table row -->
                        <div class="row">
                            <div class="col-12 table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr class="bg-info">
                                            <th> #SL</th>
                                            <th> Approval</th>
                                            <th>Section / Department</th>
                                            <th>Group</th>
                                            <th>Item Name</th>
                                            <th>Item Code</th>
                                            <th> Unit </th>
                                            <th> Cur Stock</th>
                                            <th> 2 Monts Consum.</th>
                                            <th width="10%"> present Req.Price</th>
                                            <th width="10%"> present Req.Qty</th>
                                            <th> Last Purchase</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                    @php $i=0; @endphp
                                    @if($requisition->count()>0)
                                        {{ Form::open(['route'=>['inventory.requisition-item.checking-approval',$requisition->id],'method'=>'post']) }}

                                        @foreach($requisition->RequisitonProducts as $product)
                                        @php
                                            $last_purchase = $product->product->find($product->product_id);
                                            $last_purchase_date = $last_purchase->last_purchase_date !=null ?  "Date: ". $last_purchase->last_purchase_date : "";
                                            $last_purchase_price = $last_purchase->last_purchase_price !=null ? "Price: ". $last_purchase->last_purchase_price : "";
                                        @endphp
                                        {{-- <input type="hidden" name="all_products_id[]" value="{{ $product->id }}"> --}}
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td> {{ Form::checkbox('product_id[]',$product->id,($product->user_id !=null ? true : false),['class'=>'form-control']) }} </td>
                                                <td>{{ $product->product->department ? $product->product->department->name : "N/A" }}</td>
                                                <td>{{ $product->product->group ? $product->product->group->name : " N/A " }}</td>
                                                <td>{{ $product->product ? $product->product->name  : " N/A " }}</td>
                                                <td>{{ $product->product ? $product->product->item_code : " N/A "  }}</td>
                                                <td>{{ $product->product->unit ? $product->product->unit->name : " N/A "  }}</td>
                                                <td> N/A</td>
                                                <td> N/A </td>
                                                <td>
                                                    <input type="text" name="price[{{ $product->id }}]" value="{{ $product->price }}" class="form-control form-control" />
                                                </td>
                                                <td><input type="text" class="form-control" name="present_qty[{{ $product->id }}]" value="{{ $product->qty }}"></td>
                                                <td>{{ $last_purchase_price}} <br/> {{ $last_purchase_date }} </td>
                                                <td>{{ $product->note }}</td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-lg-4">
                                <label for="sent_for">Sent For: <span style="color: red"> *</span></label>
                                <select id="sent_for" name="sent_for" class="form-control">
                                    <option @if($requisition->sent_for == 1) selected @endif value="1">Quotation</option>
                                    <option @if($requisition->sent_for == 2) selected @endif value="2">Direct Purchase</option>
                                </select>
                            </div>
                            {{-- <div id="suppliers" class="col-lg-4" style="display: none">
                                <label for="supplier_id">Suppliers: <span style="color: red"> *</span></label>
                                <select id="supplier_id" name="supplier_id" class="form-control">
                                    <option value="0">--- Select Supplier ---</option>
                                    @foreach($suppliers as $supplier)
                                    <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="col-lg-12">
                                <br>
                                {{ Form::textarea('checking_note',null,['class'=>'form-control','rows'=>5,'cols'=>5,'placeholder'=>'Approval Note....']) }}
                            </div>
                            <div class="col-lg-12">
                                <br>
                                {{ Form::submit('Approved Requisition Product / Item',['class'=>'btn btn-info']) }}
                            </div>


                        </div>

                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

    <!-- page script -->
    <script type="text/javascript">
        function toggleSupplier(){
            var sent_for = $('#sent_for').val();
            if(sent_for == 1){
                $('#suppliers').hide();
            }else{
                $('#suppliers').show();
            }
        }

    </script>
    <!-- Script -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
@stop
