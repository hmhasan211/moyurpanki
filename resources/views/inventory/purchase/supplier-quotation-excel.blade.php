
@foreach($quotations as $key => $quotation)
<table class="table table-hover table_border_print">
    <thead>
        <tr>
            <th></th>
            <th><h1>Requisition No : {{ $requisition->id }} </h1></th>
            <th></th>
            <th>
                @if($quotation->first()->supplier)
                    Supplier Details: {{ $quotation->first()->supplier->name }} -- {{ $quotation->first()->supplier->phone }}
                @endif
            </th>
        </tr>
        <tr>
            <th>SL</th>
            <th>Item Name</th>
            <th>Item Code</th>
            <th>Item Price</th>
        </tr>
    </thead>
    <tbody>
        @php $i=0; @endphp
        @foreach(\App\QuotationProduct::query()->where('requisition_id',$requisition->id)->where('supplier_id',$quotation->first()->supplier_id)->get()
        as $product)

        <tr>
            <td> {{ ++$i }} </td>
            <td> @if($product->supplier)
                {{ $product->requisitionProduct->product->name }}
                @endif
            </td>
            <td> @if($product->supplier)
                {{ $product->requisitionProduct->product->item_code }}
                @endif
            </td>
            <td> @if($product->supplier)
                {{ $product->price }} &nbsp; Tk
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endforeach
