@extends('report.index')

@section('title')
    All PO's
@endsection

{{--@section('report_title')--}}
    {{--PO Report--}}
{{--@endsection--}}

{{--@section('by')--}}
    {{--Who prepared this--}}
{{--@endsection--}}

@section('content')
<style>
    
    @media print{
        .td{
            padding: 3px !important;
        }
        .purchased {
            background-color: white !important;
            color: black !important;
            font-weight: bold !important;
        }
        .unpurchased{
            font-weight: lighter !important;
        }
    }

</style>

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    @if($purchases->count()>0)
    @foreach($purchases->chunk(30) as $all_purchases)
    <section class="sheet padding-10mm" style="">
        {{--  <div id="logo" style="float:right; display: inline;">
           <img src="{{ asset('logo.png') }}" alt="Well Group" height="40" style="border-style: none;">
         </div> --}}
        <center>
            <h1>All Purchase Orders</h1>
            <div style="margin-top: -20px;font-size: 17px">
                {{ unit_name() ?? app_info()->company_name }}
            </div>
        </center>
        <table class="table" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px;">
            <thead>
                <tr  class="text-center">
                    <th class="td"> #SL</th>
                    <th class="td"> PO No</th>
                    <th class="td"> PR No</th>
                    <th class="td" width="25%"> Authorized By</th>
                    <th class="td" width="35%"> Date & Time</th>
                    <th class="td"> Ordered Duration </th>
                </tr>
            </thead>
            <tbody>
                <tr style="font-size: 13px" class="text-center">
                    @foreach($all_purchases as $key => $purchase)
                        <tr id="tr-{{ $purchase->id }}">
                            <td class="td"> {{ $key + 1 }} </td>
                            <td class="td"> {{ $purchase->purchase_no }} </td>
                            <td class="td"> {{ $purchase->new_requisition ? $purchase->new_requisition->requisition->requisition_id : null}} </td>
                            
                            <td class="td"> <label class="badge badge-success"> {{  $purchase->user? $purchase->user->name :'' }} </label> </td>
                            <td class="td"> {{ \Carbon\Carbon::parse($purchase->created_at)->format('d-M-Y -- h:i:s A') }} </td>
                            <td class="td"> {{ $purchase->created_at->diffForHumans() }} </td>
                        </tr>
                    @endforeach
                </tr>
            </tbody>
        </table>
        
        
    </section>

    @endforeach
    <center><a id="print" class="btn-print print-none" onclick="window.print()" >Print</a></center>
    @endif

@endsection
<script type="text/javascript">
    setTimeout(function(){ 
        window.print(); 
    }, 100);
</script>
