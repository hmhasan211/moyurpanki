@extends('layouts.fixed')

@section('title','Requisition Inventory')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Purchase Order</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Invoice</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            @if($requisition_id == null)
                {{ Form::open(['route'=>'inventory.purchase.confirm','method'=>'post','id'=>'PurchaseForm','class'=>'form-horizontal  ']) }}
            @endif
            @if($requisition_id != null)
                <form action="{{ route('inventory.purchase.update') }}" method="post">
                @csrf
                <input type="hidden" name="purchase_id" value="{{ Request::segment(3) }}">
            @endif
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        {{ Form::label('','Requisition Number') }}
                        <select name="requisition_id" class="form-control invoice_requisition_id">
                            @if($requisition_id == null) <option value="0">Select Requisition Number</option>@endif
                            @forelse($requisitions as $key => $requisition)
                                <option value="{{$requisition->id}}"> {{$requisition->requisition->requisition_id}} -- {{$requisition->invoice_no}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>

                   {{--  <div class="form-group">
                        {{ Form::label('','Quotation\'s') }}
                        <select name="quotation_id" class="form-control invoice_quotation_id">
                            <option value="0">Select Party Quotation</option>
                            @if($quotations != null)
                                @foreach($quotations as $quotation)
                                    <option  {{ $quotation_id != null && $quotation_id == $quotation->id ? "selected" :""}} value="{{$quotation->id}}">Party: {{$quotation->supplier->name}}( {{$quotation->supplier->phone}})</option>
                                @endforeach
                            @endif
                        </select>
                    </div> --}}
                </div>
                <div id="products_table" style="display: none" class="col-lg-12">
                    {{ Form::label('','Items : ') }}
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#SL</th>
                                <th width="20%">Item Name</th>
                                <th>Item Code</th>
                                {{-- <th>Section</th> --}}
                                <th>Group</th>
                                <th>Unit</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Supplier</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody id="view_purchase_products">
                            
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12">
                    {{ Form::label('','Purchase Confirmation Note : ') }}
                    {{ Form::textarea('note',null,['class'=>'form-control','rows'=>5,'cols'=>5,'placeholder'=>'Purchase order confirmation note......']) }}
                </div>

                <div class="col-lg-3">
                    <br>
                    {{ Form::submit('Confirm Purchase Order',['class'=>'form-control btn btn-info']) }}
                </div>

            </div><!-- /.row -->
            {{ Form::close() }}
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>
@stop

@section('script')
    <!-- page script -->
    <script type="text/javascript">

        $(document).on('change','.invoice_requisition_id',function () {
            var requisition_id = $(this).val();

            if(requisition_id > 0){
                $.ajax({
                    method:"post",
                    url:"{{ route('inventory.ajax-quotation-list') }}",
                    data:{requisition_id:requisition_id,_token:"{{ csrf_token() }}"},
                    dataType:"html",
                    success:function (response) {
                        $('#products_table').show();
                        $('#view_purchase_products').html(response);
                    }
                });
            }else{
                $('#products_table').hide();
            }
        });

        {{--  $("#PurchaseForm").submit(function (e) {
            e.preventDefault();
                $.ajax({
                    method:"post",
                    url:"{{ route('inventory.purchase.store') }}",
                    data: $(this).serialize(),
                    dataType:"json",
                    success:function (response) {
                        if (response.success == 1){
                            $.notify("Purchase order Successfully complete", {globalPosition: 'top center',className: 'success'});
                            $('.invoice_quotation_id').html('');

                        }
                        $(this).reset();
                    }
                });
        });  --}}

    </script>

@stop
