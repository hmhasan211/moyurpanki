@extends('layouts.fixed')

@section('title','Manage Quotation - Purcahse Quotation')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Quotation Make </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a href="{{ route('individual.new.requisition.add', $invoice_requisition->id) }}" type="button" class="btn btn-success"><i class="fa fa-plus" ></i>&nbsp;Add New Item</a>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">

            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        {{-- requisition  lists start --}}
                        <div class="col-lg-12 table-responsive" >

                            <h4 class="text-left " >Requisition No :  <label style="padding: 5px 15px;" class="label-info">  {{ $invoice_requisition->requisition->requisition_id }} </label>
                                {{--<br> Requisition Invoice : <label  style="padding: 5px 15px;" class="label-info">  {{ $invoice_requisition->invoice_no }} </label> --}}
                            </h4>
                            <br>
                                <table class="table table-hover table-bordered capitalize display hide_action_column" id="myTable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th> #SL</th>
                                            <th> Item Name</th>
                                            <th> Item Code</th>
                                            <th> Section</th>
                                            <th> Group</th>
                                            <th> Unit</th>
                                            <th  width="10%"> Qty</th>
                                            <th  width="10%"> Price</th>
                                            <th  width="30%">Remarks</th>
                                            <th>Save Qty</th>
                                        </tr>
                                    </thead>
                                <tbody>
                                {{ Form::open(['route'=>['inventory.quotation.store',$invoice_requisition->id]]) }}
                                @php $i=0; @endphp
                                @if($quotations->count()>0)
                                    @foreach($quotations as $requisition)
                                        {{--@dd($requisition)--}}
                                        <tr>
                                            <td> {{ ++$i }} </td>
                                            <td> {{ $requisition->product ? $requisition->product->name : "N/A" }} </td>
                                            <td> {{ $requisition->product ? $requisition->product->item_code : "N/A" }} </td>
                                            <td> {{ $requisition->product->department ? $requisition->product->department->name : "N/A" }} </td>
                                            <td> {{ $requisition->product->group ? $requisition->product->group->name : "N/A" }} </td>
                                            <td> {{ $requisition->product->unit ? $requisition->product->unit->name : "N/A" }} </td>
                                            <td>
                                                <input type="number" name="approved_qty[]" value="{{ $requisition->qty }}" readonly id="approved_qty_{{ $requisition->id }}" class="approved_qty form-control"/>
                                            </td>
                                            <td>
                                                <input type="hidden" name="requisition_product_id[]" value="{{ $requisition->id }}" class="form-control form-control-sm" />
                                                <input type="number" step="0.000001" name="price[]" value="{{ $requisition->price }}" class="form-control"/>
                                            </td>
                                            <td><input type="text" name="product_remarks[]" placeholder="Brand:Japanese, Origin:Japan etc." class="form-control"/></td>
                                            <td class="text-center"><button onclick="saveQty('{{ $requisition->id }}')" type="button" class="btn btn-primary btn-sm">Save Qty</button></td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tbody>

                            </table>
                            <br>

                        </div>
                        {{-- requisition  lists End --}}
                        <div class="col-lg-12">
                            {{ Form::textarea('note',null,['class'=>'form-control','rows'=>5,'cols'=>5,'placeholder'=>'Quotation Note ']) }}
                            <br>
                        </div>

                        <div class="col-lg-4">
                            <select id="supplier_id" name="supplier_id" class="form-control">
                                <option value="{{ 0 }}">Select Supplier / Party</option>
                                @foreach($suppliers as $supplier)
                                    <option value="{{ $supplier->id }}"> {{ $supplier->name ."-". $supplier->phone }}</option>
                                @endforeach
                            </select>
                            <span style="height: 10px; display: block;background: #fff;"></span>
                        </div>

                        <div class="col-lg-4">
                            {{ Form::submit('Save Quotation',['class'=>'form-control btn-info']) }}
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@stop


@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' type='text/css'>
@stop
@section('css')
    <style>
        .capitalize label,.capitalize th{
            text-transform: capitalize!important;
        }
        .table td, .table th {
            padding: 0.40rem;
            vertical-align: top;
            FONT-SIZE: 14PX;
            border-top: 1px solid #dee2e6;
            font-family: monospace;
            font-weight: normal;
        }
        table.dataTable thead > tr > th.sorting_asc,
        table.dataTable thead > tr > th.sorting_desc,
        table.dataTable thead > tr > th.sorting,
        table.dataTable thead > tr > td.sorting_asc,
        table.dataTable thead > tr > td.sorting_desc,
        table.dataTable thead > tr > td.sorting {
            padding-right: 30px;
            font-size: 13px;
            background: #000;
            color: #fff;
        }
    </style>
@stop
@section('plugin_for_datatable_buttons')
    {{--  <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>  --}}

    {{--load script for data table by Ahmed --}}
    {{--<script src="{{ asset('/js/datatable/jquery-3.3.1.js') }}"></script>--}}
    {{--  <script src="{{ asset('/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/js/datatable/buttons.print.min.js') }}"></script>  --}}

@stop
@section('plugin')
    <script type="text/javascript">
        function saveQty(id){
            var price = $('#approved_qty_'+id).val();
            if(price > 0){
                window.open('{{ url('Inventory/save-requisition-product-qty') }}/'+id+'&'+price,'_parent');
            }
        }
        $(".approved_qty").click(function(){
            $(".approved_qty").not(this).attr('readonly','readonly');
            $(this).removeAttr('readonly');

        });
        $('#supplier_id').select2();
    </script>
@stop
