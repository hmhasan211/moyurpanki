@if($purchase->quotation_id == 0)
	@php
	$products = App\PurchaseProduct::where('purchase_id', $purchase->id)->get();
	@endphp
	@foreach($products as $productKey => $product)
		<tr>
			<td>{{ $productKey + 1 }}</td>
			<td>{{ optional($product->product)->name }}</td>
			<td>{{ optional($product->product)->item_code }}</td>
			<td>{{ optional(optional($product->product)->group)->name }}</td>
			<td>{{ optional(optional($product->product)->unit)->name }}</td>
			<td>{{ optional($product->quotation_product)->qty }}</td>
			<td>{{ optional($product->quotation_product)->price }}</td>
			<td>{{ optional(optional($product->quotation_product)->supplier)->name }}</td>
			<td>{{ optional($product->quotation_product)->remarks }}</td>
		</tr>
	@endforeach
@elseif(($purchase->quotation_id > 0) || ($purchase->quotation_id == 10000000000))
	@php 
		$products = App\RequisitionProduct::where('requisition_id', $purchase->requisition_id)->get();
	@endphp
	@foreach($products as $productKey => $product)
		<tr>
			<td>{{ $productKey + 1 }}</td>
			<td>{{ optional($product->product)->name }}</td>
			<td>{{ optional($product->product)->item_code }}</td>
			<td>{{ optional(optional($product->product)->group)->name }}</td>
			<td>{{ optional(optional($product->product)->unit)->name }}</td>
			<td>{{ $product->qty }}</td>
			<td>{{ $product->price }}</td>
			
			<td>
				<select required class="form-control" name="supplier_id[]" id="supplier_id">
					<option value="">Select Supplier</option>
					@foreach($suppliers as $supplier)
					<option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
					@endforeach
				</select>
			</td>
			<td>{{ $product->note }}</td>
		</tr>
	@endforeach
@endif
