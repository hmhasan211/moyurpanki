@if(!empty($suppliers_array))
	@foreach($suppliers_array as $key => $supp)

	@if($suppliers_array[$key]['qty'] > 0)
		<tr>
			<td>{{ $suppliers_array[$key]['name'] }}</td>
			<td>{{ $suppliers_array[$key]['qty'] }}</td>
			<td><input type="number" class="form-control" name="input_qty[{{ $key }}]" value="0"></td>
		</tr>
	@endif
	@endforeach
@else
	<tr>
		<td colspan="3" class="text-center bg-danger">Sorry ! No Available Products !</td>
	</tr>
@endif