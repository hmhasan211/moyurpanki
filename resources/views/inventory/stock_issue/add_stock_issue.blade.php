@extends('layouts.fixed')

@section('title','Stock Issue')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Manage  Goods Issue </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Goods Issue Management</a></li>
                        <li class="breadcrumb-item active">Add Goods Issue </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="col-lg-12">
            <form method="post" action="{{ route('inventory.stock.issue.store') }}">
            @csrf
            <div class="card"><br>
                <div class="card-body">
                    <div class="row">
                        
                            <div class="col-lg-12 table-responsive">
                                <h4 class="bg-info text-center" style="padding: 10px;">GOODS ISSUE</h4>
                                <div class="row">
                                    <div class="col-lg-4 col-sm-4">
                                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                            {{Form::label('', 'Item Code', ['class'=>'label-control'])}}
                                            <select onchange="getSuppliers()" name="product_id" class='selectedProduct form-control' style='width: 200px;'  id="product_id">
                                                <option value='0'>- Select Code/Name -</option>
                                            </select>
                                            @if ($errors->has('code'))
                                                <span class="help-block text-center" id="success-alert">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- <div class="col-lg-2 col-sm-2">
                                        {{Form::label('', 'Available Qty', ['class'=>'label-control'])}}
                                        <input type="text" name="available_qty" id="available_qty" readonly class="form-control">
                                    </div>

                                    <div class="col-lg-2 col-sm-2">
                                        {{Form::label('', 'Issue Qty', ['class'=>'label-control'])}}
                                        <input type="text" name="issue_qty" class="form-control">
                                    </div>
 --}}
                                    <div class="col-lg-4 col-sm-4">
                                        {{Form::label('', 'Reference No', ['class'=>'label-control'])}}
                                        <input type="text" name="reference_no" class="form-control">
                                    </div>

                                    <div class="col-lg-4 col-sm-4">
                                        {{Form::label('', 'Issue Date', ['class'=>'label-control'])}}
                                        <input type="date" name="issue_date" value="{{ $date_today }}" class="form-control">
                                    </div>
                            
                                    
                                    
                            
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Supplier Name</th>
                                                    <th>Available Qty</th>
                                                    <th>Issue Qty</th>
                                                </tr>
                                            </thead>
                                            <tbody id="available_qty">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                            {{Form::label('', 'Note / Remarks / Description ', ['class'=>'label-control'])}}
                                            {{ Form::textarea('note',null,['class'=>'form-control','autofocus','rows'=>5,'cols'=>5,'placeholder'=>'Ex. Remarks / Note / Description','id'=>'note']) }}
                            
                                            @if ($errors->has('price'))
                                                <span class="help-block text-center" id="success-alert">
                                                    <strong>{{ $errors->first('price') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                            
                                    <div class=" col-lg-3">
                                        <div class="form-group">
                                            {{ Form::button("ISSUE GOODS",['class'=>'btn btn-success', 'type'=>'submit']) }}
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                       
                    </div>
                </div>
            </div>
             </form>
        </div>
    </section>
    <!-- /.content -->



@stop

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' type='text/css'>
@stop

@section('plugin')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.js') }}"></script>

    <!-- page script -->
    <script type="text/javascript">
        $(document).ready(function () {
            pendingProducts();
        });

        /* pending product list start */
        function pendingProducts() {
            $.ajax({
                method:"get",
                url:"{{ route('inventory.requisition.pending.products') }}",
                dataType:"html",
                success:function (response) {
                    $("#requisition_items").html(response);
                }
            })
        }
        /* pending product list end */

        function getSuppliers() {
            var product_id = $('#product_id').val();

            $.ajax({
                url: '{{ url('Inventory/add-stock-issue/get-suppliers') }}/'+product_id,
                type: 'GET',
            })
            .done(function(response) {
                $("#available_qty").html(response)
                // if(response.success){
                //     $("#available_qty").val(response.total_goods_in);
                // }else{
                //     $("#available_qty").val(0);
                // }
            });
        }

        /* filter according to Product Code Start */

        // $(document).on('change','#product_id',function () {
        //     var id = $(this).val();
        //     if(id !=null){
        //         $.ajax({
        //             method:"post",
        //             url:"{{ route('inventory.item.info') }}",
        //             data: {id:id, _token:"{{ csrf_token() }}"},
        //             dataType:'json',
        //             success:function (response) {
        //                 $("#unit").val(response.unit);
        //                 $("#price").val(response.price);
        //             }
        //         });
        //     }
        // });

        /* filter according to Product Code End */

        /* store requisition at TempData Start*/
        $(document).on('click','.storeRequestRequisition',function () {
            var product_id = $("#product_id").val();
            var price = $("#price").val();
            var qty = $('#qty').val();
            var note = $("#note").val();

            if(product_id =="Select Code/Name" || price =='' || qty ==''){
                $("#qty").css({"background":"#DC4C40","color":"#fff"});
                $("#price").css({"background":"#DC4C40","color":"#fff"});
            }else{
                $("#qty").css({"background":"#fff","color":"#444"});
                $("#price").css({"background":"#fff","color":"#444"});
                $.ajax({
                    method:"post",
                    url:"{{ route('inventory.purchase.temp.store') }}",
                    data : {product_id:product_id,price:price,qty:qty,note:note, _token:"{{ csrf_token() }}"},
                    dataType:"json",
                    success:function (res) {
                        pendingProducts();
                        if(res.success == 1){
                            $.notify("Product / Item Added to Requisition List", {globalPosition: 'bottom center',className: 'success'});
                        }
                        $("#unit").val(null);
                        $("#price").val(null);
                        $('#qty').val(null);
                        $('#note').val(null);
                    }
                });
            }
        });
        /* store requisition at TempData End */



        /* Requisition Confirmation Start */
        $(document).on("click",".confirmRequisition",function () {
           var note = $('.confirmationNote').val();
           var unit_id = $('#unit_id').val();
            var token  = "{{ csrf_token() }}";
            if(unit_id != 0){
                $("#unit_id").css({"background":"#fff","color":"#444"});
                $.ajax({
                    method:"post",
                    url : "{{ route('inventory.store.requisition') }}",
                    data:{ note:note, unit_id:unit_id , _token:token},
                    dataType:"json",
                    success:function (res) {
                        if (res.success == 1){
                            pendingProducts();
                            $.notify("Purchase Requisition successfully created", {globalPosition: 'bottom center',className: 'success'});
                            $('.confirmationNote').val(null);
                        }else{
                            $.notify("Creating Purchase Requisition failed", {globalPosition: 'bottom center',className: 'error'});
                        }
                    }
                });
            }else{
                $("#unit_id").css({"background":"#DC4C40","color":"#fff"});
            }

        });
        /* Requisition Confirmation End */

        $(document).ready(function(){
            $(".selectedProduct").select2({
                ajax: {
                    url: "{{ url('Inventory/search-product-item') }}",
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            searchTerm: params.term,
                            type: 'query'
                          }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        var op = [];
                        $.each(data,function(index,value){
                            var text = value.item_code + " " + value.name ;
                            op.push(
                                {
                                    id:value.id,
                                    text:text
                                }
                            )
                        });
                        return {
                            results : op
                        }
                    },
                    cache: true
                }
            });
        });

    </script>
    <!-- Script -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>
@stop
